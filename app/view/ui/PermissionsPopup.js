Ext.define('FolderGridFinder.view.ui.PermissionsPopup', {
    extend: 'Ext.window.Window',
    requires: ['FolderGridFinder.model.Ace','FolderGridFinder.store.Groups'],
    draggable: false,
    floating: true,
    layout: 'fit',
    height:450,
    id: 'permissions-popup',
    width:515, 
    title: 'Access Control Listing',
    modal: true,        
    initComponent: function() {
    	var me = this;    	
    	me.editing = Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToEdit: 2,
            autoCancel: true
        });  
    	me.aclStore = Ext.create('Ext.data.Store', {    
        	storeId: 'aclStore',
        	model: 'FolderGridFinder.model.Ace',
        	proxy: {
                type: 'memory',
                reader: {
                    type: 'json',
                    root: 'acl'
                }
            },
        	data:[]
        });
    	var disableBtn = Ext.create('Ext.button.Button', {
            iconCls: 'icon-delete',
            text: 'Remove Entry',
            name: 'deleteButton',
            disabled: true,
            scope: this,
            handler: this.onRemoveClick
        });
        Ext.applyIf(me, {
            items: [
                    {
                    	xtype: 'gridpanel',
                    	store: 'aclStore',
                    	selType: 'rowmodel',
                    	autoScroll: true,
                    	iconCls: 'icon-grid',
                    	listeners: {
                   		 select: function (panel) {
                   			disableBtn.enable();
                   		 }
                        },
                        dockedItems: [{
                            xtype: 'toolbar',
                            items: [{
                                iconCls: 'icon-add',
                                text: 'Add Entry',
                                scope: this,
                                handler: this.onAddClick
                            },
                            disableBtn,
                            {
                            	xtype: 'checkboxfield',
                            	name      : 'subfoldersInherit',
                                boxLabel  : 'New Subfolders Copy These Permissions',                                
                                checked : currentlySelected.get('subfoldersInherit'),
                                listeners: {
                                	 render: function(c){
                                		 Ext.QuickTips.register({
                                			    target: c.getEl(),
                                			    text: "Check this box if new folders created under this one should copy this folder's permissions."
                                			  });
                                	 },
                            		 change: function (box, newValue, oldValue) {
                        	    		var detailsPopup = Ext.create('FolderGridFinder.view.ui.RecursivePromptPopup', {
                        	        		title: 'Scope of Folders Inherit Update',
                        	                renderTo: Ext.getBody(),
                        	                supportApplyContent: false,
                        	                cancelFunc: function(){
                        	                	box.suspendEvents();
                         	        	    	box.setValue(oldValue);
                         	        	    	box.resumeEvents();
                        	                },
                        	                continueFunc: function (includeContent,recurseSubfolders) {
                        	          			 if(recurseSubfolders){
                        	          				var recurse = function(origin,jsonencodeddata,successFn,errorFn){
                        	          		    		var applyingDialog = Ext.Msg.show({
                        	          					    title: 'Applying...',
                        	          					    msg: 'Applying folder inheritance Update. Please wait.',
                        	          					    width: 300,	                    	    								    
                        	          					    wait: true,			    
                        	          					    icon: Ext.window.MessageBox.INFO
                        	          					});    		   	    	                    	    						        	     
                        	          		    	    var taskFn = function(node, callback) {
                        	          		    	        rateLimitedApply(node,jsonencodeddata,callback);    	            	        
                        	          		    	    },    	    
                        	          		    	    rateLimitedApply = _.rateLimit(function(targetNode,jsonencodeddata,callback){    		    		    		                        	          		        		
                        	          		        		Ext.Ajax.request({
                                             	        	    method : "PUT",
                                             	        	    url: "/folder/"+targetNode.get('duid'),
                                             	        	    jsonData : jsonencodeddata,
	                                             	        	callback : function(options,success,response){            	    
	                       	          		            	    	if(!success){
	                       	          		            	    		var failureMsg = null;
	                       	          		            	    		if(response.status == 403){
	                       	          		                	    		failureMsg = "You are not permitted to perform that operation.";
	                       	          		                	    	}else{
	                       	          		                	    		failureMsg =  "Unable to complete requested operation on ["+targetNode.get('name')+"]";	
	                       	          		                	    	}        
	                       	          		            	    		queue.kill();//Stop operation completely on any error
	                       	          		            	    		applyingDialog.close();
	                       	          		            	    		errorFn(failureMsg);
	                       	          		            	    	}else if(success && recurseSubfolders){
	                       	          		            	    		targetNode.set('subfoldersInherit',newValue);
	                       	          			                		for (var i = 0; i < targetNode.childNodes.length; i++) {
	                       	          			                			queue.push(targetNode.childNodes[i]);        			    			    			
	                       	          			                		}
	                       	          		            	    	}            	    	
	                       	          		            	    	callback();  	    	
	                       	          			        	    }                                             	        	    
                                             	        	});
                        	          		        	}, fg_ratelimit);    	    
                        	          			    	var queue = async.queue(taskFn, 5);
                        	          			    	queue.drain = function(){
                        	          			    		applyingDialog.close();
                        	          			    		successFn();	    			    		
                        	          			    	};
                        	          		    	    queue.push(origin);		            		
                        	          		    	}
                        	          		    	
                        	          		    	
                        	          				var jsonencodeddata = Ext.JSON.encode({"subfoldersInherit":newValue});
                    	          					recurse(currentlySelected,jsonencodeddata,
                    	          					 function(){            	    	
                    	          		    	    	Ext.MessageBox.alert('Success', 'Permissions updated.');	    	
                    	          		    	     },
                    	          		    	     function(failureMsg){    	    	
                    	          		    	    	Ext.MessageBox.alert('Failure', failureMsg);
                    	          		    	    });			        	    	        		        	    	        	        	    	        		    
                    	          		    	                        	          		    	
                        	          			 }else{
                        	          				var data = Ext.JSON.encode({"subfoldersInherit":newValue});
                                     	        	Ext.Ajax.request({
                                     	        	    method : "PUT",
                                     	        	    url: "/folder/"+currentlySelected.get('duid'),
                                     	        	    jsonData : data,
                                     	        	    success : function(){
                                     	        	    	currentlySelected.set('subfoldersInherit',newValue);
                                     	        	    	Ext.MessageBox.alert('Success', "Folder inheritance successfully updated for "+currentlySelected.get('name'));
                                     	        	    },
                                     	        	    failure: function(response){	        	    
                                     	        	    	box.suspendEvents();
                                     	        	    	box.setValue(oldValue);
                                     	        	    	box.resumeEvents();
                                     	        	    	Ext.MessageBox.alert('Access Denied', "You are not allowed to change folder inheritance for "+currentlySelected.get('name'));        	    	
                                     	        	    }
                                     	        	});
                        	          			 }           		 
                        	                }
                        	            });	    		
                        	    		detailsPopup.show();                            	    	    	            			 		                				
                            		 }
                                }
                            }
                            ]
                        }],                     	
                        plugins: [this.editing],
                        columns: [
                            {
                                text: 'Grantee',
                                width: 350,
                                sortable: true,
                                hideable: false,
                                dataIndex: 'grantee',
                                editable: false,
                                disabled: true
                            },
                            {
                                text: 'Permission',
                                width: 135,
                                sortable: true,
                                hideable: false,
                                dataIndex: 'permission',
                                renderer: function(value) {
                                	switch(value){
                                	case "READ":
                                		return "Read";
                                		break;
                                	case "READ_ACP":
                                		return "View Permissions";
                                		break;
                                	case "WRITE_ACP":
                                		return "Modify Permissions";
                                		break;
                                	case "WRITE":
                                		return "Write";
                                		break;
                                	case "FULL_CONTROL":
                                		return "Full Control";
                                		break;
                                	default:
                                		return value;
                                	}                                     
                                },
                                name: 'permission',
                                hiddenName: 'permission',
                                editor: {
                                    xtype: 'combobox',
                                    store: {
                                    	fields : ['permission','name'],
                                    	data : [                                    
                                    	        {permission: 'READ',name: 'Read'},
                                    	        {permission: 'WRITE',name: 'Write'},
                                    	        {permission: 'FULL_CONTROL',name: 'Full Control'},
                                    	        {permission: 'READ_ACP',name: 'View Permissions'},
                                    	        {permission: 'WRITE_ACP',name: 'Modify Permissions'}                                    	        
                                    	    ]
                                    },                                    
                                    allowBlank: false,
                                    forceSelection: true,
                                    queryMode: 'local',
                                    displayField: 'name',
                                    valueField: 'permission'                                    
                                }
                            }
                        ]                           
                    }
                  ]                                    
        });                                                
        me.callParent(arguments);
    },   
    onAddClick: function(){
    	var me = this;
    	var form = Ext.widget('form', {
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            border: false,
            bodyPadding: 10,

            fieldDefaults: {
                labelAlign: 'top',
                labelWidth: 100,
                labelStyle: 'font-weight:bold'
            },
            defaults: {
                margins: '0 0 10 0'
            },

            items: [
			{xtype: 'component',
			    html: 'Complete the form to add a new entry to this Access Control List',
			    margin: '0 0 0 10'
			},
            {
				xtype: 'combobox',
				fieldLabel: 'Grantee',
				name: 'grantee',
				store: 'Groups',
                displayField: 'name',
                valueField: 'name',
                queryCaching: true,
                selectOnTab: false,
                allowBlank: false
            }, 
            {
            	xtype: 'combobox',
            	fieldLabel: 'Permission to Grant',
            	name: 'permission',
                store: {
                	fields : ['permission','name'],
                	data : [                                    
                	        {permission: 'READ',name: 'Read'},
                	        {permission: 'WRITE',name: 'Write'},
                	        {permission: 'FULL_CONTROL',name: 'Full Control'},
                	        {permission: 'READ_ACP',name: 'View Permissions'},
                	        {permission: 'WRITE_ACP',name: 'Modify Permissions'}                                    	        
                	    ]
                },                                    
                allowBlank: false,
                forceSelection: true,
                queryMode: 'local',
                displayField: 'name',
                valueField: 'permission'
            }
            ],

            buttons: [{
                text: 'Cancel',
                handler: function() {
                    this.up('form').getForm().reset();
                    this.up('window').hide();
                }
            }, {
                text: 'Create Entry',
                handler: function() {
                    if (this.up('form').getForm().isValid()) {
                    	var theform = this.up('form').getForm();                                                	
                    	var newEntry = new FolderGridFinder.model.Ace();
                    	theform.updateRecord(newEntry);                                                	                                                	
                    	var newModels = me.aclStore.insert(0, newEntry);
                        theform.reset();                        
                        this.up('window').hide();         
                        me.processPermissions(newModels[0],me.aclStore);
                    }
                }
            }]
        });

        win = Ext.widget('window', {
            title: 'New ACL Entry Form',                                        
            height:200,
            width:450,             
            layout: 'fit',
            resizable: true,
            modal: true,
            items: form
        });                                    
        win.show();
    },
    onRemoveClick: function(){
    	this.editing.cancelEdit();
    	var selected = this.down('gridpanel').getSelectionModel().getSelection()[0];
    	selected.set('permission','');    	
    	this.processPermissions(selected,this.aclStore);    	
    }
});