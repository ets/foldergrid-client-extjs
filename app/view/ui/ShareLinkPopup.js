Ext.define('FolderGridFinder.view.ui.ShareLinkPopup', {
    extend: 'Ext.window.Window',

    draggable: false,
    floating: true,
    height:300,
    width:300, 
    modal: true,        
    initComponent: function() {
    	var me = this;
    	var linkform = Ext.widget('form', {
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            border: false,
            bodyPadding: 10,

            fieldDefaults: {
                labelAlign: 'top',
                labelWidth: 100,
                labelStyle: 'font-weight:bold'
            },
            defaults: {
                margins: '0 0 10 0'
            },

            items: [
            {xtype: 'label',
                forId: 'description',
                text: 'A public link allows anyone who follows it to read this file anonymously. Recipients do not need a user account or any credentials.',
                margin: '0 0 0 10'
            },
	        {
		        xtype: 'numberfield',
		        anchor: '100%',
		        fieldLabel: 'Hours Until Link Expires',
		        name: 'hoursTL',
		        value: 24,
		        maxValue: 24*90,
		        minValue: 1
		    }, {
		        xtype: 'numberfield',
		        anchor: '100%',
		        fieldLabel: 'Clicks Until Link Expires',
		        name: 'clicksTL',
		        value: 1,
		        maxValue: 20000,
		        minValue: 1
		    }],

            buttons: [{
                text: 'Cancel',
                handler: function() {
                    this.up('form').getForm().reset();
                    this.up('window').hide();
                }
            }, {
                text: 'Create Link',
                handler: function() {
                    if (this.up('form').getForm().isValid()) {
                    	var theform = this.up('form').getForm();
                    	var theformValues = theform.getFieldValues();
                    	var linkUrl = "/file/link/"+currentlySelected.get('id'); 	    	    		
                    	Ext.Ajax.request({
                    	    method : "POST",
                    	    url: linkUrl,
                    	    params: {
                    	        hoursTL: theformValues.hoursTL,
                    	        clicksTL: theformValues.clicksTL
                    	    },
                            callback: function(options,success,response){
                            	if(response.status == 200){
	                    	    	var jsonData = Ext.JSON.decode(response.responseText);
	                    	    	win = Ext.widget('window', {
	                                    title: 'Expiring Link Successfully Created',                                        
	                                    height:250,
	                                    width:500,                                     
	                                    layout: 'fit',
	                                    resizable: true,
	                                    modal: true,
	                                    items: [
	                                       {
	                                        xtype: 'form',                                        
	                                        layout: {
	                                            type: 'vbox',
	                                            align: 'stretch'
	                                        },
	                                        border: false,
	                                        bodyPadding: 10,
	
	                                        fieldDefaults: {
	                                            labelAlign: 'top',
	                                            labelWidth: 100,
	                                            labelStyle: 'font-weight:bold'
	                                        },
	                                        defaults: {
	                                            margins: '0 0 10 0'
	                                        },
	
	                                        items: [{
	                            	        	xtype: 'displayfield',                            		        
	                            		        fieldLabel: 'Your requested expiring public link is',
	                            		        name: 'link',
	                            		        value: jsonData.link                            		        
	                            	        },{
	                            	        	xtype: 'component',
	                            	        	style: {
	                            	        		align: 'right'
	                            	        	},
	                            		        html: '<a href="mailto:replace-this@foldergrid.com?subject='+encodeURIComponent('Expiring link to '+currentlySelected.get('name'))+'&body='+encodeURIComponent('Click to download '+jsonData.link)+'">Open Email Template</a>'                            		        
	                            	        }]
	                                       }
	                                     ]
	                                });                                    
	                                win.show();  
	                        	}else if(response.status == 401){
	                                Ext.Msg.alert({
	                                    msg: 'You will now be redirected to login again.',
	                                    title: 'Your session expired.',
	                                    closable: false,
	                                    buttons: 1,
	                                    draggable: false,
	                                    fn : function() {
	                                        window.location = window.location; 
	                                    }
	                                });                    
	                            }else if(response.status == 403){
	                            	currentlySelected.set('isACLWritable',false);        	    	
	                    	    	Ext.Msg.alert('Access Denied', "You are not allowed to update permissions for ["+currentlySelected.get('name')+"] and therefore are not allowed to share it with others.");                                             
	                            }else {
	                                Ext.Msg.alert('Unknown Error', 'Your request from the server returned status code ['+response.status+']. Please contact support for assistance.');                                             
	                            }   
                    	    }
                    	});                                                 	                                                	                    	
                        theform.reset();
                        this.up('window').hide();                                                    
                    }
                }
            }]
        });    	
        Ext.applyIf(me, {
            items: [linkform]                                    
        });                                                
        me.callParent(arguments);
    },
    layout : 'fit'        
});