Ext.define('FolderGridFinder.view.ui.LogPopup', {
    extend: 'Ext.window.Window',
    requires: ['FolderGridFinder.model.LogEntry'],
    draggable: false,
    floating: true,
    layout: 'fit',
    width:615,    
    height:400,
    title: 'Audit Log Listing',
    modal: true,        
    initComponent: function() {
    	var me = this;
    	me.logStore = Ext.create('Ext.data.Store', {    
        	storeId: 'logStore', 
        	model: 'FolderGridFinder.model.LogEntry',
        	proxy: {
                type: 'memory',
                reader: {
                    type: 'json',
                    root: 'log'
                }
            },
        	data:[]
        });
        Ext.applyIf(me, {
            items: [
                    {
                    	xtype: 'gridpanel',
                    	store: 'logStore',
                    	selType: 'rowmodel',
                    	autoScroll: true,                    	
                    	iconCls: 'icon-grid',                                             	
                        columns: [								
								{
									width: 160,
								    dataIndex: 'time',
								    text: 'Time',  								    
								    sortable: true
								},								
								{
								    width: 75,
								    dataIndex: 'action',
								    text: 'Action',
								    sortable: true
								},								
                                {
                                    width: 200,
                                    dataIndex: 'actor',
                                    text: 'User',
                                    sortable: true
                                },
                                {
                                	width: 180,
								    dataIndex: 'changed',
								    text: 'Change',
								    sortable: false
								}
                        ]                           
                    }
                  ]                                    
        });                                                
        me.callParent(arguments);        
    }
});