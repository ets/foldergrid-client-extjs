Ext.define('FolderGridFinder.view.ui.MyViewport', {
    extend: 'Ext.container.Viewport',
    alias: 'widget.finderViewport',
    
    activeItem: 0,
    layout: {
        type: 'card'
    },

    initComponent: function() {
        var me = this;
        var gridStore = Ext.create('Ext.data.Store', {
            model: 'FolderGridFinder.model.Folder',
            id: "gridStore",
            remoteGroup: true,
            remoteSort: true,
            buffered: true,
            trailingBufferZone: 300,
            leadingBufferZone: 300,
            pageSize: 75,
            sorters: [{property: 'updated', direction: 'DESC'}],
            proxy: {
            	type: 'rest',
            	simpleSortMode :true,
            	url: '/folder/',
                reader: {
                    root: 'files',
                    totalProperty: 'fileCount'
                },
                extraParams: {'excludeFolders':'true'}        
            }
        });

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'container',
                    layout: {
                        type: 'border'
                    },
                    listeners: {
                    	afterrender: function () {
                    		try {                            			                                                         
                                // When the user drops native files onto the file listing - open the upload dialog
                                this.getEl().on({
                                	drop: function (evt) {
                                		// Stop the browser's default behavior when dropping files in the viewable area
                                        evt.stopPropagation();
                                        evt.preventDefault();                                                                                               
                                    }
                                }
                                );	                                    
                    		}catch(err){
                    			if (typeof console != "undefined" && typeof console.log != "undefined") {
                    				console.log(err);
                        			console.log("drop not supported - will not prevent dropping files into browser window.");	
                    			}                    			
                    		}
                        }
                    },
                    items: [
						{
						    title: 'Folder Tree',
						    region:'west',
						    xtype: 'treepanel',
						    id : 'foldertree',
						    store: 'folderTreeStore',
						    multiSelect: false,
						    displayField: 'name',
                            viewConfig: {
                                animate: false,
                                plugins: [
                                          {ptype: 'treeviewdragdrop',appendOnly: true, dragText: 'Drag and drop on a folder to move'}																		
                                ]                                                         
                            },
						    width: 200,
						    split: true,
						    layout: 'fit'
						},
						{
						    title: 'Selection Panel',
						    region:'south',
						    xtype: 'container',
						    id : 'selection-bar',
						    multiSelect: false,	
						    layout: {
				                type: 'hbox',
				                align: 'center',
				                pack: 'center'
				            }, 
						    items:[
						        {
						        	id: 'selection-msg',
									xtype: 'tbtext',
								    text: ''
						        }
						    ]				            
						},						
                        {                                      
                            title: 'File Listing',                                                        
                            region: 'center',
                            store: gridStore,
                            multiSelect: true,
                            id: 'file-listing',
                            xtype: 'gridpanel',   
                            loadMask: true,
                            selModel: {
                                pruneRemoved: false,
                                mode: 'MULTI'
                            },
                            viewConfig: {  
                                animate: false,
                                trackOver: false,
                                emptyText: '',
                                deferEmptyText: false,
                                plugins: [
                                          {ptype: 'gridviewdragdrop',dragGroup: "TreeDD",enableDrag: true, enableDrop: false,appendOnly: true, dragText: 'Drag and drop on a folder to move'}																		
                                ]
                            },
                            listeners: {
                            	afterrender: function (thispanel) {
                            		try {                            			                                                         
	                                    // When the user drops native files onto the file listing - open the upload dialog
	                                    this.getEl().on({
	                                    	dragover: function (evt) {
		                                        if(currentlySelected != null){
		                                        	var button = Ext.getCmp('fg-toolbar').getComponent('uploadBtn');
		                                            button.fireEvent('click', button, 0);	
		                                        }                                                                                
		                                    }
	                                    }
	                                    );	                                    
                            		}catch(err){
                            			if (typeof console != "undefined" && typeof console.log != "undefined") {
                            				console.log(err);
                            				console.log("dragover not supported - will not auto-popup upload dialog for native file drag.");
                            			}
                            		}                            		
                                }                                
                            },
                            columns: [
	                              {
	                                  xtype: 'gridcolumn',
	                                  width: 25,
	                                  dataIndex: 'type',
	                                  renderer: function(value){
	                                	  var charsetIndex = value.indexOf("; charset");
	                                	  if(charsetIndex > 0){
	                                		  value = value.substring(0,charsetIndex);
	                                	  }else if(value == 'application/word' || value == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'){
	                                		  value = 'application/msword';
	                                	  }else if(value == 'application/excel' || value == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'){
	                                		  value = 'application/vnd.ms-excel';
	                                	  }else if(value == 'application/mspowerpoint' || value == 'application/vnd.openxmlformats-officedocument.presentationml.presentation'){
	                                		  value = 'application/vnd.ms-powerpoint';
	                                	  }else if(value == ''){
	                                		  value = 'application/octet-stream';
	                                	  }
	                                	  return '<img width="16" src="https://dncidmso9nzk0.cloudfront.net/'+value+'?size=16&default=https%3a%2f%2fdncidmso9nzk0.cloudfront.net%2fcrystal%2fapplication%2foctet-stream">';
	                                  },
	                                  text: '',
	                                  sortable: true
	                              },                                      
                                {
                                    xtype: 'gridcolumn',
                                    width: 478,
                                    dataIndex: 'name',
                                    flex: 1,
                                    text: 'Name',
                                    sortable: true
                                },
                                {
                                    xtype: 'gridcolumn',
                                    width: 200,
                                    dataIndex: 'creator',
                                    text: 'Owner',
                                    sortable: true,
                                    hidden: true
                                },
                                {
                                    xtype: 'gridcolumn',
                                    width: 200,
                                    dataIndex: 'lockHolder',
                                    text: 'Locked By',
                                    sortable: false,
                                    hidden: true
                                },                                
                                {
                                    xtype: 'gridcolumn',
                                    width: 150,
                                    dataIndex: 'md5',
                                    text: 'MD5 Hash',
                                    sortable: true,
                                    hidden: true
                                },                                
                                {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'size',
                                    text: 'Size (bytes)',
                                    width: 100,
                                    sortable: true
                                },
                                {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'source-updated',
                                    text: 'Last Modified',  
                                    width: 150,
                                    renderer: function(value){
                                    	if(value == 0 ) return "";
                                    	var d = new Date(0); 
                                    	d.setUTCMilliseconds(value);                                    	
                                        return d;
                                    },
                                    hidden: false,
                                    sortable: true
                                },
                                {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'updated',
                                    text: 'Last Updated',    
                                    width: 120,
                                    hidden: true,
                                    sortable: true                                    
                                },                                
                                {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'type',
                                    text: 'Kind',
                                    width: 120,
                                    sortable: true
                                },
                                {
                                    xtype: 'gridcolumn',
                                    width: 200,
                                    dataIndex: 'comment',
                                    text: 'Comment',
                                    sortable: false,
                                    hidden: true
                                }                                
                            ]
                        },
                        {
                            xtype: 'toolbar',
                            id: 'fg-toolbar',
                            tpl: Ext.create('Ext.XTemplate', 
                                ''
                            ),                            
                            region: 'north',
                            items: [                                    
                                {
								    xtype: 'button',
								    itemId: 'fileBtn',
								    iconCls: 'icon-folder',
								    text: 'File',
								    menu      : [
								                 {itemId: 'openFileBtn',text: 'Open Locally',disabled: true,iconCls: 'icon-file-edit'},
								                 {itemId: 'viewBtn',text: 'Browser View',disabled: true,iconCls: 'icon-view'},
								                 {itemId: 'newFolderBtn',text: 'New Folder',disabled: true,iconCls: 'icon-folder-new'},
								                 {itemId: 'openFolderBtn',text: 'Open Here',disabled: true,iconCls: 'icon-folder-open'},								                 								                 
								                 { xtype: 'menuseparator' },
								                 {itemId: 'copyBtn',text: 'Copy',disabled: true,iconCls: 'icon-copy'},
								                 {itemId: 'pasteBtn',text: 'Paste',disabled: true,iconCls: 'icon-paste'},
								                 {itemId: 'compressBtn',text: 'Compress',disabled: true,iconCls: 'icon-compress'},
								                 {itemId: 'renameBtn',text: 'Rename',disabled: true,iconCls: 'icon-edit'},								                 
								                 {itemId: 'showDetailsBtn',text: 'Show Info',iconCls: 'icon-folder-details'},								                 
								                 {itemId: 'mapFolderBtn',text: 'Sync Locally',disabled: true,iconCls: 'icon-clone'},
								                 {itemId: 'toggleWatchBtn',text: 'Watch Folder',disabled: true,iconCls: 'icon-watch'},								                 
								                 {itemId: 'revisionsBtn',text: 'Show Revisions',disabled: true,iconCls: 'icon-revisions'},
								                 {itemId: 'logBtn',text: 'Show Audit Log',disabled: true,iconCls: 'icon-audit-log'},
								                 {itemId: 'toggleLockBtn',text: 'Lock',disabled: true,hidden: !lockingEnabled,iconCls: 'icon-file-lock'},
								                 {itemId: 'linkBtn',text: 'Share Publicly',disabled: true,iconCls: 'icon-file-share'},
								                 {itemId: 'permissionsBtn',text: 'Permissions',disabled: true,iconCls: 'icon-lock'},
								                 { xtype: 'menuseparator' },
								                 {itemId: 'undeleteBtn',text: 'Show Deleted',disabled: true,iconCls: 'icon-undelete'},
								                 {itemId: 'restoreBtn',text: 'Restore to Home',iconCls: 'icon-restore', visible: false},
								                 {itemId: 'deleteBtn',text: 'Delete',disabled: true,iconCls: 'icon-delete'},								                 
								                 {itemId: 'expireBtn',text: 'Expire',disabled: true,iconCls: 'icon-expire'}								                 
								             ]
								},
								{
								    xtype: 'button',
								    itemId: 'goBtn',
								    iconCls: 'icon-go',
								    text: 'Favorites',
								    menu      : [
								                 {itemId: 'addGoBtn',text: 'Add to Favorites',disabled: true,iconCls: 'icon-go-add'},
								                 {itemId: 'delGoBtn',text: 'Remove from Favorites',disabled: true,iconCls: 'icon-go-del'},
								                 { xtype: 'menuseparator' },										                 		
								                 {itemId: 'goHomeBtn',text: 'Home',iconCls: 'icon-go-home',duid:'~'}
								             ]
								},								
						        {
						            xtype: 'button',
						            itemId: 'uploadBtn', 
						            iconCls: 'icon-up',
						            tooltip: 'Securely add files to the currently selected folder.',
						            text: 'Upload',
						            disabled: true
						        },
						        {
						            xtype: 'button',
						            itemId: 'downloadBtn',
						            iconCls: 'icon-down',
						            tooltip: 'Save the currently selected file to your workstation.',
						            text: 'Download',
						            disabled: true
						        },
						        {
								    xtype: 'button',
								    itemId: 'toolBtn',
								    iconCls: 'icon-tools',
								    hidden: isNotDomainAdmin,
								    text: 'Admin Tools',
								    menu      : [
								                 {itemId: 'mngUsersBtn',text: 'Manage Users',disabled: false,iconCls: 'icon-user-manage'},										                 
								                 {itemId: 'mngGroupsBtn',text: 'Manage Groups',disabled: false,iconCls: 'icon-group-manage'},
								                 {itemId: 'mngSettingsBtn',text: 'Domain Settings',disabled: false,iconCls: 'icon-retain'},
								                 {itemId: 'mngTrashBinBtn',text: 'Manage Trash Bin',disabled: false,iconCls: 'icon-trash-manage'},
								                 { xtype: 'menuseparator' },
								                 {itemId: 'viewUploadReport',text: 'Recent Uploads Report',disabled: false,iconCls: 'icon-upload-report'},								                 
								                 /*
								                 {itemId: 'viewStorageBtn',text: 'Bytes Under Management Chart',disabled: false, hidden:isNotAccountAdmin,iconCls: 'icon-line-chart'},
								                 */
								                 {itemId: 'viewBillingBtn',text: 'Usage & Billing Report',disabled: false, hidden:isNotAccountAdmin,iconCls: 'icon-usage-report',
								                	 tooltip: 'View usage summary from your selected storage provider.',
								                	 listeners: {
								                		 click: function (node, data) {
								                			 window.open(billingReportUrl,'_blank');
								                		 }
						                             }
								                 }								                 
								             ]
								},
								{
								    xtype: 'button',
								    itemId: 'userBtn',
								    iconCls: 'icon-user',
								    text: currentUsername,
								    menu      : [
												{itemId: 'userHomeBtn',text: 'User Home',disabled: false,
													 tooltip: 'Open your user profile page.',
													 iconCls: 'icon-go-home',
													 listeners: {
														 click: function (node, data) {
															 window.location = '/user.html';
														 }
												    }	 
												},
								                 {itemId: 'resetBtn',text: 'Change Password',disabled: false,
								                	 tooltip: 'Change your password.',
								                	 iconCls: 'icon-password',
								                	 listeners: {
								                		 click: function (node, data) {
								                			 window.location = '/reset';
								                		 }
						                             }	 
								                 },								                 								                
								                 {itemId: 'foldersyncBtn',text: 'Install FolderSync',disabled: false,
								                	 tooltip: 'Download and install FolderSync.',
								                	 iconCls: 'icon-disk'
								                 },								                 								                 
								                 {itemId: 'logoutBtn',text: 'Sign Out',disabled: false,
								                	 tooltip: 'Sign Out and quit the current session.',
								                	 iconCls: 'icon-logout',
								                	 listeners: {
								                		 click: function (node, data) {
								                			 window.location = '/logout';
								                		 }
						                             }	 
								                 },
								                 { xtype: 'menuseparator' },								                 
								                 {xtype: 'menucheckitem', text: 'Open Locally by default', disabled: false,
								                	 tooltip: 'On a double-click, attempt to open the selected file locally instead of downloading it.',
								                	 checked: Ext.util.Cookies.get("openlocallyByDefault"),								                	 
								                	 listeners: {
								                		 checkchange: function (item, isChecked) {
								                			 if(isChecked){
								                				 var expiration_date = new Date();
							                		    		 expiration_date.setFullYear(expiration_date.getFullYear() + 5);
							                		    		 Ext.util.Cookies.set("openlocallyByDefault",'true',expiration_date);
								                			 }else{
								                				 Ext.util.Cookies.clear("openlocallyByDefault");
								                			 }
								                		 }
						                             }	 
								                 }
								             ]
								},																								
						        {
						            xtype: 'button',
						            itemId: 'helpBtn',
						            iconCls: 'icon-help',
						            text: 'Help',
						            menu      : [
								                 {itemId: 'kbBtn',text: 'User Documentation', iconCls: 'icon-search-help',
								                	 listeners: {
								                		 click: function (node, data) {
								                			 window.open(kbURL);
								                		 }
						                             }
								                 },
								                 {itemId: 'supportBtn',text: 'Open a Support Ticket', iconCls: 'icon-ticket-help',
								                	 listeners: {
								                		 click: function (node, data) {
								                			 window.open(supportURL);
								                		 }										                 
						                             }
								                 }										                 
								             ]
						        },                                   
                                '->',
                                {
                                	xtype: 'searchcontainer',
                                	id: 'search-container',
                                	width: 230,
                                	margin: '4 0 0 0'
                                }                                                                                                
                            ]
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }
});