Ext.define('FolderGridFinder.view.ui.SearchContainer', {
    extend: 'Ext.container.Container',
    alias: 'widget.searchcontainer',

    initComponent: function() {
            this.cls = 'search';
            this.items = [
                {
                    xtype: 'triggerfield',
                    triggerCls: 'reset',
                    emptyText: 'Search',
                    width: 170,
                    id: 'search-field',
                    enableKeyEvents: true,
                    hideTrigger: true,
                    onTriggerClick: function() {                    	
                    	this.clearAndClose();
                    },
                    clearAndClose:function() {
                    	this.reset();                        
                        this.setHideTrigger(true);
                        Ext.getCmp('search-dropdown').hide();
                    }
                },
                {
                    xtype: 'searchdropdown'
                }
            ];
        this.callParent();
    }    
});