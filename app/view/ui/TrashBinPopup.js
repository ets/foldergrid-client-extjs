Ext.define('FolderGridFinder.view.ui.TrashBinPopup', {
    extend: 'Ext.window.Window',
    requires: ['FolderGridFinder.model.Folder'],
    draggable: false,
    floating: true,
    layout: 'fit',
    width:515, 
    height: 500,
    title: 'TrashBin Folder Listing',
    modal: true,        
    initComponent: function() {
    	var me = this;
    	var trashBinStore = Ext.create('Ext.data.Store', {
            model: 'FolderGridFinder.model.Folder',
            id: "trashBinStore",
            remoteGroup: true,
            remoteSort: true,
            buffered: true,
            trailingBufferZone: 600,
            leadingBufferZone: 600,
            pageSize: 300,
            autoLoad: true,
            sorters: [{property: 'updated', direction: 'DESC'}],            
            proxy: {
            	type: 'rest',
            	simpleSortMode :true,
            	url: '/folder/TRASHBIN',
                reader: {
                    root: 'folders',
                    totalProperty: 'folderCount'
                },
                startParam: 'folders_start',
                limitParam: 'folders_limit',
                directionParam: 'folders_dir',
                extraParams: {'excludeFiles':'true'}        
            }
        });    	
        Ext.applyIf(me, {
            items: [
                    {
                    	xtype: 'gridpanel',
                    	store: 'trashBinStore',
                    	selType: 'rowmodel',
                    	multiSelect: false,
                    	autoScroll: true,
                    	iconCls: 'icon-grid',                    	
                        dockedItems: [{
                            xtype: 'toolbar',
                            id: 'trash-toolbar',
                            items: [
	                            {
	                            	itemId: 'trashRestoreBtn',text: 'Restore to Home',iconCls: 'icon-restore', disabled: true,
	                            	handler: function () {
	                            		var selection = grid.getView().getSelectionModel().getSelection()[0];
	                                    if (selection) {
	                                    	var reqJson = Ext.JSON.encode({"parentDuid":"~"});
		                               		moveUrl = "/folder/"+selection.get('duid');	        	  
		                                	Ext.Ajax.request({
		                                	    method : "PUT",
		                                	    url: moveUrl,
		                                	    jsonData : reqJson,
		                                	    success : function(){    	        	
		                                	    	grid.getStore().remove(selection); 
	                        	        	    	grid.getStore().load();
		                                	    	Ext.MessageBox.alert('Restore Succeeded', "["+selection.get('name')+"] was restored to your home folder.");
		                                	    },
		                                	    failure : function(){
		                                	    	Ext.MessageBox.alert('Restore Failed', "Unable to restore folder to your home folder.");        	        	    	
		                                	    }
		                                	});	
	                                    }	                            		
	                            	}
	                            }
                            ]
                        }],                     	
                        columns: [
								{
								    xtype: 'gridcolumn',
								    width: 478,
								    dataIndex: 'name',
								    flex: 1,
								    text: 'Name',
								    sortable: true
								},
                                {
                                    width: 150,
                                    dataIndex: 'creator',
                                    text: 'Owner',
                                    sortable: false
                                },                                                                                                
                                {
                                    dataIndex: 'updated',
                                    text: 'Updated',                                      
                                    sortable: false
                                },
                                {
                                    dataIndex: 'created',
                                    text: 'Created',                                      
                                    sortable: false                                 
                                }
                        ]                           
                    }
                  ]                                    
        });                                                
        me.callParent(arguments);
        var grid = me.down('gridpanel');
        grid.getSelectionModel().on('selectionchange', function(selModel, selections){
        	var selection = grid.getView().getSelectionModel().getSelection()[0];
        	grid.down('#trashRestoreBtn').setDisabled(false);        	
        });
    }
});