Ext.define('FolderGridFinder.view.ui.GroupsPopup', {
    extend: 'Ext.window.Window',
    requires: ['FolderGridFinder.model.User','FolderGridFinder.model.Group','FolderGridFinder.store.Groups'],
    draggable: false,
    floating: true,
    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    height:500,
    width:850, 
    title: 'Manage Domain Groups',
    modal: true,        
    initComponent: function() {
    	var me = this;  	
    	var groupStore = Ext.data.StoreManager.lookup('Groups');
    	groupStore.load();
    	var sm = Ext.create('Ext.selection.CheckboxModel',{
    		checkOnly: true
    	});
    	var userStore = Ext.create('Ext.data.Store', {
            autoLoad: true,
            autoSync: true,
            storeId: 'domainUserStore',
            model: 'FolderGridFinder.model.User',
            sorters: [{
                property: 'email',
                direction: 'ASC'
            }],
            proxy: {
                type: 'rest',
                url: '/user/',
                reader: {
                    type: 'json'                                        
                }                
            } 
        });
        var grid1 = Ext.create('Ext.grid.Panel', {
            store: userStore,            
            selModel: sm,
            disabled: true,
            columns: [
				{
				    text: 'Email',
				    flex: 1,
				    sortable: true,							    
				    dataIndex: 'email',
				    field: {
				        xtype: 'textfield',
				        disabled: true
				    }							
				}, {
				    header: 'First Name',
				    width: 80,
				    sortable: true,
				    dataIndex: 'firstname',
				    field: {
				        xtype: 'textfield'
				    }
				}, {
				    text: 'Last Name',
				    width: 80,
				    sortable: true,
				    dataIndex: 'lastname',
				    field: {
				        xtype: 'textfield'
				    }
				}							
            ],
            columnLines: true,
            flex: 2,       
            frame: false,
            title: 'Group Members'            
        });            	 	
    	var grid2 = Ext.create('Ext.grid.Panel', {
    		store: groupStore,
    		dockedItems: [{
                xtype: 'toolbar',
                items: [{
                    iconCls: 'icon-add',
                    text: 'Add',
                    scope: this,
                    handler: function(){
                    	Ext.Msg.prompt('New Group', 'Please enter a new group name:', function(btn, text){
                    	    if (btn == 'ok'){
                    	    	var groupNameTest = /^[A-Za-z 0-9\\-]+$/;
                    	    	if(!groupNameTest.test(text)){
                    	    		Ext.Msg.alert({
                                		msg: 'Group names may only contain alphanumerics with spaces and dashes. All other characters are prohibited.',
                                		title: 'Invalid Group Name',                                		
                                		buttons: 1,
                                		draggable: false                                		
                                	});	
                    	    	}else if(groupStore.findExact('name',text) < 0){
	                    	    	var newGroup= new FolderGridFinder.model.Group();
	                    	    	newGroup.set('name',text);                                                	                                                	
	                    	    	groupStore.add(newGroup);
	                    	    	groupStore.sync();
                    	    	}
                    	    }
                    	});    	                    	
                    }
                }, '-',  {
                    iconCls: 'icon-delete',
                    itemId: 'deleteGroup',
                    text: 'Delete',
                    scope: this,
                    disabled: true,
                    handler: function(){
                        var selection = grid2.getView().getSelectionModel().getSelection()[0];
                        if (selection) {
                        	groupStore.remove(selection);
                        }
                    }
                }]
    		}],
            columns: [
				{
				    header: 'Group Name',
				    flex: 1,
				    sortable: true,
				    dataIndex: 'name'				    
				}							
            ],
            flex: 1,
            frame: false,
            title: 'Groups'            
        });            	
        Ext.applyIf(me, {
            items: [grid2,grid1]                                                      
        });                                                
        me.callParent(arguments);        
        grid2.getSelectionModel().on('selectionchange', function(selModel, selections){
        	grid2.down('#deleteGroup').setDisabled(selections.length === 0);
        	grid1.setDisabled(selections.length === 0);        	
        	var sm = grid1.getView().getSelectionModel();
        	sm.deselectAll(true);
        	if(selections.length > 0){	        		
        		var group = selections[0];
            	grid1.setTitle("Members of "+group.get('name'));
            	if(group.raw){
            		var members = group.raw.users;
    	        	if(members != null){
    	        		for(var i=0;i<members.length;i++){        		
    		        		sm.select(grid1.getStore().find('email', members[i].email),true,true);	
    		        	}	
    	        	}	
            	}	        		        	
        	}
        });
        grid1.getSelectionModel().on('select', function(rowModel,record, index){
        	var group = grid2.getView().getSelectionModel().getLastSelected();
        	Ext.Ajax.request({
        	    method : "PUT",
        	    url: '/group/'+group.get('name')+'/'+record.get('email'),
        	    success : function(){        	    	
        	    	FolderGridFinder.model.Group.load(group.get('name'),{
        	        	success: function(record) {
        	        		var existingRecord = groupStore.getById(record.get('name'));
                	    	existingRecord.data = record.data;
                	    	existingRecord.raw = record.raw;
                	    	existingRecord.commit();                                          
        	            }        	            
        	        });         	    	
        	    },
        	    failure: function(response){	        	    
        	    	Ext.MessageBox.alert('Error', "Unable to add "+record.get('email')+" to "+group.get('name'));
        	    	grid1.getSelectionModel().deselect(record,true);
        	    }
        	});
        });
        grid1.getSelectionModel().on('deselect', function(rowModel,record, index){
        	var group = grid2.getView().getSelectionModel().getLastSelected();
        	Ext.Ajax.request({
        	    method : "DELETE",
        	    url: '/group/'+group.get('name')+'/'+record.get('email'),
        	    success : function(){
        	    	FolderGridFinder.model.Group.load(group.get('name'),{
        	        	success: function(record) {
        	        		var existingRecord = groupStore.getById(record.get('name'));
                	    	existingRecord.data = record.data;
                	    	existingRecord.raw = record.raw;
                	    	existingRecord.commit();                                          
        	            }        	            
        	      
                  });        	    	
        	    },
        	    failure: function(response){	        	    
        	    	Ext.MessageBox.alert('Error', "Unable to remove "+record.get('email')+" from "+group.get('name'));
        	    	grid1.getSelectionModel().select(record,true);
        	    }
        	});
        });        
    }    
});