Ext.define('FolderGridFinder.view.ui.DeletedPopup', {
    extend: 'Ext.window.Window',
    requires: ['FolderGridFinder.model.File'],
    draggable: false,
    floating: true,
    layout: 'fit',
    width:550, 
    height: 500,
    title: 'Deleted File Listing',
    modal: true,    
    initComponent: function() {
    	var me = this;
    	me.delStore = Ext.create('Ext.data.Store', {    
        	storeId: 'delStore', 
        	model: 'FolderGridFinder.model.File',
            remoteGroup: true,
            remoteSort: true,
            buffered: true,
        	proxy: {
                type: 'memory',
                reader: {
                    type: 'json'
                }
            },
        	data:[]
        });
        Ext.applyIf(me, {
            items: [
                    {
                    	xtype: 'gridpanel',
                    	store: 'delStore',
                    	selType: 'rowmodel',
                    	multiSelect: true,                    	
                    	iconCls: 'icon-grid',
                        dockedItems: [{
                            xtype: 'toolbar',
                            items: [{
                                iconCls: 'icon-down',
                                itemId: 'undeleteFile',
                                text: 'Undelete',
                                scope: this,
                                disabled: true,
                                handler: function(){
                                	var selection = grid.getView().getSelectionModel().getSelection();
                                	if(!selection) return;
                                	
                                	if(selection instanceof Array){
                                		var numSelected = selection.length;
                    					for(i=0;i<numSelected;i++){
                    						var item = selection[i];
                    						Ext.Ajax.request({
                    							method : "POST",
                    							url: '/file/undelete/'+item.get('id'),
                            	        	    success: function(purgedItem){
                            	        	    	return function(response){      
                            	        	    		grid.getStore().remove(purgedItem); 
                            	        	    		grid.getStore().load();
                                	        	    	Ext.ComponentManager.get('file-listing').getStore().load({addRecords: true});      	        	    		
                            	        	    	};            	        	    	    	        	    	
                            	        	    }(item),
                            	        	    failure: function(purgedItem){
                            	        	    	return function(response){                                 	        	    		  
                            	        	    		if(response.status == 403) {
                            	        	    			Ext.MessageBox.alert('Forbidden', "You can not undelete ["+purgedItem.get('name')+"] in this folder. The most likely cause is an existing file of the same name in the original folder.");
                            	        	    		}else if(response.status == 401) {	 
                                	        	    		Ext.MessageBox.alert('Unauthorized', "You are not allowed to undelete "+purgedItem.get('name'));	
                            	        	    		}else{
                            	        	    			Ext.MessageBox.alert('Error', "Unable to undelete "+purgedItem.get('name')+" at this time. Please try again later.");
                            	        	    		}
                            	        	    	};            	        	    	    	        	    	
                            	        	    }(item)
                            	        	});					
                    					}  
                                	}else{
                                                                        	
                                    	Ext.Ajax.request({
                        	        	    method : "POST",
                        	        	    url: '/file/undelete/'+selection.get('id'),
                        	        	    success : function(resp){   
                        	        	    	grid.getStore().remove(selection); 
                        	        	    	grid.getStore().load();
                        	        	    	Ext.ComponentManager.get('file-listing').getStore().load({addRecords: true});
                        	        	    },
                        	        	    failure: function(resp){
                        	        	    	return function(response){      
                        	        	    		if(response.status == 403) {
                        	        	    			Ext.MessageBox.alert('Forbidden', "You can not undelete ["+selection.get('name')+"] in this folder. The most likely cause is an existing file of the same name in the original folder.");
                        	        	    		}else if(response.status == 401) {	 
                            	        	    		Ext.MessageBox.alert('Unauthorized', "You are not allowed to undelete "+selection.get('name'));	
                        	        	    		}else{
                        	        	    			Ext.MessageBox.alert('Error', "Unable to undelete "+selection.get('name')+" at this time. Please try again later.");
                        	        	    		}      	        	    		
                        	        	    	};            	        	    	    	        	    	
                        	        	    }
                        	        	});
                                    }
                                }
                            },
                            {
							    xtype: 'button',
							    iconCls: 'icon-delete',
							    itemId: 'purgeFile',
							    hidden: isNotDomainAdmin,
							    text: 'Purge',
							    scope: this,
                                disabled: true,
                                handler: function(){
                                	var selection = grid.getView().getSelectionModel().getSelection();
                                	if(!selection) return;
                                	
                                	if(selection instanceof Array){
                    					var numSelected = selection.length;
                    					for(i=0;i<numSelected;i++){
                    						var item = selection[i];
                    						Ext.Ajax.request({
                    							method : "POST",
                            	        	    url: '/file/purge/'+item.get('id'),
                            	        	    success: function(purgedItem){
                            	        	    	return function(response){      
                            	        	    		grid.getStore().remove(purgedItem);
                            	        	    		grid.getStore().load();
                            	        	    	};            	        	    	    	        	    	
                            	        	    }(item),
                            	        	    failure: function(purgedItem){
                            	        	    	return function(response){     
                            	        	    		if(response.status == 403) {
                            	        	    			Ext.MessageBox.alert('Forbidden', "You can not purge ["+purgedItem.get('name')+"] in this folder.");
                            	        	    		}else if(response.status == 401) {	 
                                	        	    		Ext.MessageBox.alert('Unauthorized', "You are not allowed to purge "+purgedItem.get('name'));	
                            	        	    		}else{
                            	        	    			Ext.MessageBox.alert('Error', "Unable to purge "+purgedItem.get('name')+" at this time. Please try again later.");
                            	        	    		}       	        	    		
                            	        	    	};            	        	    	    	        	    	
                            	        	    }(item)
                            	        	});					
                    					}    															
                                	}else{
                                    	Ext.Ajax.request({
                        	        	    method : "POST",
                        	        	    url: '/file/purge/'+selection.get('id'),
                        	        	    success : function(resp){   
                        	        	    	grid.getStore().remove(selection);                             	        	    	
                        	        	    	grid.getStore().load();
                        	        	    },
                        	        	    failure: function(resp){
                        	        	    	return function(response){      
                        	        	    		if(response.status == 403) {
                        	        	    			Ext.MessageBox.alert('Forbidden', "You can not purge ["+selection.get('name')+"] in this folder.");
                        	        	    		}else if(response.status == 401) {	 
                            	        	    		Ext.MessageBox.alert('Unauthorized', "You are not allowed to purge "+selection.get('name'));	
                        	        	    		}else{
                        	        	    			Ext.MessageBox.alert('Error', "Unable to purge "+selection.get('name')+" at this time. Please try again later.");
                        	        	    		}      	        	    		
                        	        	    	};            	        	    	    	        	    	
                        	        	    }
                        	        	});    		
                                	}    	    	    	   
                                }
							}
                            ]
                        }],                     	
                        columns: [
								{
								    xtype: 'gridcolumn',
								    width: 200,
								    dataIndex: 'name',
								    flex: 1,
								    text: 'Name',
								    sortable: true
								},
                                {
                                    width: 150,
                                    dataIndex: 'creator',
                                    text: 'Owner',
                                    sortable: true,
                                    hidden: true
                                },                                                                
                                {
                                    width: 150,
                                    dataIndex: 'md5',
                                    text: 'MD5 Hash',
                                    sortable: true,
                                    hidden: true
                                },                                
                                {
                                    dataIndex: 'size',
                                    text: 'Size (bytes)',
                                    sortable: true
                                },                                
                                {
                                    dataIndex: 'source-updated',
                                    text: 'Last Modified',  
                                    renderer: function(value){
                                    	if(value == 0 ) return "";
                                    	var d = new Date(0); 
                                    	d.setUTCMilliseconds(value);                                    	
                                        return d;
                                    },
                                    sortable: true
                                },
                                {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'created',
                                    text: 'Created',                                    
                                    sortable: true,
                                    hidden: true
                                },                                
                                {
                                    dataIndex: 'type',
                                    text: 'Kind',
                                    sortable: true
                                },
                                {
                                	xtype: 'booleancolumn', 
                                    dataIndex: 'payload-exists',
                                    text: 'Loaded',
                                    trueText: 'Yes',
                                    falseText: 'No',
                                    sortable: true,
                                    hidden: true
                                }
                        ]                           
                    }
                  ]                                    
        });                                                
        me.callParent(arguments);
        var grid = me.down('gridpanel');
        grid.getSelectionModel().on('selectionchange', function(selModel, selections){
        	var selection = grid.getView().getSelectionModel().getSelection()[0];
        	grid.down('#undeleteFile').setDisabled(selection == null);
        	grid.down('#purgeFile').setDisabled(selection == null);
        });
    }
});