Ext.define('FolderGridFinder.view.ui.RevisionsPopup', {
    extend: 'Ext.window.Window',
    requires: ['FolderGridFinder.model.File'],
    draggable: false,
    floating: true,
    layout: 'fit',
    width:515, 
    height: 500,
    title: 'File Revision Listing',
    modal: true,        
    initComponent: function() {
    	var me = this;
    	me.revStore = Ext.create('Ext.data.Store', {    
        	storeId: 'revStore', 
        	model: 'FolderGridFinder.model.File',
        	idProperty:'revision-id', 
        	proxy: {
                type: 'memory',
                reader: {
                    type: 'json'
                }
            },
        	data:[]
        });
        Ext.applyIf(me, {
            items: [
                    {
                    	xtype: 'gridpanel',
                    	store: 'revStore',
                    	selType: 'rowmodel',
                    	autoScroll: true,
                    	iconCls: 'icon-grid',
                        dockedItems: [{
                            xtype: 'toolbar',
                            items: [{
                                iconCls: 'icon-down',
                                itemId: 'downloadRevision',
                                text: 'Download',
                                scope: this,
                                disabled: true,
                                handler: function(){
                                	var selection = grid.getView().getSelectionModel().getSelection()[0];
                                    if (selection) {
                                    	try {
                                    	    Ext.destroy(Ext.get('downloadIframe'));
                                    	}catch(ignore) {}
                                    	try {
                                	    	Ext.core.DomHelper.append(document.body, {
                                	    	    tag: 'iframe',
                                	    	    id:'downloadIframe',
                                	    	    frameBorder: 0,
                                	    	    width: 0,
                                	    	    height: 0,
                                	    	    css: 'display:none;visibility:hidden;height:0px;',
                                	    	    src: '/file/'+currentlySelected.get('id')+'/'+selection.get('revision-id')
                                	    	});
                                    	}catch(e) {
                                    		Ext.MessageBox.alert('Failed', 'We were unable to complete the requested download of that specific revision. Please try again. '+e.message);
                                    	}
                                    }
                                }
                            }]
                        }],                     	
                        columns: [
                                {
                                    width: 150,
                                    dataIndex: 'creator',
                                    text: 'Owner',
                                    sortable: true
                                },                                                                
                                {
                                    width: 150,
                                    dataIndex: 'md5',
                                    text: 'MD5 Hash',
                                    sortable: true,
                                    hidden: true
                                },                                
                                {
                                    dataIndex: 'size',
                                    text: 'Size (bytes)',
                                    sortable: true
                                },
                                {
                                    dataIndex: 'source-updated',
                                    text: 'Last Modified',  
                                    renderer: function(value){
                                    	if(value == 0 ) return "";
                                    	var d = new Date(0); 
                                    	d.setUTCMilliseconds(value);                                    	
                                        return d;
                                    },
                                    sortable: true
                                },
                                {
                                    dataIndex: 'source-created',
                                    text: 'Created',  
                                    renderer: function(value){
                                    	if(value == 0 ) return "";
                                    	var d = new Date(0); 
                                    	d.setUTCMilliseconds(value);                                    	
                                        return d;
                                    },
                                    sortable: true,
                                    hidden: true                                   
                                },                                
                                {
                                    dataIndex: 'type',
                                    text: 'Kind',
                                    sortable: true
                                },
                                {
                                	xtype: 'booleancolumn', 
                                    dataIndex: 'payload-exists',
                                    text: 'Loaded',
                                    trueText: 'Yes',
                                    falseText: 'No',
                                    sortable: true
                                }
                        ]                           
                    }
                  ]                                    
        });                                                
        me.callParent(arguments);
        var grid = me.down('gridpanel');
        grid.getSelectionModel().on('selectionchange', function(selModel, selections){
        	var selection = grid.getView().getSelectionModel().getSelection()[0];
        	grid.down('#downloadRevision').setDisabled(selection == null || !selection.get('payload-exists'));        	
        });
    }
});