Ext.define('FolderGridFinder.view.ui.UsersPopup', {
    extend: 'Ext.window.Window',
    requires: ['FolderGridFinder.model.User'],
    draggable: false,
    floating: true,
    height:450,
    width:600, 
    title: 'Manage Domain Users',
    modal: true,        
    initComponent: function() {
    	var me = this;
    	me.editing = Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToEdit: 1                             
        });      	
    	me.editing.on('validateedit',        	    		        	    	
        function (editor,event) {
			if(event.newValues.email != event.originalValues.email){
				Ext.MessageBox.alert('Access Denied', "You can't modify the email of an existing user.");
				event.cancel = true;
				return false;
			}				  						
    	});    	
    	me.domainUserStore = Ext.create('Ext.data.Store', {
            autoLoad: true,
            autoSync: true,
            storeId: 'domainUserStore',
            model: 'FolderGridFinder.model.User',
            sorters: [{
                property: 'email',
                direction: 'ASC'
            }],
            proxy: {
                type: 'rest',
                url: '/user/',
                reader: {
                    type: 'json'                                        
                },
                writer: {
                    type: 'json'                    
                },
                listeners: {
                    exception: {
                        fn: function (proxy,response,op) {
                        	//TODO: force global handler in Controls.js to handle this and avoid the duplicate code
                        	if(response.status == 401){
                            	Ext.Msg.alert({
                            		msg: 'You will now be redirected to login again.',
                            		title: 'Your session expired.',
                            		closable: false,
                            		buttons: 1,
                            		draggable: false,
                            		fn : function() {
                                		window.location = window.location; 
                                	}
                            	});
                        	}else{
                        		Ext.MessageBox.alert('Error', response.responseText);
                        		var updatedRecords = me.domainUserStore.getNewRecords();
                                for(var i=0; i<updatedRecords.length; i++){
                                	me.domainUserStore.remove(updatedRecords[i]);
                                }	
                        	}                    		                            
                    	}
                    }                    
                }
            } 
        });    	    	    	
        Ext.applyIf(me, {
            items: [
                    {
                    	xtype: 'gridpanel',
                    	store: 'domainUserStore',
                    	selType: 'rowmodel',
                    	autoScroll: true,
                    	multiSelect: true,
                    	iconCls: 'icon-grid',                    	
                        dockedItems: [{
                            xtype: 'toolbar',
                            items: [{
                                iconCls: 'icon-add',
                                text: 'Add',
                                scope: this,
                                handler: function(){  
                                	var form = Ext.widget('form', {
                                        layout: {
                                            type: 'vbox',
                                            align: 'stretch'
                                        },
                                        border: false,
                                        bodyPadding: 10,

                                        fieldDefaults: {
                                            labelAlign: 'top',
                                            labelWidth: 100,
                                            labelStyle: 'font-weight:bold'
                                        },
                                        defaults: {
                                            margins: '0 0 10 0'
                                        },

                                        items: [
										{xtype: 'component',
										    html: 'Complete the form to create a new user account. We\'ll send a short email directing the user to visit <a target="_blank" href="/reset">your domain\'s password reset page</a> to select a password and gain access.',
										    margin: '0 0 0 10'
										},
                                        {
                                            xtype: 'textfield',
                                            fieldLabel: 'Email Address',
                                            vtype: 'email',
                                            name: 'email',
                                            allowBlank: false
                                        }, {
                                            xtype: 'textfield',
                                            fieldLabel: 'First Name',
                                            name: 'firstname',
                                            allowBlank: false
                                        }, {
                                            xtype: 'textfield',
                                            fieldLabel: 'Last Name',
                                            name: 'lastname',
                                            allowBlank: false
                                        },
                                        {								
                                        	fieldLabel: 'Check to Suppress Welcome Email',            							    
                                        	name: 'suppressWelcomeEmail',            							    
            							    xtype: 'checkboxfield'            							    
            							}
                                        /* Disable password entry for new users - force reset process 
                                         * , {
                                            xtype: 'textfield',
                                            fieldLabel: 'Password',
                                            'securepword',
                                            name: 'password',
                                            allowBlank: false
                                        }*/],

                                        buttons: [{
                                            text: 'Cancel',
                                            handler: function() {
                                                this.up('form').getForm().reset();
                                                this.up('window').hide();
                                            }
                                        }, {
                                            text: 'Create User',
                                            handler: function() {
                                                if (this.up('form').getForm().isValid()) {
                                                	var theform = this.up('form').getForm();                                                	
                                                	var newUser = new FolderGridFinder.model.User();
                                                	theform.updateRecord(newUser);                                                	                                                	
                                                	me.domainUserStore.insert(0, newUser);
                                                	me.domainUserStore.sync();
                                                    theform.reset();
                                                    this.up('window').hide();                                                    
                                                }
                                            }
                                        }]
                                    });

                                    win = Ext.widget('window', {
                                        title: 'New User Form',                                        
                                        height:350,
                                        width:600, 
                                        minHeight: 300,
                                        layout: 'fit',
                                        resizable: true,
                                        modal: true,
                                        items: form
                                    });                                    
                                    win.show();
                                }
                            }, '-',  {
                                iconCls: 'icon-delete',
                                itemId: 'deleteUser',
                                text: 'Delete',
                                scope: this,
                                disabled: true,
                                handler: function(){
                                    var selection = grid.getView().getSelectionModel().getSelection()[0];
                                    if (selection) {
                                    	me.domainUserStore.remove(selection);
                                    }
                                }
                            }, '-', {
                                iconCls: 'icon-lock-edit',
                                itemId: 'editPasswd',
                                text: 'Edit Password',
                                scope: this,
                                disabled: true,
                                handler: function(){  
                                	var form = Ext.widget('form', {
                                        layout: {
                                            type: 'vbox',
                                            align: 'stretch'
                                        },
                                        border: false,
                                        bodyPadding: 10,

                                        fieldDefaults: {
                                            labelAlign: 'top',
                                            labelWidth: 100,
                                            labelStyle: 'font-weight:bold'
                                        },
                                        defaults: {
                                            margins: '0 0 10 0'
                                        },

                                        items: [
										{xtype: 'component',
										    html: 'Enter a new password for the selected user.',
										    margin: '0 0 0 10'
										},
                                        {
                                            xtype: 'textfield',
                                            fieldLabel: 'Password',
                                            name: 'password',
                                            allowBlank: false,
                                            minLength: 8,
                                            maxLength: 256
                                        }],

                                        buttons: [{
                                            text: 'Cancel',
                                            handler: function() {
                                                this.up('form').getForm().reset();
                                                this.up('window').hide();
                                            }
                                        }, {
                                            text: 'Override Password',
                                            handler: function() {
                                            	var theform = this.up('form').getForm();
                                                if (theform.isValid()) {                                                	                                               	
                                                	var selection = grid.getView().getSelectionModel().getSelection()[0];
                                                    if (selection) {
                                                    	var data = Ext.JSON.encode({"email":selection.get('email'),"password":theform.getFieldValues()['password']});
                                         	        	Ext.Ajax.request({
                                         	        	    method : "PUT",
                                         	        	    url: "/user/"+selection.get('email'),
                                         	        	    jsonData : data,
                                         	        	   success: function(){Ext.MessageBox.alert('Success', "Password successfully set for "+selection.get('email'));},
                                         	        	   failure: function(){Ext.MessageBox.alert('Error', "Unable to set password for "+selection.get('email'));}
                                         	        	});    	   
                                                    }                                 	
                                                    theform.reset();
                                                    this.up('window').hide();                                                    
                                                }
                                            }
                                        }]
                                    });

                                    win = Ext.widget('window', {
                                        title: 'New Password Form',                                        
                                        height:170,
                                        width:300, 
                                        minHeight: 170,
                                        layout: 'fit',
                                        resizable: true,
                                        modal: true,
                                        items: form
                                    });                                    
                                    win.show();
                                }
                            }]
                        }],                     	
                        plugins: [this.editing],
                        columns: [
							{
							    text: 'Email',
							    flex: 1,
							    sortable: true,							    
							    dataIndex: 'email',
							    field: {
							        xtype: 'textfield',
							        disabled: true
							    }							
							}, {
							    header: 'First Name',
							    width: 80,
							    sortable: true,
							    dataIndex: 'firstname',
							    field: {
							        xtype: 'textfield'
							    }
							}, {
							    text: 'Last Name',
							    width: 80,
							    sortable: true,
							    dataIndex: 'lastname',
							    field: {
							        xtype: 'textfield'
							    }
							},							
							{
							    text: 'Home Folder',
							    width: 75,
							    sortable: false,
							    dataIndex: 'homeDuid',
							    hidden: false,
							    field: {
							        xtype: 'textfield'
							    }
							},	
							{
                                dataIndex: 'lastLogin',
                                text: 'Last Login',  
                                width: 75,
                                renderer: function(value){
                                	if(value == 0 ) return "";
                                	var d = new Date(0); 
                                	d.setUTCMilliseconds(value);                                    	
                                    return d;
                                },
                                hidden: false,
                                sortable: true
                            },
							{								
							    text: 'Password Expired',
							    width: 40,
							    sortable: false,
							    dataIndex: 'passwordExpired',
							    hidden: true,
							    field: {
							        xtype: 'checkboxfield'
							    }
							},
                            {                               
                                text: 'Enabled',
                                width: 30,
                                sortable: false,
                                dataIndex: 'enabled',
                                hidden: true,
                                field: {
                                    xtype: 'checkboxfield'
                                }
                            }							
                        ]                           
                    }
                  ]                                    
        });                                                
        me.callParent(arguments);
        var grid = me.down('gridpanel');
        grid.getSelectionModel().on('selectionchange', function(selModel, selections){
        	grid.down('#deleteUser').setDisabled(selections.length === 0);
        	grid.down('#editPasswd').setDisabled(selections.length === 0);
        });
        me.editing.on('edit',        	    		        	    	
                function (editor,e) {
        			var selected = grid.getSelectionModel().getSelection();
        			for(i = 1;i<selected.length;i++){
        				for(var fieldName in e.originalValues) {
        					if(e.originalValues[fieldName] != e.newValues[fieldName]){        						
        						selected[i].set(fieldName,e.newValues[fieldName]);
        					}
    					}     				
        				//me.domainUserStore
        			}
            	});
    },
    layout : 'fit'        
});