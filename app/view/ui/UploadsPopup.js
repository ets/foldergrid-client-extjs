Ext.define('FolderGridFinder.view.ui.UploadsPopup', {
    extend: 'Ext.window.Window',
    requires: ['FolderGridFinder.model.File'],
    draggable: false,
    floating: true,
    layout: 'fit',
    width:600, 
    height: 500,
    title: 'Recently Uploaded File Listing',
    modal: true,    
    initComponent: function() {
    	var me = this;
    	me.uploadsStore = Ext.create('Ext.data.Store', {    
        	storeId: 'uploadsStore', 
        	model: 'FolderGridFinder.model.File',
        	proxy: {
                type: 'memory',
                reader: {
                    type: 'json',
                    root: 'hits'
                }
            },
        	data:[]
        });
        Ext.applyIf(me, {
            items: [
                    {
                    	xtype: 'gridpanel',
                    	selType: 'rowmodel',
                    	store: 'uploadsStore',
                    	multiSelect: false,
                    	autoScroll: true,                                            	
                        columns: [
								{
								    xtype: 'gridcolumn',
								    width: 200,
								    dataIndex: 'name',
								    flex: 1,
								    text: 'Name',
								    sortable: true
								},
                                {
                                    width: 120,
                                    dataIndex: 'creator',
                                    text: 'Owner',
                                    sortable: true,
                                    hidden: false
                                },                                                                
                                {
                                    width: 150,
                                    dataIndex: 'md5',
                                    text: 'MD5 Hash',
                                    sortable: true,
                                    hidden: true
                                },                                
                                {
                                    dataIndex: 'size',
                                    text: 'Size (bytes)',
                                    sortable: true
                                },
                                {
                                	width: 150,
                                    dataIndex: 'source-updated',
                                    text: 'Last Modified',  
                                    renderer: function(value){
                                    	if(value == 0 ) return "";
                                    	var d = new Date(0); 
                                    	d.setUTCMilliseconds(value);                                    	
                                        return d;
                                    },
                                    sortable: true,
                                    hidden: true
                                },
                                {
                                	width: 150,
                                    xtype: 'gridcolumn',
                                    dataIndex: 'created',
                                    text: 'Created',                                    
                                    sortable: true                                    
                                },                                
                                {
                                    dataIndex: 'type',
                                    text: 'Kind',
                                    sortable: true,
                                    hidden: true
                                },
                                {
                                	xtype: 'booleancolumn', 
                                    dataIndex: 'payload-exists',
                                    text: 'Loaded',
                                    trueText: 'Yes',
                                    falseText: 'No',
                                    sortable: true,
                                    hidden: true
                                }
                        ]                           
                    }
                  ]                                    
        });                                                
        me.callParent(arguments);        
    }
});