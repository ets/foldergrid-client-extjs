Ext.define('FolderGridFinder.view.ui.ExpirationPopup', {
    extend: 'Ext.window.Window',

    draggable: false,
    floating: true,
    height:400,
    width:500, 
    modal: true,        
    initComponent: function() {
    	var me = this;
    	var linkform = Ext.widget('form', {
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            border: false,
            bodyPadding: 10,

            fieldDefaults: {
                labelAlign: 'top',
                labelWidth: 100,
                labelStyle: 'font-weight:bold'
            },
            defaults: {
                margins: '0 0 10 0'
            },

            items: [
            {	xtype: 'label',
                forId: 'description',
                html: 'Assign an expiration to schedule the complete removal of a file for a future day. Only the file creator or a domain administrator may assign an expiration.<br><br>'+ 
                	'<b>Expirations bypass your domain\'s retention settings and result in a complete irreversible purging of all file content and metadata.</b>',
                margin: '0 0 20 10'
            },
            {	xtype: 'hiddenfield',
                name: 'expirationTime'                
            },
	        {
            	xtype: 'datepicker',
                minDate: Ext.Date.add(new Date(),Ext.Date.DAY,1),
                showToday: false,
                handler: function(picker, date) {
                	var theForm = this.up('form').getForm();
                	theForm.findField('expirationTime').setValue(date.getTime());
                	var button =  theForm.owner.query('button[text=Assign Expiration]');
                    button[0].enable();
                }
		    },{	xtype: 'label',
                html: 'Currently Assigned Expiration: None',
                margin: '0 0 20 10',
                listeners: {
                	beforerender: function (theLabel) {                		
                		if(currentlySelected instanceof Array){
                			theLabel.setVisible(false);
                		}else{                			
                			Ext.Ajax.request({
	                    	    method : "GET",
	                    	    url: "/file/expiration/"+currentlySelected.get('id'),
	                    	    success : function(response){
	                    	    	var jsonData = Ext.JSON.decode(response.responseText);
	                    	    	if(jsonData != null){
	                    	    		theLabel.setText("Currently Assigned Expiration: "+jsonData['expirationDate']);
	                    	    		theLabel.setVisible(true);
	                    	    		var button =  theLabel.up('form').getForm().owner.query('button[text=Delete Expiration]');
	                                    button[0].enable();   
	                    	    	}
	                    	    }
	                    	});
                		}
                    }
                }
            }],

            buttons: [{
                text: 'Cancel',
                handler: function() {
                    this.up('form').getForm().reset();
                    this.up('window').hide();
                }
            }, {
                text: 'Delete Expiration',
                disabled: true,
                handler: function() {
                	unexpire = _.rateLimit(function(target,alertOnSuccessMsg){    		
                		var linkUrl = "/file/expiration/"+target.get('id'); 	    	    		
                    	Ext.Ajax.request({
                    	    method : "DELETE",
                    	    url: linkUrl,                    	    
                            callback: function(options,success,response){
                            	if(response.status == 200){
                            		if(alertOnSuccessMsg){
                            			Ext.Msg.alert('Expiration Successfully Deleted', 'The expiration has been removed.');
                            		}
	                        	}else if(response.status == 401){
	                                Ext.Msg.alert({
	                                    msg: 'You will now be redirected to login again.',
	                                    title: 'Your session expired.',
	                                    closable: false,
	                                    buttons: 1,
	                                    draggable: false,
	                                    fn : function() {
	                                        window.location = window.location; 
	                                    }
	                                });                    
	                            }else if(response.status == 403){        	    	
	                    	    	Ext.Msg.alert('Access Denied', "You are not allowed to delete the expiration on ["+target.get('name')+"]");                                             
	                            }else {
	                                Ext.Msg.alert('Unknown Error', 'Your request for ['+target.get('name')+'] returned status code ['+response.status+']. Please contact support for assistance.');                                             
	                            }   
                    	    }
                    	}); 
                	}, fg_ratelimit);
                                    	
                	if(currentlySelected instanceof Array){
                		var numSelected = currentlySelected.length;
            			for(i=0;i<numSelected;i++){ 	    	    		
            				unexpire(currentlySelected[i],i + 1 == numSelected);
            			}	                    	        
                	}else{
                		unexpire(currentlySelected,true);
                	}
                    this.up('window').hide();                                                    
                    
                }
            }, {
                text: 'Assign Expiration',
                disabled: true,
                handler: function() {
                	expire = _.rateLimit(function(target,expirationTime,alertOnSuccessMsg){    		
                		var linkUrl = "/file/expiration/"+target.get('id'); 	    	    		
                    	Ext.Ajax.request({
                    	    method : "POST",
                    	    url: linkUrl,
                    	    params: {
                    	    	expirationTime: expirationTime
                    	    },
                            callback: function(options,success,response){
                            	if(response.status == 200){
                            		if(alertOnSuccessMsg){
                            			Ext.Msg.alert('Expiration Successfully Assigned', 'The expiration date you selected has been assigned.');
                            		}
	                        	}else if(response.status == 401){
	                                Ext.Msg.alert({
	                                    msg: 'You will now be redirected to login again.',
	                                    title: 'Your session expired.',
	                                    closable: false,
	                                    buttons: 1,
	                                    draggable: false,
	                                    fn : function() {
	                                        window.location = window.location; 
	                                    }
	                                });                    
	                            }else if(response.status == 403){        	    	
	                    	    	Ext.Msg.alert('Access Denied', "You are not allowed to assign an expiration to ["+target.get('name')+"]");                                             
	                            }else {
	                                Ext.Msg.alert('Unknown Error', 'Your request for ['+target.get('name')+'] returned status code ['+response.status+']. Please contact support for assistance.');                                             
	                            }   
                    	    }
                    	}); 
                	}, fg_ratelimit);
                	
                    if (this.up('form').getForm().isValid()) {
                    	var theform = this.up('form').getForm();
                    	var theformValues = theform.getFieldValues();
                    	
                    	if(currentlySelected instanceof Array){
                    		var numSelected = currentlySelected.length;
                			for(i=0;i<numSelected;i++){ 	    	    		
                				expire(currentlySelected[i],theformValues.expirationTime,i + 1 == numSelected);
                			}	                    	        
                    	}else{
                    		expire(currentlySelected,theformValues.expirationTime,true);
                    	}
                        theform.reset();
                        this.up('window').hide();                                                    
                    }
                }
            }]
        });    	
        Ext.applyIf(me, {
            items: [linkform]                                    
        });                                                
        me.callParent(arguments);
    },
    layout : 'fit'        
});