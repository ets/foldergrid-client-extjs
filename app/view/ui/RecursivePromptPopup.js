Ext.define('FolderGridFinder.view.ui.RecursivePromptPopup', {
    extend: 'Ext.window.Window',
    supportApplyContent:true,
    draggable: false,
    floating: true,
    width:550,     
    modal: true,   
    initComponent: function() {
    	var me = this;
    	var linkform = Ext.widget('form', {
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            autoScroll: true,
            border: false,
            bodyPadding: 10,

            fieldDefaults: {
                labelAlign: 'top',
                labelWidth: 100,
                labelStyle: 'font-weight:bold'
            },
            defaults: {
                margins: '0 0 10 0'
            },
            buttons: [{
                text: 'Cancel',
                handler: function() {         
                	me.cancelFunc();
                    me.close();
                }
            }, {
                text: 'Apply Change',
                handler: function() {                	                	           	                	                  	
                    me.continueFunc(Ext.getCmp('applyToContents').getValue(),Ext.getCmp('recurseSubfolders').getValue());
                    me.close();
                }
            }],
            items: [
	        {
		        xtype: 'checkboxfield',
		        anchor: '100%',
		        boxLabel: 'Apply recursively to all subfolders under this folder?',
		        visible: currentlySelected.get('duid'),
		        id: 'recurseSubfolders',
		        listeners: {
           		 change: function (box, newValue, oldValue) {
           			 if(box.getValue()){
           				 Ext.MessageBox.alert({
       	    					title: 'WARNING', 
       	    					modal: true,
       	    					buttons: Ext.Msg.OKCANCEL,
       	    					msg: 'By checking this box you have elected to apply any subsequent permissions changes to all folders under this folder. To support this request we will first load all the affected subfolders. <b>If you have many folders under this one the operations could take a long time!</b>',
   	    						closable: false,
       	    					fn: function(response){        	    						    	        	    						
       	    						if (response == 'cancel'){
       	    							box.setValue(false);	                    	    							
       	    						}else{
       	    							Ext.getCmp('permissions-popup').setDisabled(true);
       	    							var expandingDialog = Ext.Msg.show({
   	    								    title: 'Loading...',
   	    								    msg: 'Loading all subfolders. Please wait.',
   	    								    width: 300,	                    	    								    
   	    								    wait: true,
   	    								    icon: Ext.window.MessageBox.INFO
   	    								});
       	    							var origin = currentlySelected;
       	    							treeSelectionPopulatesGrid = false;
       	    							
       	    							var allNodesLoadedFn = function() {
       	    								Ext.getCmp('foldertree').getSelectionModel().select(origin,false,false);	                    	    									               	                    	    								
       	    								treeSelectionPopulatesGrid = true;	           
       	    								expandingDialog.close();
       	    								Ext.getCmp('permissions-popup').setDisabled(false);
       	    						    },	                    	    						    
       	    						    taskFn = function(node, callback) {	                    	    						        
       	    						        node.expand(false, function(childNodes) {	                    	    						            	                    	    						            
       	    						            for(var i=0;i<childNodes.length;i++) {
       	    						                queue.push(childNodes[i]);
       	    						            }                
       	    						            callback();	                    	    						            	                    	    						            
       	    						        });
       	    						    },
       	    						    queue = async.queue(taskFn, 5);
       	    						    queue.drain = allNodesLoadedFn;
       	    						    queue.push(origin);	                    	    						    	                    	    								                    	    	            				                    	    						
       	    						}
       	    					}
       	    			 });
           			 }			                				 
           		 }
               }
		    }, {
		        xtype: 'checkboxfield',
		        id: 'applyToContents',
		        anchor: '100%',
		        boxLabel: 'Also apply to files under each affected folder?',
		        name: 'contents',
		        hidden: !this.supportApplyContent
            }]
        });    	
        Ext.applyIf(me, {
            items: [linkform]                                    
        });                                                
        me.callParent(arguments);
    },
    layout : 'fit'        
});