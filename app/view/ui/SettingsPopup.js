Ext.define('FolderGridFinder.view.ui.SettingsPopup', {
    extend: 'Ext.window.Window',
    draggable: false,
    floating: true,
    height:475,
    width:600, 
    modal: true,        
    initComponent: function() {
    	var me = this;
    	var linkform = Ext.widget('form', {
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            border: false,
            autoScroll: true,
            bodyPadding: 10,
            fieldDefaults: {
                labelAlign: 'top',
                labelWidth: 100,
                labelStyle: 'font-weight:bold'
            },
            defaults: {
                margins: '0 0 20 0'
            },
            items: [
			{xtype: 'fieldset',
				title: 'File Settings',
				collapsible: true,
				defaults: {
				    anchor: '100%'
				},
				layout: 'anchor',
				items: [
					{xtype: 'label',
					    text: 'Select the time to retain deleted files before they are completely purged. Deleted files can be undeleted until a Domain Administrator explicitly purges them or until they are automatically purged based upon the file retention setting below.',
					    margin: '0 0 0 10'
					},
					{
					    xtype: 'numberfield',					    
					    fieldLabel: 'Retain deleted files for this many weeks after the last modified date',					    
					    name: 'deletedWTL',
					    margin: '5 10 0 0',
					    anchor: '100%',
					    value: 2,
					    maxValue: 52*100,
					    minValue: 2
					},
		            {
						xtype: 'checkboxfield',
						margin: '10 10 0 0',
					    boxLabel  : 'Enforce file locking? If file locking is enforced, then no user other than the lock holder will be allowed to provision a new version of a locked file. Both the lock holder and any domain administrator can remove an existing lock.',
					    name      : 'enforceFileLocks',
					    checked : false                
					},
		            {
						xtype: 'checkboxfield',
						margin: '10 10 0 0',
					    boxLabel  : 'Hide unreadable folders? If this is enabled, then users will not be able to see folders that they are unable to open. This is enabled by default for new Domains',
					    name      : 'hideUnreadableFolders',
					    checked : false                
					}
				 ]
			}, 
			{xtype: 'fieldset',
				title: 'Branding Settings',
				collapsible: true,
				defaults: {
				    anchor: '100%'			        				    
				},
				layout: 'anchor',
				items: [
					{
						xtype: 'checkboxfield',						
					    boxLabel  : 'Remove Default Branding? If branding is removed, custom styling should be managed through <a href="http://help.foldergrid.com/support/solutions/articles/5000587893-branding-with-your-logo-and-styles">the white labeling guide</a>.',
					    name      : 'whiteLabel',
					    checked : false                
					},
					{
				        xtype: 'textfield',
				        name: 'supportEmail',
				        fieldLabel: 'Custom Support Email Address',
				        allowBlank: true, 
				        vtype: 'email'
				    },
				    {
				        xtype: 'textfield',
				        name: 'supportURL',
				        fieldLabel: 'Custom User Documentation',
				        allowBlank: true, 
				        vtype: 'url'
				    }
				 ]
			}			
		    ],

            buttons: [{
                text: 'Cancel',
                handler: function() {
                    this.up('form').getForm().reset();
                    this.up('window').hide();
                }
            }, {
                text: 'Update Settings',
                handler: function() {
                    if (this.up('form').getForm().isValid()) {
                    	var theform = this.up('form').getForm();
                    	var theformValues = theform.getFieldValues();
                    	var linkUrl = "/domain/update"; 	    	    		
                    	Ext.Ajax.request({
                    	    method : "POST",
                    	    url: linkUrl,
                    	    params: {
                    	        deletedWTL: theformValues.deletedWTL,
                    	        enforceFileLocks: theformValues.enforceFileLocks,
                    	        hideUnreadableFolders: theformValues.hideUnreadableFolders,
                    	        whiteLabel: theformValues.whiteLabel,
                    	        supportEmail: theformValues.supportEmail,
                    	        supportURL: theformValues.supportURL
                    	    },
                            callback: function(options,success,response){
                            	if(response.status == 200){
                                	supportURL = 'mailto:'+theformValues.supportEmail;
                                	kbURL = theformValues.supportURL;
                            		Ext.Msg.show({
                            			msg: 'Domain Settings Updated for '+currentDomain,
                                		title: 'Success',
                            		    closable: true,
                            		    draggable: false                                 		
                            		}); 
	                        	}else if(response.status == 401){
	                                Ext.Msg.alert({
	                                    msg: 'You will now be redirected to login again.',
	                                    title: 'Your session expired.',
	                                    closable: false,
	                                    buttons: 1,
	                                    draggable: false,
	                                    fn : function() {
	                                        window.location = window.location; 
	                                    }
	                                });                    
	                            }else if(response.status == 403){        	    	
	                    	    	Ext.Msg.alert('Access Denied', "You are not allowed to update domain settings for "+currentDomain);                                             
	                            }else {
	                                Ext.Msg.alert('Unknown Error', 'Your request from the server returned status code ['+response.status+']. Please contact support for assistance.');                                             
	                            }   
                    	    }
                    	});                                                 	                                                	                    	
                        theform.reset();
                        this.up('window').hide();                                                    
                    }
                }
            }]
        });    	
    	Ext.Ajax.request({
    	    method : "GET",
    	    url: '/domain/show',
    	    success : function(response){        	    	
    	    	var jsonData = Ext.JSON.decode(response.responseText);    	    	
    	    	linkform.getForm().findField('deletedWTL').setValue(jsonData.deletedWTL);
    	    	linkform.getForm().findField('enforceFileLocks').setValue(jsonData.enforceFileLocks);
    	    	linkform.getForm().findField('hideUnreadableFolders').setValue(jsonData.hideUnreadableFolders);    	    	
    	    	linkform.getForm().findField('whiteLabel').setValue(jsonData.whiteLabel);
    	    	linkform.getForm().findField('supportEmail').setValue(jsonData.supportEmail);
    	    	linkform.getForm().findField('supportURL').setValue(jsonData.supportURL);    	    	
    	    },
    	    failure: function(response){        	    	
    	    	Ext.MessageBox.alert('Access Denied', "You are not allowed to read domain details for "+currentDomain);        	    	
    	    }
    	});
        Ext.applyIf(me, {
            items: [linkform]                                    
        });                                                
        me.callParent(arguments);
    },
    layout : 'fit'        
});