Ext.define('FolderGridFinder.view.ui.FileDetailsPopup', {
    extend: 'Ext.window.Window',

    draggable: false,
    floating: true,
    width:550,     
    modal: true,        
    initComponent: function() {
    	var me = this;
    	var linkform = Ext.widget('form', {
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            autoScroll: true,
            border: false,
            bodyPadding: 10,

            fieldDefaults: {
                labelAlign: 'top',
                labelWidth: 100,
                labelStyle: 'font-weight:bold'
            },
            defaults: {
                margins: '0 0 10 0'
            },
            buttons: [{
                text: 'Cancel',
                handler: function() {
                    this.up('form').getForm().reset();
                    this.up('window').hide();
                }
            }, {
                text: 'Save Changes',
                handler: function() {                	
                    if (this.up('form').getForm().isValid()) {
                    	var theform = this.up('form').getForm();                                                	
                    	theform.updateRecord(currentlySelected);
                    	var fileUpdateUrl = "/file/"+currentlySelected.get('id'); 
        	    		var data = Ext.JSON.encode({"comment":currentlySelected.get("comment")});
        	        	Ext.Ajax.request({
        	        	    method : "PUT",
        	        	    url: fileUpdateUrl,
        	        	    jsonData : data,        	        	    
        	        	    failure: function(response){	        	    
        	        	    	Ext.MessageBox.alert('Access Denied', "You are not allowed to update "+currentlySelected.get('name'));        	    	
        	        	    }
        	        	});    		                    	
                    	Ext.getCmp('file-listing').getSelectionModel().select(currentlySelected);
                        theform.reset();
                        this.up('window').hide();                                                    
                    }
                }
            }],
            items: [
	        {
		        xtype: 'displayfield',
		        anchor: '100%',
		        fieldLabel: 'Name',
		        name: 'name',
		        value: currentlySelected.get('name')
		    }, {
		        xtype: 'displayfield',
		        anchor: '100%',
		        fieldLabel: 'Updated',
		        name: 'updated',
		        value: currentlySelected.get('updated')
		    },{
		        xtype: 'displayfield',
		        anchor: '100%',
		        fieldLabel: 'Creator',
		        name: 'creator',
		        value: currentlySelected.get('creator')
		    },{
                xtype: 'displayfield',
                anchor: '100%',
		        fieldLabel: 'Permanent Identifier',
		        name: 'id',
		        value: currentlySelected.get('id')
            },{
            	xtype: 'component',
                anchor: '100%',
		        fieldLabel: 'Permanent Link',
		        name: 'directLink',
		        html: '<a target="_blank" href="/file/'+currentlySelected.get('id')+'?filename='+currentlySelected.get('name')+'">Direct link to '+currentlySelected.get('name')+'</a>'                            		        
	        },{
                xtype: 'textareafield',
                anchor: '100%',
                grow      : true,
		        fieldLabel: 'Unstructured Comment',
		        name: 'comment',
		        value: currentlySelected.get('comment')
            }]
        });    	
        Ext.applyIf(me, {
            items: [linkform]                                    
        });                                                
        me.callParent(arguments);
    },
    layout : 'fit'        
});