Ext.define('FolderGridFinder.view.ui.FolderDetailsPopup', {
    extend: 'Ext.window.Window',

    draggable: false,
    floating: true,
    width:550,     
    modal: true,        
    initComponent: function() {
    	var me = this;
    	var linkform = Ext.widget('form', {
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            autoScroll: true,
            border: false,
            bodyPadding: 10,

            fieldDefaults: {
                labelAlign: 'top',
                labelWidth: 100,
                labelStyle: 'font-weight:bold'
            },
            defaults: {
                margins: '0 0 10 0'
            },

            items: [
	        {
		        xtype: 'displayfield',
		        anchor: '100%',
		        fieldLabel: 'Name',
		        name: 'name',
		        value: currentlySelected.get('name')
		    }, {
		        xtype: 'displayfield',
		        anchor: '100%',
		        fieldLabel: 'Updated',
		        name: 'updated',
		        value: currentlySelected.get('updated')
		    },{
		        xtype: 'displayfield',
		        anchor: '100%',
		        fieldLabel: 'Creator',
		        name: 'creator',
		        value: currentlySelected.get('creator')
		    },{
                xtype: 'textfield',
                anchor: '100%',
		        fieldLabel: 'DUID',
		        name: 'duid',
		        readOnly: true,
		        value: currentlySelected.get('duid')
            },{
	        	xtype: 'component',
	        	anchor: '100%',
		        name: 'directLink',
		        html: '<a href="/show/'+currentlySelected.get('duid')+'.html">Direct link to '+currentlySelected.get('name')+'</a>'                            		        
	        }]
        });    	
        Ext.applyIf(me, {
            items: [linkform]                                    
        });                                                
        me.callParent(arguments);
    },
    layout : 'fit'        
});