Ext.define('FolderGridFinder.view.ui.DropzonePopup', {
    extend: 'Ext.window.Window',

    draggable: false,
    floating: true,
    height:200,
    width:515, 
    modal: true,        
    initComponent: function() {
    	var me = this;
    	var linkform = Ext.widget('form', {
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            border: false,
            bodyPadding: 10,

            fieldDefaults: {
                labelAlign: 'top',
                labelWidth: 100,
                labelStyle: 'font-weight:bold'
            },
            defaults: {
                margins: '0 0 10 0'
            },

            items: [
			{xtype: 'label',
			    forId: 'description',
			    text: 'A dropzone allows anyone who follows it to upload files to this folder anonymously. Recipients do not need a user account or any credentials.',
			    margin: '0 0 0 10'
			},
	        {
		        xtype: 'numberfield',
		        anchor: '100%',
		        fieldLabel: 'Hours Until Dropzone Expires',
		        name: 'hoursTL',
		        value: 24,
		        maxValue: 24*365*5,
		        minValue: 1
		    }],

            buttons: [{
                text: 'Cancel',
                handler: function() {
                    this.up('form').getForm().reset();
                    this.up('window').hide();
                }
            }, {
                text: 'Create Dropzone',
                handler: function() {
                    if (this.up('form').getForm().isValid()) {
                    	var theform = this.up('form').getForm();
                    	var theformValues = theform.getFieldValues();
                    	var linkUrl = "/folder/link/"+currentlySelected.get('duid'); 	    	    		
                    	Ext.Ajax.request({
                    	    method : "POST",
                    	    url: linkUrl,
                    	    params: {
                    	        hoursTL: theformValues.hoursTL
                    	    },
                            callback: function(options,success,response){
                            	if(response.status == 200){
	                    	    	var jsonData = Ext.JSON.decode(response.responseText);
	                    	    	win = Ext.widget('window', {
	                                    title: 'Expiring Dropzone Successfully Created',                                        
	                                    height:250,
	                                    width:800,                                     
	                                    layout: 'fit',
	                                    resizable: true,
	                                    modal: true,
	                                    items: [
	                                       {
	                                        xtype: 'form',                                        
	                                        layout: {
	                                            type: 'vbox',
	                                            align: 'stretch'
	                                        },
	                                        border: false,
	                                        bodyPadding: 10,
	
	                                        fieldDefaults: {
	                                            labelAlign: 'top',
	                                            labelWidth: 100,
	                                            labelStyle: 'font-weight:bold'
	                                        },
	                                        defaults: {
	                                            margins: '0 0 10 0'
	                                        },
	
	                                        items: [{
	                            	        	xtype: 'displayfield',                            		        
	                            		        fieldLabel: 'Your requested expiring dropzone link is',
	                            		        name: 'link',
	                            		        value: jsonData.link                            		        
	                            	        },{
	                            	        	xtype: 'component',
	                            	        	style: {
	                            	        		align: 'right'
	                            	        	},
	                            		        html: '<a href="mailto:replace-this@foldergrid.com?subject='+encodeURIComponent('Expiring link to a dropzone for '+currentlySelected.get('name'))+
	                            		        '&body='+encodeURIComponent('Follow this link to upload files '+jsonData.link)+'">Open Email Template</a>'                            		        
	                            	        }]
	                                       }
	                                     ]
	                                });                                    
	                                win.show();  
	                        	}else if(response.status == 401){
	                                Ext.Msg.alert({
	                                    msg: 'You will now be redirected to login again.',
	                                    title: 'Your session expired.',
	                                    closable: false,
	                                    buttons: 1,
	                                    draggable: false,
	                                    fn : function() {
	                                        window.location = window.location; 
	                                    }
	                                });                    
	                            }else if(response.status == 403){
	                            	currentlySelected.set('isACLWritable',false);
	                            	currentlySelected.set('isWritable',false);
	                    	    	Ext.Msg.alert('Access Denied', "You are not allowed full control of "+currentlySelected.get('name')+" and therefore are not allowed to create a dropzone for it.");                                             
	                            }else {
	                                Ext.Msg.alert('Unknown Error', 'Your request from the server returned status code ['+response.status+']. Please contact support for assistance.');                                             
	                            }   
                    	    }
                    	});                                                 	                                                	                    	
                        theform.reset();
                        this.up('window').hide();                                                    
                    }
                }
            }]
        });    	
        Ext.applyIf(me, {
            items: [linkform]                                    
        });                                                
        me.callParent(arguments);
    },
    layout : 'fit'        
});