Ext.define('FolderGridFinder.view.PermissionsPopup', {
    extend: 'FolderGridFinder.view.ui.PermissionsPopup',
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
    },
    processPermissionsWithRecursiveSupport: function(record,store,includeContent,recurseSubfolders){
    	
    	var recurse = function(origin,jsonencodeddata,successFn,errorFn){
    		var applyingDialog = Ext.Msg.show({
			    title: 'Applying...',
			    msg: 'Applying ACL Update. Please wait.',
			    width: 300,	                    	    								    
			    wait: true,			    
			    icon: Ext.window.MessageBox.INFO
			});    		   	    	                    	    						        	     
    	    var taskFn = function(node, callback) {
    	        rateLimitedApply(node,jsonencodeddata,callback);    	            	        
    	    },    	    
    	    rateLimitedApply = _.rateLimit(function(targetNode,jsonencodeddata,callback){    		    		    		
        		Ext.Ajax.request({
            	    method : "PUT",
            	    url: targetNode.get('duid') ? "/folder/acl/"+targetNode.get('duid') : "/file/acl/"+targetNode.get('id'),
            	    jsonData: jsonencodeddata,            	                	    
            	    callback : function(options,success,response){            	    
            	    	if(!success){
            	    		var failureMsg = null;
            	    		if(response.status == 404){
            	    			store.removeAt(0);
            	    			failureMsg = "The given grantee could not be found.";
                	    	}else if(response.status == 403){
                	    		failureMsg = "You are not permitted to perform that operation.";
                	    	}else{
                	    		failureMsg =  "Unable to complete requested operation on ["+targetNode.get('name')+"]";	
                	    	}        
            	    		queue.kill();//Stop operation completely on any error
            	    		applyingDialog.close();
            	    		errorFn(failureMsg);
            	    	}else if(success && recurseSubfolders){
	                		for (var i = 0; i < targetNode.childNodes.length; i++) {
	                			queue.push(targetNode.childNodes[i]);        			    			    			
	                		}
            	    	}            	    	
            	    	callback();  	    	
	        	    }
            	});            	
        	}, fg_ratelimit);    	    
	    	var queue = async.queue(taskFn, 5);
	    	queue.drain = function(){
	    		applyingDialog.close();
	    		successFn();	    			    		
	    	};
    	    queue.push(origin);		            		
    	}
    	
    	if(record.get('permission') == ''){		
			var jsonencodeddata = Ext.JSON.encode({"grantee":record.get('grantee'),"permission":record.modified.permission,"newPermission":null,"includeContent":includeContent});
			recurse(currentlySelected,jsonencodeddata,
			 function(){
    	    	store.remove(record);            	    	
    	    	Ext.MessageBox.alert('Success', 'Permission revoked from "'+record.get('grantee')+'"');	    	
    	     },
    	     function(failureMsg){
    	    	record.reject();    	    	
    	    	Ext.MessageBox.alert('Failure', failureMsg);
    	    });			        	    	        		        	    	        	        	    	        		    
    	}else{
    		var jsonencodeddata = Ext.JSON.encode({"grantee":record.get('grantee'),"permission":record.modified.permission,"newPermission":record.get('permission'),"includeContent":includeContent});
    		recurse(currentlySelected,jsonencodeddata,
			 function(){
    			record.commit();            	    	
    	    	Ext.MessageBox.alert('Success', 'Permission updated for"'+record.get('grantee')+'"');	    	
    	     },
    	     function(failureMsg){
    	    	record.reject();    	    	
    	    	Ext.MessageBox.alert('Failure', failureMsg);
    	    });			      	    	        		
    	} 
    	
    },
    processPermissions: function(record,store){
    	if(currentlySelected.get('duid')){
    		var processor = this;
    		var detailsPopup = Ext.create('FolderGridFinder.view.ui.RecursivePromptPopup', {
        		title: 'Scope of Folder ACL Update',
                renderTo: Ext.getBody(),
                cancelFunc: function(){
                	record.reject();
                	processor.close();
                },
                continueFunc: function (includeContent,recurseSubfolders) {
          			 processor.processPermissionsWithRecursiveSupport(record,store,includeContent,recurseSubfolders);              		 
                }
            });	    		
    		detailsPopup.show();    			
		}else{
			this.processPermissionsWithRecursiveSupport(record,store,false,false);
		}    		
    }        
});