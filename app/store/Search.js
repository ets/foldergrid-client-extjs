/**
 * Store for keeping search results.
 */
Ext.define('FolderGridFinder.store.Search', {
    extend: 'Ext.data.Store',    
    requires: ['FolderGridFinder.model.Folder'],
    model: 'FolderGridFinder.model.Folder',
    fields: ['duid', 'name', 'created', 'updated','type','size','parentDuid','id'],
    proxy: {
        type: 'memory',
        reader: {
            type: 'json'
        }
    }
});