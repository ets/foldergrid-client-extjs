/**
 * Store for keeping search results.
 */
Ext.define('FolderGridFinder.store.Favorites', {
    extend: 'Ext.data.Store',    
    requires: ['FolderGridFinder.model.Folder'],
    model: 'FolderGridFinder.model.Folder',
    fields: ['duid', 'name'],
    idProperty:'duid',             
    autoLoad: true,
    proxy: {
         type: 'rest',
         id: 'duid',
         url : '/user/'+currentUsername,
         reader: {
             type: 'json',
             root: 'favorites'
         }
    } 
});