/**
 * Store for keeping search results.
 */
Ext.define('FolderGridFinder.store.Groups', {
    extend: 'Ext.data.Store',    
    requires: ['FolderGridFinder.model.Group'],
    model: 'FolderGridFinder.model.Group',
    autoLoad: false,
    autoSync: true,    
    sorters: [{
        property: 'name',
        direction: 'ASC'
    }],
    proxy: {
        type: 'rest',
        url: '/group/',
        reader: {
            type: 'json'                                
        },
        writer: {
            type: 'json'                    
        },
        listeners: {
            exception: {
                fn: function (proxy,response,op) {
                	//TODO: force global handler in Controls.js to handle this and avoid the duplicate code
                	if(response.status == 401){
                    	Ext.Msg.alert({
                    		msg: 'You will now be redirected to login again.',
                    		title: 'Your session expired.',
                    		closable: false,
                    		buttons: 1,
                    		draggable: false,
                    		fn : function() {
                        		window.location = window.location; 
                        	}
                    	});
                	}else if(response.status == 403){
                        	//It's okay if the current user can not load all groups                    	
                	}else{
                		Ext.MessageBox.alert('Error', response.responseText);
                	}                    		                            
            	}
            }                    
        }
    }   
});