
Ext.define('FolderGridFinder.model.Ace', {
    extend: 'Ext.data.Model',
    fields: [
             {name: 'id', type:'string', convert: function(value,record) {
            	 return record.get('grantee') +'|'+ record.get('permission')
             	}
             },
             {name: 'grantee', type: 'string'},
             {name: 'permission', type: 'string'}
             ]
});