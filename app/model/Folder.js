
Ext.define('FolderGridFinder.model.Folder', {
    extend: 'FolderGridFinder.model.File',
    
    fields: [
             {name: 'duid', type: 'string'},
             {name: 'etag', type: 'string', defaultValue: ''},
             {name: 'lastRefreshed', type: 'number'},
             {name: 'subfoldersInherit', type: 'boolean', defaultValue: false}             
             ],
    idProperty:'duid',             
    proxy: {
         type: 'rest',
         id: 'duid',
         extraParams: {'excludeFiles':'true','folders_limit':'1000'},
         url : '/folder',
         appendId: true,
         reader: {
             type: 'json',                      
             getResponseData: function(response) {
             	var jsonData, fileData, error;

                 try {
                     jsonData = Ext.JSON.decode(response.responseText); 
                     jsonData['children'] = jsonData['folders'];             
                     jsonData['etag'] = response.getAllResponseHeaders()['etag'];
                     return this.readRecords(jsonData);
                 } catch (ex) {
                     error = new Ext.data.ResultSet({
                         total  : 0,
                         count  : 0,
                         records: [],
                         success: false,
                         message: ex.message
                     });
                     this.fireEvent('exception', this, response, error);
                     Ext.Logger.warn('Unable to parse the JSON returned by the server');
                     return error;
                 }              
             }                    
         }    
    },
    hasMany: [
              {model: 'FolderGridFinder.model.Folder', name: 'folders' },
              {model: 'FolderGridFinder.model.File', name: 'files' }
              ]   
    
});