
Ext.define('FolderGridFinder.model.Group', {
    extend: 'Ext.data.Model',
    fields: [
             {name: 'name', type: 'string'}             
             ],
    idProperty:'name',
    hasMany: [
      {model: 'FolderGridFinder.model.User', name: 'users' }
      ],
      proxy: {
          type: 'rest',
          url: '/group/',
          reader: {
              type: 'json'                                
          },
          writer: {
              type: 'json'                    
          }
      }
});