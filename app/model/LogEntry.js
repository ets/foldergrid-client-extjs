
Ext.define('FolderGridFinder.model.LogEntry', {
    extend: 'Ext.data.Model',
    fields: [
             {name: 'action', type: 'string'},
             {name: 'changed', type: 'string'},
             {name: 'file', type: 'string'},
             {name: 'time', type: 'date'},
             {name: 'actor', type: 'string'}
             ],
    idProperty:'time',    
      proxy: {
          type: 'memory',
          url: '/file/log/',
          reader: {
              type: 'json'                                
          },
          writer: {
              type: 'json'                    
          }
      }
});