
Ext.define('FolderGridFinder.model.User', {
    extend: 'Ext.data.Model',
    fields: [
             {name: 'email', type: 'string'},
             {name: 'firstname', type: 'string'},             
             {name: 'lastname', type: 'string'},
             {name: 'homeDuid', type: 'string'},
             {name: 'lastLogin', type: 'number'},
             {name: 'enabled', type: 'boolean', defaultValue: true},
             {name: 'passwordExpired', type: 'boolean', defaultValue: false},
             {name: 'suppressWelcomeEmail', type: 'boolean', defaultValue: false},
             {name: 'password', type: 'string',defaultValue:null}
             ],
    idProperty:'email'                    
});