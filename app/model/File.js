
Ext.define('FolderGridFinder.model.File', {
    extend: 'Ext.data.Model',
    fields: [
             {name: 'id', type: 'number'},
             {name: 'revision-id', type: 'number'},
             {name: 'name', type: 'string'},
             {name: 'type', type: 'string'},
             {name: 'comment', type: 'string', defaultValue: ''},
             {name: 'md5', type: 'string'},
             {name: 'size', type: 'string', defaultValue: ''},
             {name: 'source-updated', type: 'number'},
             {name: 'source-created', type: 'number'},
             {name: 'updated', type: 'date'},
             {name: 'created', type: 'date'},
             {name: 'creator', type: 'string'},
             {name: 'lockHolder', type: 'string'},
             {name: 'parentDuid', type: 'string'},
             {name: 'payload-exists', type: 'boolean', defaultValue: false},             
             {name: 'isACLReadable', type: 'boolean', defaultValue: true},
             {name: 'isACLWritable', type: 'boolean', defaultValue: true},
             {name: 'isWritable', type: 'boolean', defaultValue: true},
             {name: 'isReadable', type: 'boolean', defaultValue: true},
             {name: 'isRemovable', type: 'boolean', defaultValue: true}             
             ],
    idProperty:'id',             
    proxy: {
        type: 'rest',
        url : '/file/',
        reader: {
            type: 'json'
        }
    }
});