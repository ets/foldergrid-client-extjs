
Ext.define('FolderGridFinder.controller.Controls', {
    extend: 'Ext.app.Controller',
    requires: ['Ext.data.TreeStore','FolderGridFinder.view.MyViewport','Ext.tree.plugin.TreeViewDragDrop',
               'Ext.tree.Panel','Ext.layout.container.Border','Ext.form.field.Trigger','Ext.layout.container.Card'],
    models: ['Folder','File','Ace','User','Group'],
    stores: ['Favorites','Watches','Groups'],
	init: function() {
		me = this;
		currentlySelected = null;		
		clipBoard = null;
		treeSelectionPopulatesGrid = true;		
	    this.control({
	        'finderViewport > container > treepanel': {
	            itemcontextmenu: this.itemContextMenu,
	            beforeitemdblclick: function() { return false; },
	            celldblclick: function() { return false; },
	            selectionchange: this.treeSelectionChange,
	            select: this.treeSelect
	        },
	        'finderViewport > container > gridpanel': {
	            itemdblclick: this.dblClick,	            
	            itemcontextmenu: this.itemContextMenu,
	            itemclick: function(view, record) {	            	
	            	Ext.getCmp('search-field').clearAndClose();                	                   
                },
	            selectionchange: this.gridSelectionChange,
	            select: this.gridSelect
	        },
	        //'trashBinPopup > gridpanel > toolbar > button': { },	        
	        '#search-dropdown': {
                itemclick: function(dropdown, record) {
                	this.processSearchSelection(record);                	                   
                },
                itemcontextmenu: function(view, record, item, index, ev) {
                	finderViewport.down('gridpanel').getSelectionModel().deselectAll();
                	currentlySelected = record;
                	this.itemContextMenu(view, record, item, index, ev);    
                }
            },
            '#search-field': {
                keyup: function(el, ev) {                	
                	if (ev.keyCode === Ext.EventObject.ENTER) {
                        ev.preventDefault();
                        var record = Ext.getCmp('search-dropdown').getSelectionModel().getLastSelected();
                        record && this.processSearchSelection(record);
                    }                	
                }
            }            
	    });        	    
        /*
         * Global check for session timout after app is launched
         */
        Ext.Ajax.on('requestexception', function (conn, response, options) {
        	if(response.status == 403 || response.status == 404) return; //handle by the invoker
        	
        	if(response.status == 401){
            	Ext.Msg.alert({
            		msg: 'You will now be redirected to login again.',
            		title: 'Your session expired.',
            		closable: false,
            		buttons: 1,
            		draggable: false,
            		fn : function() {
                		window.location = window.location; 
                	}
            	});
        	}else if(response.status == 429) {
        		fg_ratelimit += 50;
        		Ext.Msg.show({
        			msg: 'In the past 60 seconds your request count rate has exceeded the maximum we allow from a single user.<br>Please pause your activity temporarily and try again later.',
            		title: 'Too Many Requests',
        		     buttons: Ext.Msg.OK,
        		     closable: true,
        		     draggable: false
        		});            	        		        		
        	}else if(response.status >= 400) {
        		Ext.Msg.show({
        			msg: 'Please contact customer support for assistance with error: '+response.status,
            		title: 'Account configuration error!',
        		     buttons: Ext.Msg.YESNO,
        		     closable: true,
        		     draggable: false,
             		fn : function(btn, text) {
             			if (btn == 'ok'){
             				window.location = "http://help.foldergrid.com/"; //TODO: make this configurable
             		    }else{
             		    	this.close();
             		    }             			 
                 	}
        		});            	        		        		
        	} 
        });         
                
        treeStore = Ext.create('Ext.data.TreeStore', {    
        	storeId: 'folderTreeStore',
            model: 'FolderGridFinder.model.Folder',          
            sorters: [{
                property: 'name'
            }],
            autoLoad: false,            
            proxy: {            	
            	id: 'duid',
                type: 'rest', // use 'jsonp' with a full url for a cross-domain request
                extraParams: {'excludeFiles':'true','folders_limit':'5000'},
                url: '/folder',
                appendId: true,
                reader: {
                    type: 'json',                      
                    getResponseData: function(response) {
                    	var jsonData, fileData, error;
                        try {
                            jsonData = Ext.JSON.decode(response.responseText); 
                            jsonData['children'] = jsonData['folders'];                                          
                            jsonData['etag'] = response.getAllResponseHeaders()['etag'];
                            return this.readRecords(jsonData);
                        } catch (ex) {
                            error = new Ext.data.ResultSet({
                                total  : 0,
                                count  : 0,
                                records: [],
                                success: false,
                                message: ex.message
                            });
                            this.fireEvent('exception', this, response, error);
                            Ext.Logger.warn('Unable to parse the JSON returned by the server');
                            return error;
                        }              
                    }                    
                }        
            },
	        listeners: {
	        	beforeappend: function( thisNode, newChildNode, index, eOpts ) {
	            	if(thisNode && thisNode.findChild('name',newChildNode.get('name'))){
	            		return false; //ignore dupe folders
	            	}	                
	            },
	            beforeexpand: function(thisNode) {
	            	if(thisNode.isLoading() || thisNode.isExpanded() || thisNode.isExpanding){
	            		return false;
	            	}
	            	thisNode.isExpanding = true;
	            },
	            expand: function( thisNode, newChildNode, index, eOpts ) {
	            	thisNode.isExpanding = false;	                
	            },
	            append: function( thisNode, newChildNode, index, eOpts ) {
	            	if(newChildNode && newChildNode.get('name') == "Trash Bin"){
	            		newChildNode.set('iconCls', 'iconTrashcan');	
	            	}	                
	            },
	            load: function(store, node, records, successful, eOpts ){
	            	node.set('lastRefreshed',(new Date).getTime());
	            }		            	            
	        }
        });   
        treeStore.load = _.rateLimit(treeStore.load, fg_ratelimit);
        
        finderViewport = Ext.create('FolderGridFinder.view.MyViewport', {
            renderTo: Ext.getBody()
        });
        var treepanel = finderViewport.down('treepanel');
        //Load root node manually to configure tree
        var Folder = Ext.ModelMgr.getModel('FolderGridFinder.model.Folder');
        Folder.load(rootDuid,{
        	success: function(folder) {
                treeStore.setRootNode({
                    name: folder.get('name'),
                    duid: folder.get('duid'),
                    updated: folder.get('updated'),
                    parentDuid: folder.get('parentId'),
                    etag: folder.get('etag'),
                    leaf: false,
                    expanded: false
                }); 
                treepanel.getSelectionModel().select(treeStore.getRootNode());                
            },
            failure:function(folder, operation) {
                if(operation.error.status == 403){
                	Ext.MessageBox.alert('Access Denied', 'Sorry, you do not have permission to read your home folder for this domain. Please contact your domain administrator for assistance.');
                }                                          
            }
        });
        
        var toolbar = Ext.getCmp('fg-toolbar');
        toolbar.getComponent('downloadBtn').addListener('click',this.downloadClick);
        toolbar.getComponent('uploadBtn').addListener('click',this.uploadClick);
        var fileBtn = toolbar.getComponent('fileBtn');        
        fileBtn.addListener('click',this.fileMenuClick);        
        fileBtn.menu.getComponent('viewBtn').addListener('click',this.viewClick);
        fileBtn.menu.getComponent('openFileBtn').addListener('click',this.openSelectedFile);        
        fileBtn.menu.getComponent('renameBtn').addListener('click',this.renameClick);
        fileBtn.menu.getComponent('copyBtn').addListener('click',this.copyClick);
        fileBtn.menu.getComponent('pasteBtn').addListener('click',this.pasteClick);
        fileBtn.menu.getComponent('compressBtn').addListener('click',this.compressClick);        
        fileBtn.menu.getComponent('permissionsBtn').addListener('click',this.permissionsClick);
        fileBtn.menu.getComponent('linkBtn').addListener('click',this.linkClick);           
        fileBtn.menu.getComponent('newFolderBtn').addListener('click',this.newFolderClick);
        fileBtn.menu.getComponent('revisionsBtn').addListener('click',this.revisionsClick);
        fileBtn.menu.getComponent('logBtn').addListener('click',this.logClick);      
        fileBtn.menu.getComponent('expireBtn').addListener('click',this.expireClick);
        fileBtn.menu.getComponent('openFolderBtn').addListener('click',this.openSelectedFolder);
        fileBtn.menu.getComponent('deleteBtn').addListener('click',this.deleteClick);
        fileBtn.menu.getComponent('restoreBtn').addListener('click',this.restoreClick);        
        fileBtn.menu.getComponent('undeleteBtn').addListener('click',this.undeleteClick);        
        fileBtn.menu.getComponent('showDetailsBtn').addListener('click',this.showDetailsClick);        
        fileBtn.menu.getComponent('mapFolderBtn').addListener('click',this.mapFolderClick);
        fileBtn.menu.getComponent('toggleWatchBtn').addListener('click',this.toggleWatchClick);
        fileBtn.menu.getComponent('toggleLockBtn').addListener('click',this.toggleLockClick);        
        
        var goBtn = toolbar.getComponent('goBtn');
        goBtn.addListener('click',this.goMenuClick);
        goBtn.menu.getComponent('addGoBtn').addListener('click',this.goAddClick);
        goBtn.menu.getComponent('delGoBtn').addListener('click',this.goDelClick);
        goBtn.menu.getComponent('goHomeBtn').addListener('click',this.goClick);        
        var userBtn = toolbar.getComponent('userBtn');
        userBtn.menu.getComponent('foldersyncBtn').addListener('click',this.installFolderSync);
        
        var favorites = me.getStore('Favorites');
        favorites.addListener('load',function(store,records,successful){
        	var favCount = records.length;
    		for(i=0;i<favCount;i++){
    			var fav = favorites.getAt(i);						
    			goBtn.menu.add(Ext.create('Ext.menu.Item', {
    	        	text: fav.get('name'),
    	        	duid: fav.get('duid'),
    	        	itemId: fav.get('duid'),
    	        	iconCls: 'icon-go-heart',
    	        	listeners: {
               		 click: function (node, data) {
               			me.goClick(node,data);
               		 }
                    }
    	        }));
    		}
    		store.clearListeners();
        });		
                
        /*TODO: Not binding...
        var keymap = Ext.create('Ext.util.KeyMap', {
        	el: this,
            renderTo: Ext.getBody(),
            binding: {
                key: "f",
                alt:true,                 
                fn: function(){ fileBtn.fireEvent('click'); }
            }
        });
        */        
        var toolBtn = toolbar.getComponent('toolBtn');
        toolBtn.menu.getComponent('mngUsersBtn').addListener('click',this.manageUsersClick);
        toolBtn.menu.getComponent('mngGroupsBtn').addListener('click',this.manageGroupsClick);
        toolBtn.menu.getComponent('mngTrashBinBtn').addListener('click',this.manageTrashClick);        
        toolBtn.menu.getComponent('mngSettingsBtn').addListener('click',this.manageSettingsClick);
        toolBtn.menu.getComponent('viewUploadReport').addListener('click',this.viewUploadReportClick);
        /*toolBtn.menu.getComponent('viewStorageBtn').addListener('click',this.viewStorageBtnClick); */                
        
        fileContextMenu = new Ext.menu.Menu({
    	  items: [
    	    {itemId: 'openFileBtn',text: 'Open Locally',iconCls: 'icon-file-edit', handler: this.openSelectedFile },
			{itemId: 'viewBtn',text: 'Browser View',iconCls: 'icon-view', handler: this.viewClick },			
			{ xtype: 'menuseparator' },
			{itemId: 'copyBtn',text: 'Copy',iconCls: 'icon-copy', handler: this.copyClick},			
			{itemId: 'renameBtn',text: 'Rename',iconCls: 'icon-edit', handler: this.renameClick},			
			{itemId: 'showDetailsBtn',text: 'Show Info',iconCls: 'icon-folder-details', handler: this.showDetailsClick},
			{itemId: 'compressBtn',text: 'Compress',iconCls: 'icon-compress', handler: this.compressClick},
			{itemId: 'revisionsBtn',text: 'Show Revisions',iconCls: 'icon-revisions', handler: this.revisionsClick},			
			{itemId: 'logBtn',text: 'Show Audit Log',iconCls: 'icon-audit-log', handler: this.logClick},
			{itemId: 'toggleLockBtn',text: 'Lock',hidden: !lockingEnabled,iconCls: 'icon-file-lock', handler: this.toggleLockClick},
			{itemId: 'linkBtn',text: 'Share Publicly',disabled: true,iconCls: 'icon-file-share', handler: this.linkClick},
			{itemId: 'permissionsBtn',text: 'Permissions',iconCls: 'icon-lock', handler: this.permissionsClick},
			{ xtype: 'menuseparator' },
			{itemId: 'deleteBtn',text: 'Delete',iconCls: 'icon-delete', handler: this.deleteClick},
			{itemId: 'expireBtn',text: 'Expire',iconCls: 'icon-expire', handler: this.expireClick}			
    	  ]
    	});
        folderContextMenu = new Ext.menu.Menu({
      	  items: [  			
  			{itemId: 'newFolderBtn',text: 'New Folder',iconCls: 'icon-folder-new', handler: this.newFolderClick},
  			{itemId: 'openFolderBtn',text: 'Open Here',iconCls: 'icon-folder-open', handler: this.openSelectedFolder },
  			{ xtype: 'menuseparator' },
  			{itemId: 'copyBtn',text: 'Copy',iconCls: 'icon-copy', handler: this.copyClick},
  			{itemId: 'pasteBtn',text: 'Paste',iconCls: 'icon-paste', handler: this.pasteClick},  			
  			{itemId: 'renameBtn',text: 'Rename',iconCls: 'icon-edit', handler: this.renameClick},
  			{itemId: 'showDetailsBtn',text: 'Show Info',iconCls: 'icon-folder-details', handler: this.showDetailsClick},
  			{itemId: 'mapFolderBtn',text: 'Sync Locally',iconCls: 'icon-clone', handler: this.mapFolderClick},
  			{itemId: 'toggleWatchBtn',text: 'Watch Folder',iconCls: 'icon-watch', handler: this.toggleWatchClick},
  			{itemId: 'logBtn',text: 'Show Audit Log',iconCls: 'icon-audit-log', handler: this.logClick},
  			{itemId: 'linkBtn',text: 'Share Publicly',disabled: true,iconCls: 'icon-file-share', handler: this.linkClick},
  			{itemId: 'permissionsBtn',text: 'Permissions',iconCls: 'icon-lock', handler: this.permissionsClick},
  			{ xtype: 'menuseparator' },
  			{itemId: 'undeleteBtn',text: 'Show Deleted',iconCls: 'icon-undelete', handler: this.undeleteClick},
  			{itemId: 'restoreBtn',text: 'Restore to Home',iconCls: 'icon-restore', handler: this.restoreClick, visible: false},
  			{itemId: 'deleteBtn',text: 'Delete',iconCls: 'icon-delete', handler: this.deleteClick}  			          
      	  ]
      	});        
                
        treepanel.getView().on('beforedrop', function (node, data, overModel, dropPosition, dropHandler) {
    		if(data.records.length > 1){
    			droppedName = data.records.length+' selected items'    			
        		Ext.MessageBox.show({
        			title: 'Are you certain?', 
        			msg: 'Do you want to move the ['+droppedName+'] under ['+overModel.get('name')+']?',
        			buttons: Ext.Msg.YESNO,
        			closable: false,
        			fn: function(btn){
        				if (btn == 'yes'){                            
                            var newParentDuid = overModel.get('parentId');
                            if(overModel.get('duid')){
                            	overModel.expand();
                            	newParentDuid = overModel.get('duid');
                	    	}
                            var reqJson = Ext.JSON.encode({"parentDuid":newParentDuid});    	                        
                            var numSelected = data.records.length;
                            var numRemaining = numSelected;
                            var allSucceeded = true;
        					for(i=0;i<numSelected;i++){
        						var droppedNode = data.records[i];        						
    	            	    	var moveUrl = "/file/"+droppedNode.get('id'); 
    	            	    	if(droppedNode.get('duid')){
    	            	    		moveUrl = "/folder/"+droppedNode.get('duid');
    	            	    	}	        	  
    	        	        	Ext.Ajax.request({
    	        	        	    method : "PUT",
    	        	        	    url: moveUrl,
    	        	        	    jsonData : reqJson,
    	        	        	    success : function(){    	        	
    	        	        	    	if(i + 1 >= numSelected){
    	        	        	    		finderViewport.down('treepanel').getSelectionModel().select(overModel);
    	        	        	    		if(droppedNode.get('duid')){
    	        	        	    			dropHandler.processDrop();
    	        	            	    	}else{
    	        	            	    		dropHandler.cancelDrop();
    	        	            	    	}	        	  
    	        	        	    	}    	        	        	    	
    	        	        	    },
    	        	        	    failure : function(){
    	        	        	    	allSucceeded = false;
    	        	        	    	if(i + 1 >= numSelected){
    	        	        	    		finderViewport.down('treepanel').getSelectionModel().select(overModel);
    	        	        	    		dropHandler.cancelDrop();
    	        	        	    	}    	        	        	    	
    	        	        	    },
    	        	        	    callback : function(){
    	        	        	    	numRemaining -= 1;
    	        	        	    	if(numRemaining == 0 && !allSucceeded){
    	    	        	        		Ext.MessageBox.alert('Forbidden', 'You do not have permission to move the ['+droppedName+'] under ['+overModel.get('name')+']. Either you have no permission to write to the destination folder or file(s) of the same name already exist.');	
    	    	        	        	}
    	        	        	    }
    	        	        	});    				    	        	        	
        					}        						        	                					
        				}
        			}
            	});
    		}else{
    			var droppedNode = data.records[0];        		
    	    	var droppedName = droppedNode.get('name');
            	Ext.MessageBox.show({
    				title: 'Are you certain?', 
    				msg: 'Do you want to move ['+droppedName+'] under ['+overModel.get('name')+']?',
    				buttons: Ext.Msg.YESNO,
    				closable: false,
    				fn: function(btn){
    					if (btn == 'yes'){
                            var newParentDuid = overModel.get('parentId');
                            if(overModel.get('duid')){
                            	overModel.expand();
                            	newParentDuid = overModel.get('duid');
                	    	}
                            var data = Ext.JSON.encode({"parentDuid":newParentDuid});
                            
                	    	var moveUrl = "/file/"+droppedNode.get('id'); 
                	    	if(droppedNode.get('duid')){
                	    		moveUrl = "/folder/"+droppedNode.get('duid');
                	    	}	        	    		
            	        	Ext.Ajax.request({
            	        	    method : "PUT",
            	        	    url: moveUrl,
            	        	    jsonData : data,
            	        	    success : function(){            	        	    	
            	        	    	if(droppedNode.get('duid')){
        	        	    			dropHandler.processDrop();
        	            	    	}else{
        	            	    		dropHandler.cancelDrop();
        	            	    	}
            	        	    	finderViewport.down('treepanel').getSelectionModel().select(overModel);
            	        	    },
            	        	    failure : function(){
            	        	    	dropHandler.cancelDrop();
            	        	    	Ext.MessageBox.alert('Forbidden', 'You do not have permission to move ['+droppedName+'] under ['+overModel.get('name')+']. Either you have no permission to write to the destination folder or a file of the same name already exists.');
            	        	    }
            	        	});	             	        	
    					}
    				}
    			});        			
    		}        
    		dropHandler.wait = true;
        	return false;           	
        } );
        finderViewport.down('gridpanel').getView().on('beforedrop', function (node, data, overModel, dropPosition, drop) {
    		if(data.records.length > 1){
    			droppedName = data.records.length+' selected items'
    			
        		Ext.MessageBox.show({
        			title: 'Are you certain?', 
        			msg: 'Do you want to move the ['+droppedName+'] under ['+overModel.get('name')+']?',
        			buttons: Ext.Msg.YESNO,
        			closable: false,
        			fn: function(btn){
        				if (btn == 'yes'){
                            var newParentDuid = overModel.get('parentId');
                            if(overModel.get('duid')){
                            	overModel.expand();
                            	newParentDuid = overModel.get('duid');
                	    	}
                            var reqJson = Ext.JSON.encode({"parentDuid":newParentDuid});    	                        
                            var numSelected = data.records.length;
                            var numRemaining = numSelected;
                            var allSucceeded = true;
        					for(i=0;i<numSelected;i++){
        						var droppedNode = data.records[i];
    	            	    	var moveUrl = "/file/"+droppedNode.get('id'); 
    	            	    	if(droppedNode.get('duid')){
    	            	    		moveUrl = "/folder/"+droppedNode.get('duid');
    	            	    	}	        	    		
    	        	        	Ext.Ajax.request({
    	        	        	    method : "PUT",
    	        	        	    url: moveUrl,
    	        	        	    jsonData : reqJson,
    	        	        	    success : function(){
    	        	        	    	drop();
    	        	        	    },
    	        	        	    failure : function(){
    	        	        	    	allSucceeded = false;
    	        	        	    },
    	        	        	    callback : function(){
    	        	        	    	numRemaining -= 1;
    	        	        	    	if(numRemaining == 0 && !allSucceeded){
    	        	        	    		Ext.MessageBox.alert('Forbidden', 'You do not have permission to perform that move. Either you have no permission to write to the destination folder or a file of the same name already exists.');	
    	    	        	        	}
    	        	        	    }
    	        	        	});    						
        					}    															
        				}
        			}
            	});
    		}else{
    			var droppedNode = data.records[0];
        		var droppedName = droppedNode.get('name');
            	Ext.MessageBox.show({
    				title: 'Are you certain?', 
    				msg: 'Do you want to move ['+droppedName+'] under ['+overModel.get('name')+']?',
    				buttons: Ext.Msg.YESNO,
    				closable: false,
    				fn: function(btn){
    					if (btn == 'yes'){
    						//Workaround for 4.0.7 bug
    						var plugin = finderViewport.down('treepanel').getView().getPlugin('ddplugin'), dropZone = plugin.dropZone;
    						dropZone.overRecord = overModel;
                            dropZone.currentPosition = dropPosition;
                            
                            var newParentDuid = overModel.get('parentId');
                            if(overModel.get('duid')){
                            	overModel.expand();
                            	newParentDuid = overModel.get('duid');
                	    	}
                            var data = Ext.JSON.encode({"parentDuid":newParentDuid});
                            
                	    	var moveUrl = "/file/"+droppedNode.get('id'); 
                	    	if(droppedNode.get('duid')){
                	    		moveUrl = "/folder/"+droppedNode.get('duid');
                	    	}	        	    		
            	        	Ext.Ajax.request({
            	        	    method : "PUT",
            	        	    url: moveUrl,
            	        	    jsonData : data,
            	        	    success : function(){
            	        	    	drop();
            	        	    },
            	        	    failure : function(){
            	        	    	Ext.MessageBox.alert('Forbidden', 'You do not have permission to perform that move. Either you have no permission to write to the destination folder or a file of the same name already exists.');
            	        	    }
            	        	});	        	        				    			
    					}
    				}
    			});        			
    		}        		
        	return false;           	
        } );
        
        finderViewport.show();           
        
		Ext.get('uploader').setVisibilityMode(Ext.Element.VISIBILITY).show();
    	uploadPopup = Ext.create('FolderGridFinder.view.UploadPopup', {	    		            
            closeAction: 'hide',            
            listeners: {            	
                close: {                	
                    fn: function(){                     	
                    	var uploader = $("#uploader").pluploadQueue();
                    	uploader.splice();                    	                    	
                		$('.plupload_message').remove();                		    		    		
                    }
                }            	
            },
            items : [
				{
				     region: "north",
				     xtype: 'toolbar',
                     id: 'uploader-toolbar',
                     tpl: Ext.create('Ext.XTemplate', 
                         ''
                     ),
                     items : [
                         {
	                         itemId: 'bulkUploaderBtn',text: 'Open Archive Uploader',disabled: false,
	                         tooltip: 'Open the archive uploader applet for this folder.',
	                         iconCls: 'icon-disk-add',
	                         listeners: {
	                             click: function (node, data) {
	                            	 var targetFolderDuid = currentlySelected.get('duid') != null ? currentlySelected.get('duid') : currentlySelected.get('parentDuid');
	                                 window.location = '/user/bulkUploader.html?target='+targetFolderDuid;
	                             }
	                         }
                         }
                     ]
                     
				},
 			    {
 			        xtype : "panel",
 			        region: "center",
 			        renderTo: Ext.getBody(), 
 		            contentEl: 'uploaderform',
 			    }
 			 ]
        });          	
    	var complexityTest = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/;
    	Ext.apply(Ext.form.field.VTypes, {
    		securepword: function(val, field) {
    			if(val == null || val.length < 8){
    				return false;
    			}    			    			
                if(!complexityTest.test(val)){
                    return false;
                }                                        
                return true;	        
    	    },
    	    securepwordText: 'Not a valid password.  Must contain at least eight characters including at least one lowercase, uppercase, and numeric character.'
    	});
	},
	processSearchSelection:function(record){	
		if(record.get('duid') != null && record.get('duid').trim().length > 0){
    		this.openFolder(record.get('duid'));                			
    	}else{
    		this.openFolder(record.get('parentDuid'),record);                		                		
    	}		
	},	
	mapFolderClick:function() {
		if(currentlySelected.get('duid')){		
			Ext.MessageBox.alert('Map Folder Locally', "Copy and paste the following DUID into <a href='http://help.foldergrid.com/support/solutions/articles/5000562695-what-is-foldersync-' target='_blank'>FolderSync</a> to map this folder locally:<br><br><b>"+currentlySelected.get('duid')+"</b>");
		}
	},	
	installFolderSync:function(node, data){
		if ( navigator.userAgent.match(/(iPad|iPhone|iPod)/i) ? true : false ) {
				Ext.Msg.alert({
	                msg: 'Unfortunately, your Apple iOS device does not support keeping a local copy of FolderGrid files.',
	                title: 'Apple iOS Unsupported',
	                closable: true,
	                buttons: Ext.MessageBox.OK,
	                buttonText:{ 
	                    ok: "Close"
	                },
	                draggable: false                         
	            });
		}else{
			window.location = 'http://foldergrid.com/foldersync.html#options';	
		}   		 
	},
	toggleWatchClick:function() {
		if(currentlySelected.get('duid')){			
			var isWatched = false;
			var watches = me.getStore('Watches');
			var watCount = watches.count();
			for(i=0;i<watCount;i++){
				var wat = watches.getAt(i);
				if(wat.get('duid') == currentlySelected.get('duid')){
					isWatched = true;
					break;
				}
			}
			if(isWatched){
				Ext.Ajax.request({
	        	    method : "DELETE",
	        	    url: '/user/watch/'+currentlySelected.get('duid'),
	        	    success : function(){        	    	
	        	    	Ext.MessageBox.alert('Success', currentlySelected.get('name')+" has been removed from your watch list. You will no longer receive notifications of new files uploaded to this folder.");
	        	    	me.getStore('Watches').reload(); 
	        	    },
	        	    failure: function(response){	        	    
	        	    	Ext.MessageBox.alert('Error', "Unable to remove "+currentlySelected.get('name')+" from your watch list.");
	        	    }
	        	});
			}else{
				Ext.Ajax.request({
	        	    method : "PUT",
	        	    url: '/user/watch/'+currentlySelected.get('duid'),
	        	    success : function(){
	        	    	Ext.MessageBox.alert('Success', currentlySelected.get('name')+" has been added to your watch list. You will now receive notifications of new files uploaded to this folder.");
	        	    	me.getStore('Watches').reload();       			
	        	    },
	        	    failure: function(response){	        	    
	        	    	Ext.MessageBox.alert('Error', "Unable to add "+currentlySelected.get('name')+" to your watch list.");
	        	    }
	        	});	
			}			
		}
	},	
	toggleLockClick:function() {
		if(currentlySelected.get('id')){						
			if(currentlySelected.get('lockHolder')){
				Ext.Ajax.request({
	        	    method : "DELETE",
	        	    url: '/file/lock/'+currentlySelected.get('id'),
	        	    success : function(){        	    	
	        	    	Ext.MessageBox.alert('Success', currentlySelected.get('name')+" has been unlocked.");
	        	    	currentlySelected.set('lockHolder',null);
	        	    	currentlySelected.commit();
	        	    	finderViewport.down('gridpanel').getSelectionModel().select(currentlySelected,false,true);
	        	    },
	        	    failure: function(response){	        	    
	        	    	Ext.MessageBox.alert('Error', "Unable to remove lock for "+currentlySelected.get('name'));
	        	    }
	        	});
			}else{
				Ext.Ajax.request({
	        	    method : "PUT",
	        	    url: '/file/lock/'+currentlySelected.get('id'),
	        	    success : function(){
	        	    	Ext.MessageBox.alert('Success', currentlySelected.get('name')+" has been locked for you.");
	        	    	currentlySelected.set('lockHolder',currentUsername);
	        	    	currentlySelected.commit();
	        	    	finderViewport.down('gridpanel').getSelectionModel().select(currentlySelected,false,true);
	        	    },
	        	    failure: function(response){	        	    
	        	    	Ext.MessageBox.alert('Error', "Unable to lock "+currentlySelected.get('name'));
	        	    }
	        	});	
			}			
		}
	},		
	goAddClick: function(node, data){
		if(currentlySelected.get('duid')){		
			Ext.Ajax.request({
        	    method : "PUT",
        	    url: '/user/favorite/'+currentlySelected.get('duid'),
        	    success : function(){
        	    	var toolbar = Ext.getCmp('fg-toolbar');
        	    	var goMenu = toolbar.getComponent('goBtn').menu;
        	    	var newItem = Ext.create('Ext.menu.Item', {
        	        	text: currentlySelected.get('name'),
        	        	duid: currentlySelected.get('duid'),
        	        	itemId: currentlySelected.get('duid'),
        	        	iconCls: 'icon-go-heart',
        	        	listeners: {
                   		 click: function (node, data) {
                   			me.goClick(node,data);
                   		 }
                        }
        	        });
        	    	goMenu.add(newItem); 
        	    	me.getStore('Favorites').reload();
        	    	goMenu.getComponent('addGoBtn').disable();
        			goMenu.getComponent('delGoBtn').enable();        			
        	    },
        	    failure: function(response){	        	    
        	    	Ext.MessageBox.alert('Error', "Unable to add "+currentlySelected.get('name')+" as a favorite.");
        	    }
        	});
		}
	},
	goDelClick: function(node, data){
		if(currentlySelected.get('duid')){		
			Ext.Ajax.request({
        	    method : "DELETE",
        	    url: '/user/favorite/'+currentlySelected.get('duid'),
        	    success : function(){        	    	
        	    	var toolbar = Ext.getCmp('fg-toolbar');
        	    	var goMenu = toolbar.getComponent('goBtn').menu;
        	    	goMenu.remove(goMenu.queryById(currentlySelected.get('duid')));
        	    	me.getStore('Favorites').reload();
        	    	goMenu.getComponent('addGoBtn').enable();
        			goMenu.getComponent('delGoBtn').disable(); 
        	    },
        	    failure: function(response){	        	    
        	    	Ext.MessageBox.alert('Error', "Unable to remove "+currentlySelected.get('name')+" from favorites.");
        	    }
        	});
		}
	},
	goClick: function(node, data){
		me.openFolder(node.duid);
	},
	openSelectedFile:function() {
		var fileId = currentlySelected.get('id');
		if(fileId){			
			//Because this method is called from context menu 'this' refers to the menu item...						
			$.ajax({                 	
	            type: "GET",
	            url: "/file/open/"+fileId,
	            complete: function(jqXHR,status){
	            	if(jqXHR.status == 200){
	            		window.location = "foldergrid://file/"+fileId;	
	            	}else if(jqXHR.status == 406){
	                    Ext.Msg.alert({
	                        msg: 'To open a file locally you must first sign-in to <a href="http://foldergrid.com/foldersync.html">FolderSync v1.20 or greater</a> from this workstation.',
	                        title: 'FolderSync Sign-In Required',	                        
	                        buttons: 1,
	                        draggable: false	                        
	                    });                    
	                }else if(jqXHR.status == 403){
	                    Ext.Msg.alert('Access Denied', 'You do not have permission to download the file ['+itemName+']');
	                }else if(jqXHR.status == 401){	                    
	                    Ext.Msg.alert({
	                		msg: 'You will now be redirected to login again.',
	                		title: 'Your session expired.',
	                		closable: false,
	                		buttons: 1,
	                		draggable: false,
	                		fn : function() {
	                    		window.location = window.location; 
	                    	}
	                	});
	                }else {
	                    Ext.Msg.alert('Unknown Error', 'Your request from the server returned status code ['+jqXHR.status+']. Please contact support for assistance.');                                             
	                }                                             
	            }
	        });
		}
	},	
	openSelectedFolder:function() {
		if(currentlySelected.get('duid')){			
			//Because this method is called from context menu 'this' refers to the menu item...						
			if(treeStore.getRootNode() != currentlySelected){
				me.openFolder(currentlySelected.get('duid'));
			}else if(currentlySelected.get('parentDuid') != ''){
				me.openFolder(currentlySelected.get('parentDuid'));
			}
		}
	},	
	openFolder:function(targetDuid,selectNode) {
		var Folder = Ext.ModelMgr.getModel('FolderGridFinder.model.Folder');
		if(targetDuid == null || targetDuid.trim() == "") targetDuid = '*';
		
        Folder.load(targetDuid,{
        	success: function(folder) {
                treeStore.setRootNode({
                    name: folder.get('name'),
                    duid: folder.get('duid'),
                    parentDuid: (folder.raw.parent != undefined) ? folder.raw.parent.duid : null,
                    updated: folder.get('updated'),
                    leaf: false,
                    expanded: false
                });             
                var treepanel = finderViewport.down('treepanel');
                var gridpanel = finderViewport.down('gridpanel');
            	var rootNode = treepanel.getRootNode();
            	if(selectNode != null){            		
                	var fileHitSelector = function(){                		
                		var child = gridpanel.getStore().findRecord('id',selectNode.get('id'));
                		if(child != null){	                
                			//Keep searching on each refresh to support buffered views                			
                			gridpanel.getView().removeListener('refresh',fileHitSelector);
                        	gridpanel.getSelectionModel().select(0,false,true);
                			gridpanel.getSelectionModel().select(child,false,true);
                			currentlySelected = selectNode;
                		}
                	};
                	gridpanel.getView().on('refresh',fileHitSelector);
                }
            	treepanel.getSelectionModel().select(rootNode);                                
            },
            failure:function(folder, operation) {        	                
                Ext.MessageBox.alert('Access Denied', 'Unable to navigate to that folder.');        	                                                          
            }
        });
	}, 		
	dblClick: function(panel, record) {
        if(currentlySelected instanceof Array || !currentlySelected.get('duid')){
        	if(currentlySelected.get('id') && Ext.util.Cookies.get("openlocallyByDefault")){
        		this.openSelectedFile();
        	}else{
        		this.downloadClick();	
        	}        	
        }
	},
	itemContextMenu : function(view, record, item, index, e) {
		e.preventDefault();		
		if(!record.get('duid')){
			fileContextMenu.showAt(e.getX(),e.getY());
			var lockBtn = fileContextMenu.items.get('toggleLockBtn');    		
    		if(record.get('lockHolder')){
    			lockBtn.setText("Unlock");
    		}else{
    			lockBtn.setText("Lock");
    		}
		}else{
			finderViewport.down('treepanel').getSelectionModel().select(record);			
			var watchBtn = folderContextMenu.items.get('toggleWatchBtn');			    		
    		var isWatched = false;
    		var watches = me.getStore('Watches');
    		var watCount = watches.count();
    		for(i=0;i<watCount;i++){
    			var wat = watches.getAt(i);
    			if(wat.get('duid') == currentlySelected.get('duid')){
    				isWatched = true;
    				break;
    			}
    		}
    		if(isWatched){
    			watchBtn.setText("UnWatch Folder");
    		}else{				
    			watchBtn.setText("Watch Folder");
    		}
    		    	
			folderContextMenu.showAt(e.getX(),e.getY());
		}
	},
	fileMenuClick: function(){
		if(currentlySelected == null) return;
		var toolbar = Ext.getCmp('fg-toolbar');
		var fileMenu = toolbar.getComponent('fileBtn').menu;
		if(currentlySelected instanceof Array){
			me.toggleMultiSelectOperationButtons(fileMenu);    		
    	}else if(!currentlySelected.get('duid')){
			me.toggleFileOperationButtons(currentlySelected, fileMenu);						
		}else{
			me.toggleFolderOperationButtons(currentlySelected, fileMenu);
		}
	},
	goMenuClick: function(){		
		var toolbar = Ext.getCmp('fg-toolbar');
		var goMenu = toolbar.getComponent('goBtn').menu;
		var addGoBtn = goMenu.getComponent('addGoBtn');
		var delGoBtn = goMenu.getComponent('delGoBtn');
		delGoBtn.disable();
		addGoBtn.disable();
		if(currentlySelected == null || currentlySelected instanceof Array || currentlySelected.get('duid') == null ) return;
		
		if(currentlySelected.get('duid')){
			var isFavorite = false;
			var favorites = me.getStore('Favorites');
			var favCount = favorites.count();
			for(i=0;i<favCount;i++){
				var fav = favorites.getAt(i);
				if(fav.get('duid') == currentlySelected.get('duid')){
					isFavorite = true;
					break;
				}
			}
			if(isFavorite){
				delGoBtn.enable();
				addGoBtn.disable();
			}else{				
				delGoBtn.disable();
				addGoBtn.enable();
			}			
		}
	},
    toggleAllMenus: function(nothing) {
    	var toolbar = Ext.getCmp('fg-toolbar');    	
    	if(currentlySelected instanceof Array){
			this.toggleMultiSelectOperationButtons(toolbar);		
			this.toggleMultiSelectOperationButtons(fileContextMenu);    		
    	}else if(!currentlySelected.get('duid')){
			this.toggleFileOperationButtons(currentlySelected, toolbar);
			this.toggleFileOperationButtons(currentlySelected, fileContextMenu);						
		}else{
			this.toggleFolderOperationButtons(currentlySelected, toolbar);
			this.toggleFolderOperationButtons(currentlySelected, folderContextMenu);
		}		    	
    },
    toggleMultiSelectOperationButtons: function (comp){    	
    	if(comp.getComponent('openFileBtn')) comp.getComponent('openFileBtn').disable();
    	if(comp.getComponent('uploadBtn')) comp.getComponent('uploadBtn').disable();    	
    	if(comp.getComponent('renameBtn')) comp.getComponent('renameBtn').disable();
    	if(comp.getComponent('copyBtn')) comp.getComponent('copyBtn').enable();
    	if(comp.getComponent('pasteBtn')) comp.getComponent('pasteBtn').disable();
    	if(comp.getComponent('compressBtn')) comp.getComponent('compressBtn').enable();    	
    	if(comp.getComponent('newFolderBtn')) comp.getComponent('newFolderBtn').disable();
    	if(comp.getComponent('openFolderBtn')) comp.getComponent('openFolderBtn').disable();    	
    	if(comp.getComponent('permissionsBtn')) comp.getComponent('permissionsBtn').disable();
    	if(comp.getComponent('linkBtn')) comp.getComponent('linkBtn').disable();       	
    	if(comp.getComponent('showDetailsBtn')) comp.getComponent('showDetailsBtn').disable();
    	if(comp.getComponent('mapFolderBtn')) comp.getComponent('mapFolderBtn').disable();
    	if(comp.getComponent('restoreBtn')) this.activateButton(comp.getComponent('restoreBtn'),false,true);
    	if(comp.getComponent('toggleWatchBtn')) comp.getComponent('toggleWatchBtn').disable();
    	if(comp.getComponent('toggleLockBtn')) comp.getComponent('toggleLockBtn').disable();
    	if(comp.getComponent('revisionsBtn')) comp.getComponent('revisionsBtn').disable();
    	if(comp.getComponent('logBtn')) comp.getComponent('logBtn').disable();
    	if(comp.getComponent('downloadBtn')) comp.getComponent('downloadBtn').enable();
    	if(comp.getComponent('viewBtn')) comp.getComponent('viewBtn').disable();
    	if(comp.getComponent('deleteBtn')) comp.getComponent('deleteBtn').enable();
    	if(comp.getComponent('expireBtn')) comp.getComponent('expireBtn').enable();
    	if(comp.getComponent('undeleteBtn')) comp.getComponent('undeleteBtn').disable();
    },    
    activateButton: function(comp,visible,enable){
    	comp.setVisible(visible);
    	if(enable){
    		comp.enable();
    	}else{
    		comp.disable();
    	}
    },
    toggleFileOperationButtons: function (record, comp){    	
    	if(comp.getComponent('openFileBtn')) {
    		var fname = record.get('name');
    		if(fname.indexOf(".zip", this.length - 4) !== -1 || fname.indexOf(".tar", this.length - 4) !== -1 || fname.indexOf(".gz", this.length - 4) !== -1 || fname.indexOf(".bzip2", this.length - 4) !== -1){
    			this.activateButton(comp.getComponent('openFileBtn'),true,false);
    		}else{
        		this.activateButton(comp.getComponent('openFileBtn'),true,true);    			
    		}
    	}    	
    	if(comp.getComponent('uploadBtn')) this.activateButton(comp.getComponent('uploadBtn'),true,false);
    	if(comp.getComponent('downloadBtn')) this.activateButton(comp.getComponent('downloadBtn'),true,record.get('isReadable'));
    	if(comp.getComponent('viewBtn')) this.activateButton(comp.getComponent('viewBtn'),true,true);
    	if(comp.getComponent('renameBtn')) this.activateButton(comp.getComponent('renameBtn'),true,record.get('isWritable'));
    	if(comp.getComponent('copyBtn')) this.activateButton(comp.getComponent('copyBtn'),true,record.get('isReadable'));
    	if(comp.getComponent('pasteBtn')) this.activateButton(comp.getComponent('pasteBtn'),true,false);
    	if(comp.getComponent('compressBtn')) this.activateButton(comp.getComponent('compressBtn'),true,false);    	
    	if(comp.getComponent('deleteBtn')) this.activateButton(comp.getComponent('deleteBtn'),true,record.get('isWritable') && record.get('isRemovable'));
    	if(comp.getComponent('expireBtn')) this.activateButton(comp.getComponent('expireBtn'),true,record.get('creator') == currentUsername || !isNotDomainAdmin);
    	if(comp.getComponent('undeleteBtn')) this.activateButton(comp.getComponent('undeleteBtn'),false,true);
    	if(comp.getComponent('newFolderBtn')) this.activateButton(comp.getComponent('newFolderBtn'),false,true);
    	if(comp.getComponent('openFolderBtn')) this.activateButton(comp.getComponent('openFolderBtn'),false,true);    	
    	if(comp.getComponent('permissionsBtn')) this.activateButton(comp.getComponent('permissionsBtn'),true,record.get('isACLReadable'));
    	if(comp.getComponent('linkBtn')) this.activateButton(comp.getComponent('linkBtn'),true,record.get('isACLWritable') && record.get('isACLReadable'));
    	if(comp.getComponent('showDetailsBtn')) this.activateButton(comp.getComponent('showDetailsBtn'),true,true);
    	if(comp.getComponent('mapFolderBtn')) this.activateButton(comp.getComponent('mapFolderBtn'),false,true);
    	if(comp.getComponent('restoreBtn')) this.activateButton(comp.getComponent('restoreBtn'),false,true);
    	if(comp.getComponent('toggleWatchBtn')) this.activateButton(comp.getComponent('toggleWatchBtn'),false,true);
    	if(comp.getComponent('revisionsBtn')) this.activateButton(comp.getComponent('revisionsBtn'),true,true);    	
    	if(comp.getComponent('logBtn')) this.activateButton(comp.getComponent('logBtn'),true,record.get('isACLReadable'));
    	if(comp.getComponent('toggleLockBtn')){
    		this.activateButton(comp.getComponent('toggleLockBtn'),lockingEnabled,true);    		
    		if(record.get('lockHolder')){
    			comp.getComponent('toggleLockBtn').setText("Unlock");
    		}else{
    			comp.getComponent('toggleLockBtn').setText("Lock");
    		}
    	}
    },
    toggleFolderOperationButtons: function (record, comp){
    	if(comp.getComponent('openFileBtn')) this.activateButton(comp.getComponent('openFileBtn'),false,true);
    	if(comp.getComponent('uploadBtn')) this.activateButton(comp.getComponent('uploadBtn'),true,record.get('isWritable'));
    	if(comp.getComponent('downloadBtn')) this.activateButton(comp.getComponent('downloadBtn'),true,false);
    	if(comp.getComponent('viewBtn')) this.activateButton(comp.getComponent('viewBtn'),false,true);
    	if(comp.getComponent('renameBtn')) this.activateButton(comp.getComponent('renameBtn'),true,record.get('isWritable'));
    	if(comp.getComponent('copyBtn')) this.activateButton(comp.getComponent('copyBtn'),true,record.get('isReadable'));    	
    	if(comp.getComponent('pasteBtn')) this.activateButton(comp.getComponent('pasteBtn'),true,record.get('isWritable') && clipBoard != null);
    	if(comp.getComponent('compressBtn')) this.activateButton(comp.getComponent('compressBtn'),false,false);
    	if(comp.getComponent('expireBtn')) this.activateButton(comp.getComponent('expireBtn'),false,false);    	
    	if(comp.getComponent('undeleteBtn')) this.activateButton(comp.getComponent('undeleteBtn'),true,true);
    	if(comp.getComponent('newFolderBtn')) this.activateButton(comp.getComponent('newFolderBtn'),true,record.get('isWritable'));    	    	
    	if(comp.getComponent('permissionsBtn')) this.activateButton(comp.getComponent('permissionsBtn'),true,record.get('isACLReadable'));
    	if(comp.getComponent('linkBtn')) this.activateButton(comp.getComponent('linkBtn'),true,record.get('isACLWritable') && record.get('isWritable'));
    	if(comp.getComponent('showDetailsBtn')) this.activateButton(comp.getComponent('showDetailsBtn'),true,true);
    	if(comp.getComponent('mapFolderBtn')) this.activateButton(comp.getComponent('mapFolderBtn'),true,true);    	
    	if(comp.getComponent('revisionsBtn')) this.activateButton(comp.getComponent('revisionsBtn'),true,false);    	
    	if(comp.getComponent('logBtn')) this.activateButton(comp.getComponent('logBtn'),true,record.get('isACLReadable'));
    	if(comp.getComponent('toggleLockBtn')) this.activateButton(comp.getComponent('toggleLockBtn'),false,true);
    	
    	var delFolderBtn = comp.getComponent('deleteBtn');
    	var restoreBtn = comp.getComponent('restoreBtn');
    	if(delFolderBtn != null){        		
    		if(record.get('parentId') == 'TRASHBIN'){    			
    			this.activateButton(delFolderBtn,false,true);
    			this.activateButton(restoreBtn,true,true);
    		}else{
    			this.activateButton(delFolderBtn,true,record.get('isWritable') && record.get('isRemovable'));
    			this.activateButton(restoreBtn,false,true);
    		}
    	}
    	
    	var openFolderBtn = comp.getComponent('openFolderBtn');
    	if(openFolderBtn != null){    					
    		if(treeStore.getRootNode() != record){
    			this.activateButton(openFolderBtn,true,true);
    			openFolderBtn.setText('Open Here');
    		}else if(record.get('parentId') != ''){    			
    			this.activateButton(openFolderBtn,true,true);
    			openFolderBtn.setText('Open Parent');
    		}else{				
    			this.activateButton(openFolderBtn,true,false);
    			openFolderBtn.setText('Open Parent');
    		}    		
    	}    	    	
    	var toggleWatchBtn = comp.getComponent('toggleWatchBtn');
    	if(toggleWatchBtn){
    		this.activateButton(toggleWatchBtn,true,true);
    		var isWatched = false;
    		var watches = me.getStore('Watches');
    		var watCount = watches.count();
    		for(i=0;i<watCount;i++){
    			var wat = watches.getAt(i);
    			if(wat.get('duid') == currentlySelected.get('duid')){
    				isWatched = true;
    				break;
    			}
    		}
    		if(isWatched){
    			comp.getComponent('toggleWatchBtn').setText("UnWatch Folder");
    		}else{				
    			comp.getComponent('toggleWatchBtn').setText("Watch Folder");
    		}    		
    	}
    	
    },    
    treeSelect: function(thisModel,selectedModel,eOpts){;
    	Ext.getCmp('search-field').clearAndClose();
    	finderViewport.down('gridpanel').getSelectionModel().deselectAll();
    },
    treeSelectionChange: function(thisModel,selectedModels,eOpts){   
		if(!treeSelectionPopulatesGrid) return;		
		
    	if(selectedModels.length == 1 && selectedModels[0] != null && currentlySelected != selectedModels[0]){
    		currentlySelected = selectedModels[0];
    		
    		if(currentlySelected.get('isReadable')){
    			if(!currentlySelected.isExpanded() ) {    	
    				currentlySelected.expand();
    			}
        		this.toggleAllMenus();        		
        		var panel = finderViewport.down('gridpanel');
        		var title = 'File Listing for ['+currentlySelected.get('name')+']';
        		if( currentlySelected.get('parentId') == 'TRASHBIN' || currentlySelected.get('parentDuid') == 'TRASHBIN' ){
        			title += " :: This Folder has been deleted and all its contents are in the TrashBin";
        		}
        		panel.setTitle(title);        		
        		var newStore = Ext.create('Ext.data.Store', {
		            model: 'FolderGridFinder.model.Folder',              
		            remoteGroup: true,
		            remoteSort: true,
		            buffered: true,
		            trailingBufferZone: 300,
		            leadingBufferZone: 300,
		            pageSize: 75,
		            autoLoad: true,
		            fileCount : 0,
		            sorters: [{property: 'updated', direction: 'DESC'}],
		            listeners: {
                        load : function() {                                	
                    		Ext.getCmp('selection-msg').setText(this.getTotalCount()+" files");
                        }
                    },
		            proxy: {
		            	type: 'rest',
		            	simpleSortMode :true,
		            	url: '/folder/'+currentlySelected.get('duid'),
		                reader: {
		                    root: 'files',
		                    totalProperty: 'fileCount'        		                    
		                },
		                extraParams: {'excludeFolders':'true'}
		            }    		                        
		        });
        		panel.reconfigure(
        				newStore
        	    );
        		
        		if(panel.isDisabled()){
        			panel.setDisabled(false);        			
        		}
    		}else{
    			var panel = finderViewport.down('gridpanel');
    			panel.setDisabled(true);
    			panel.reconfigure(
    					Ext.create('Ext.data.Store', {
    						model: 'FolderGridFinder.model.Folder',
    			            remoteGroup: true,
    			            remoteSort: true,
    			            buffered: true,
    			            trailingBufferZone: 300,
    			            leadingBufferZone: 300,
    			            pageSize: 75,
    			            autoLoad: false,
    			            proxy: {
    			            	type: 'rest',
    			            	simpleSortMode :true,
    			            	url: '/folder/',
    			                reader: {
    			                    root: 'files',
    			                    totalProperty: 'fileCount'
    			                },
    			                extraParams: {'excludeFolders':'true'}        
    			            }
    			        })
        	    	);
    			panel.setTitle('You do not have permission to read the ['+currentlySelected.get('name')+'] folder.');
    		}
    		
    		//FolderStore caches folder data indefinitely - force periodic refreshes from server
    		var folderToReload = currentlySelected;
    		var now = (new Date).getTime();
    		if(folderToReload.get('lastRefreshed') > 0 && (now - folderToReload.get('lastRefreshed')) > 30000)
    		{ //cache folder children for only 30s - then pass etag as parameter on subsequent 30s refreshes
    			FolderGridFinder.model.Folder.load(folderToReload.get('duid'),{
    				params: { 'if-none-match': folderToReload.get('etag') },
        		    success: function(record, operation) {        		    	
        		    	if(operation.response.status == 200){        		    		
        		    		folderToReload.removeAll(false);
        		    		var refreshedChildren = record.getData()['children'];
        		    		if(refreshedChildren && refreshedChildren.length){        		    			
            					for(i=0;i<refreshedChildren.length;i++){
            						folderToReload.appendChild(refreshedChildren[i]);     						
            					}	
        		    		}
        		    		folderToReload.set('etag',record.getData()['etag']);
        		    		folderToReload.set('lastRefreshed',now);        		    		
        		    	}        		    	
        		    }
        		});	
    		}    		    		
    	}else if(selectedModels.length > 1){
    		currentlySelected = selectedModels;
    		this.toggleAllMenus();
    	}
    },
    gridSelect: function(thisModel,selectedModel,eOpts){    	    	
    	finderViewport.down('treepanel').getSelectionModel().deselectAll();
    },
    gridSelectionChange: function(thisModel,selectedModels,eOpts){
    	var selMsg = Ext.getCmp('selection-msg');
    	if(selectedModels.length == 1 && selectedModels[0] != null){
    		currentlySelected = selectedModels[0];    		
    		selMsg.setText("1 of "+finderViewport.down('gridpanel').getStore().getTotalCount()+" files selected");
    		this.toggleAllMenus();
    	}else if(selectedModels.length > 1){
    		currentlySelected = selectedModels;    		
    		selMsg.setText(currentlySelected.length + " of "+finderViewport.down('gridpanel').getStore().getTotalCount()+" files selected");
    		this.toggleAllMenus();
    	}
    },    
    downloadInIframe: function(itemName,itemId,iFrameName){ 						
		$.ajax({                 	
            type: "HEAD",
            url: "/file/"+itemId,
            complete: function(jqXHR,status){
            	if(jqXHR.status == 200){
            		try {
                	    Ext.destroy(Ext.get(iFrameName));
                	}catch(ignore) {}
                	try {                		 	 
                		if ( navigator.userAgent.match(/(iPad|iPhone|iPod)/i) !== null ? true : false ) {
                			var directLink = '/file/'+itemId+'?filename='+itemName; 
                			Ext.Msg.alert({
                                msg: 'Your device requires you to confirm your intent to open the selected file by clicking the link below:<br><br><a target="_blank" href="'+directLink+'">Click here to open ['+itemName+']</a>',
                                title: 'Device Download Prompt',
                                closable: true,
                                buttons: Ext.MessageBox.OK,
                                buttonText:{ 
                                    ok: "Close"
                                },
                                draggable: false                         
                            });                			
                		}else{                			
                			Ext.core.DomHelper.append(document.body, {
                	    	    tag: 'iframe',
                	    	    id:iFrameName,
                	    	    frameBorder: 0,
                	    	    width: 0,
                	    	    height: 0,
                	    	    css: 'display:none;visibility:hidden;height:0px;',
                	    	    src: '/file/'+itemId
                	    	});	
                		}            	    	
                	}catch(e) {
                		Ext.MessageBox.alert('Failed', 'We were unable to complete the requested download. Please try again. '+e.message);
                	}
            	}else if(jqXHR.status == 401){
                    Ext.Msg.alert({
                        msg: 'You will now be redirected to login again.',
                        title: 'Your session expired.',
                        closable: false,
                        buttons: 1,
                        draggable: false,
                        fn : function() {
                            window.location = window.location; 
                        }
                    });                    
                }else if(jqXHR.status == 403){
                    Ext.Msg.alert('Access Denied', 'You do not have permission to download the file ['+itemName+']');                                             
                }else {
                    Ext.Msg.alert('Unknown Error', 'Your request from the server returned status code ['+jqXHR.status+']. Please contact support for assistance.');                                             
                }                                             
            }
        });
    },
    downloadClick: function(button,event,eOpts){
    	if(currentlySelected instanceof Array){
    		Ext.MessageBox.show({
    			title: 'Are you certain?', 
    			msg: 'You are about to download the ['+currentlySelected.length+'] currently selected items. We recommend you ensure your browser is set to automatically save downloads to avoid being prompted for every selected file. Are you certain you wish to continue?',
    			buttons: Ext.Msg.YESNO,
    			closable: false,
    			fn: function(btn){
    				if (btn == 'yes'){
    					var numSelected = currentlySelected.length;
    					for(i=0;i<numSelected;i++){
    						if(currentlySelected[i].get('payload-exists')){
    							var settimeoutcall = 'me.downloadInIframe("'+currentlySelected[i].get('name')+'","'+currentlySelected[i].get('id')+'","downloadIframe'+i+'")';
    							window.setTimeout(settimeoutcall, i * 1000);
    						}
    					}    															
    				}
    			}
        	});
    	}else{
    		if(currentlySelected.get('payload-exists')){
    			me.downloadInIframe(currentlySelected.get('name'),currentlySelected.get('id'),'downloadIframe0');
    		}else{
    			Ext.Msg.alert('Unavailable','The selected file has no fully uploaded version.');
    		}
    	}    	
    },
    viewClick: function(button,event,eOpts){    	
		if(currentlySelected.get('payload-exists')){			
			new Ext.Window({
				title : "Browser View of <a target='_blank' href='/file/"+currentlySelected.get('id')+"?filename="+currentlySelected.get('name')+"'>["+currentlySelected.get('name')+"]</a>",
			    width : Ext.getBody().getViewSize().width*.9,
			    height: Ext.getBody().getViewSize().height*.9,
			    layout: 'fit',
			    items : [			           		 
			        {
			        xtype : "component",			        
			        autoEl : {
			            tag : "iframe",			         
			            src : "/file/"+currentlySelected.get('id')+"?inline=true"
			        }
			    }]
			}).show();
		}else{
			Ext.Msg.alert('Unavailable','The selected file has no fully uploaded version.');
		}	    	
    },
    uploadClick: function(button,event,eOpts){
    	var selectedFolder = currentlySelected;
    	if(selectedFolder) {    		
    		if(selectedFolder instanceof Array){
    			selectedFolder = selectedFolder[0];
    		}
    		if(!selectedFolder.get('isWriteable')){
        		uploadPopup.setTitle('Upload to '+selectedFolder.get('name'));    		
    	    	uploadPopup.show();
    	    }else{
    	    	selectedFolder.set('isWritable',false);	 
    	    	this.toggleAllMenus();
    	    	Ext.MessageBox.alert('Access Denied', 'You do not have permission to upload to the selected folder.');	    	
    	    }	
    	}    	
    },    
    copyClick: function(button,event,eOpts){    	
    	clipBoard = currentlySelected;
    	if(clipBoard instanceof Array){
    		Ext.MessageBox.alert('Success', "Prepared to copy "+clipBoard.length+" files. Ready to paste to a destination folder.");
    	}else{
    		Ext.MessageBox.alert('Success', "Prepared to copy "+clipBoard.get("name")+". Ready to paste to a destination folder.");
    	}
    },
    pasteClick: function(button,event,eOpts){    	
        var target = currentlySelected;
    	if(clipBoard instanceof Array){
    		var numSelected = clipBoard.length;
			for(i=0;i<numSelected;i++){
				var item = clipBoard[i];
				var copyUrl = "/file/copy/"+item.get('id');         			    	
				var data = Ext.JSON.encode({"parentDuid":target.get('duid')});
	        	Ext.Ajax.request({
	        	    method : "PUT",
	        	    url: copyUrl,
	        	    jsonData : data,
	        	    success : function(){
	        	    	Ext.ComponentManager.get('file-listing').getStore().load({addRecords: true});
	        	    }	        	    
	        	});  				
			}     		
    	}else{    	   
			var item = clipBoard;			
			if(item.get('duid')){    			                                                                                                     
                var copyingDialog = Ext.Msg.show({
                    title: 'Copying...',
                    msg: 'Copying all subfolders. Please wait.',
                    width: 300,                                                                 
                    wait: true,
                    icon: Ext.window.MessageBox.INFO
                });    
                //Expand tree - workaround for bug in ExtJS
                var allNodesLoadedFn = function() {
                    var copyCalls = [];
                    recurse = function(srcNode,parentDuid){           
                        var newDuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
                            var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
                            return v.toString(16);
                        });                           
                        copyCalls.push(function(callback){                                        
                            var data = Ext.JSON.encode({"parentDuid":parentDuid, "targetDuid":newDuid});
                            //console.log("Executing copy for "+srcNode.get('name')+" into "+parentDuid+" as "+newDuid);
                            Ext.Ajax.request({
                                method : "PUT",
                                url: "/folder/copy/"+srcNode.get('duid'),
                                jsonData : data,
                                success : function(){                                    
                                    callback();
                                },
                                failure : function(response){
                                    if(response.getResponseHeader("Reason-Phrase")) {
                                        callback(response.getResponseHeader("Reason-Phrase"));
                                    }else if(response.status == 401) {   
                                        callback("You are not allowed to copy ["+srcNode.get('name')+"]" );  
                                    }else{
                                        callback("Unable to copy ["+srcNode.get('name')+"]" );
                                    }                                        
                                }                   
                            });                                                      
                        });
                        //console.log("Success now iterating over "+srcNode.childNodes.length+" child nodes");
                        for (var i = 0; i < srcNode.childNodes.length; i++) {
                            //console.log("About to recurse("+srcNode.childNodes[i].get('name')+","+newDuid+")");
                            recurse(srcNode.childNodes[i],newDuid);                              
                        }                                               
                    };      
                    recurse(item,target.get('duid'));
                    async.series(copyCalls,function(error,results){
                        copyingDialog.close();                                                 
                        if(error){
                            Ext.MessageBox.alert('Failure', error);
                        }else{
                            Ext.MessageBox.alert('Success', "Copied ["+item.get('name')+"] into "+target.get('name') );
                            target.set('lastRefreshed',1);
                            Ext.getCmp('foldertree').getSelectionModel().select(item,false,false);
                            Ext.getCmp('foldertree').getSelectionModel().select(target,false,false);
                        }
                    });                                                                                               
                },                                                          
                taskFn = function(node, callback) {                                                             
                    node.expand(false, function(childNodes) {                                                                                                                                       
                        for(var i=0;i<childNodes.length;i++) {
                            queue.push(childNodes[i]);
                        }                
                        callback();                                                                                                                                     
                    });
                },
                queue = async.queue(taskFn, 5);
                queue.drain = allNodesLoadedFn;
                queue.push(item);                                                                                                                                                                                                                                                                                                                                                                                       			                                                
            }else{
    			var copyUrl = "/file/copy/"+item.get('id');         			    	
    			var data = Ext.JSON.encode({"parentDuid":target.get('duid')});
            	Ext.Ajax.request({
            	    method : "PUT",
            	    url: copyUrl,
            	    jsonData : data,
            	    success : function(){
            	    	Ext.MessageBox.alert('Success', "Copied ["+item.get('name')+"] into "+target.get('name') );
            	    	Ext.ComponentManager.get('file-listing').getStore().load({addRecords: true});
            	    },
    	        	failure : function(response){
    	    	    	if(response.status == 403) {
        	    			Ext.MessageBox.alert('Forbidden', response.getResponseHeader("Reason-Phrase"));
        	    		}else if(response.status == 401) {	 
            	    		Ext.MessageBox.alert('Unauthorized', "You are not allowed to copy ["+item.get('name')+"] \""+target.get('name')+"\"" );	
        	    		}else{
        	    			Ext.MessageBox.alert('Failed', "Unable to copy ["+item.get('name')+"] into \""+target.get('name')+"\"" );
        	    		}
    	    	    }	        	    
            	});
            }  				     		 		
    	}    	  
    },
    compressClick: function(button,event,eOpts){    	
        var target = currentlySelected;
    	if(target instanceof Array){
    		var numSelected = target.length;
    		var duid = target[0].get('parentDuid');
    		var files = [];
			for(i=0;i<numSelected;i++){
				files[i] = target[i].get('id');	  				
			}
    		var promptMsg = 'Please enter a name for the new archive of '+numSelected+' files.';
        	Ext.Msg.show(
    			{
                    prompt: true,
                    title: 'New Archive Name',
                    minWidth: 300,
                    width: 450,
                    msg: promptMsg,
                    buttons: Ext.Msg.OKCANCEL,
                    callback: function(btn, text){
                	    if (btn == 'ok'){                 	    	
            	    		var data = Ext.JSON.encode({"name":text,"duid":duid,"files":files});
            	        	Ext.Ajax.request({
            	        	    method : "PUT",
            	        	    url: "/file/archive",
            	        	    jsonData : data,
            	        	    success : function(jqXHR){
            	        	    	Ext.ComponentManager.get('file-listing').getStore().load({addRecords: true});
            	        	    	if(jqXHR.responseText){
            	        	    		Ext.MessageBox.alert('Success', "Requested archive created as ["+jqXHR.responseText+"]");
            	        	    	}            	        	    	
            	        	    },
            	        	    failure: function(jqXHR, textStatus, errorThrown){	    
            	        	    	msg = "Unable to compress the selected files.";
    			                    if(jqXHR.getResponseHeader("Reason-Phrase")){
    			                        msg = msg + " " + jqXHR.getResponseHeader("Reason-Phrase");
    			                    }
            	        	    	Ext.MessageBox.alert('Failed', msg);        	    	
            	        	    }
            	        	});    		    	    	    	
                	    }
                	},                
                    value: 'Archive.zip'
                }
        	);    		     		
    	}  	  
    },
    renameClick: function(button,event,eOpts){
    	var promptMsg = 'Please enter a new name for ['+currentlySelected.get('name')+']';
    	Ext.Msg.show(
			{
                prompt: true,
                title: 'Rename',
                minWidth: 300,
                width: 450,
                msg: promptMsg,
                buttons: Ext.Msg.OKCANCEL,
                callback: function(btn, text){
            	    if (btn == 'ok'){
            	    	var renameUrl = "/file/"+currentlySelected.get('id'); 
            	    	if(currentlySelected.get('duid')){
            	    		renameUrl = "/folder/"+currentlySelected.get('duid');
            	    	}
        	    		var data = Ext.JSON.encode({"name":text.trim()});
        	        	Ext.Ajax.request({
        	        	    method : "PUT",
        	        	    url: renameUrl,
        	        	    jsonData : data,
        	        	    success : function(){
        	        	    	currentlySelected.set('name',text.trim());
        	        	    	currentlySelected.commit();
        	        	    	finderViewport.down('gridpanel').getSelectionModel().select(currentlySelected,false,true);
        	        	    },
        	        	    failure: function(jqXHR, textStatus, errorThrown){	    
        	        	    	msg = "You are not allowed to rename "+currentlySelected.get('name');
			                    if(jqXHR.getResponseHeader("Reason-Phrase")){
			                        msg = msg + " " + jqXHR.getResponseHeader("Reason-Phrase");
			                    }
        	        	    	Ext.MessageBox.alert('Access Denied', msg);        	    	
        	        	    }
        	        	});    		    	    	    	
            	    }
            	},                
                value: currentlySelected.get('name')
            }
    	);    			        	
    },
    undeleteClick: function(button,event,eOpts){
    	var delsPopup = Ext.create('FolderGridFinder.view.ui.DeletedPopup', {
    		title: 'Deleted Files Listing for ['+currentlySelected.get('name')+']',
            renderTo: Ext.getBody()
        });
    	delsPopup.down('gridpanel').reconfigure(Ext.create('Ext.data.Store', {
            model: 'FolderGridFinder.model.Folder',              
            remoteGroup: true,
            remoteSort: true,
            buffered: true,
            trailingBufferZone: 300,
            leadingBufferZone: 300,
            pageSize: 75,
            autoLoad: true,
            fileCount : 0,
            proxy: {
            	type: 'rest',
            	simpleSortMode :true,
            	url: '/folder/'+currentlySelected.get('duid'),
                reader: {
                    root: 'files',
                    totalProperty: 'fileCount'        		                    
                },
                extraParams: {'excludeFolders':'true','showDeletedFiles':'true'}
            }    		                        
        }));  
    	delsPopup.on('close', function(panel, opts){
    		finderViewport.down('treepanel').getSelectionModel().select(currentlySelected);
        });
    	delsPopup.show();      	
    	      		
    },    
    restoreClick: function(button,event,eOpts){    	
    	var reqJson = Ext.JSON.encode({"parentDuid":"~"});
   		moveUrl = "/folder/"+currentlySelected.get('duid');	        	  
    	Ext.Ajax.request({
    	    method : "PUT",
    	    url: moveUrl,
    	    jsonData : reqJson,
    	    success : function(){    	        	
    	    	me.openFolder("~")
    	    },
    	    failure : function(){
    	    	Ext.MessageBox.alert('Restore Failed', "Unable to restore folder to your home folder.");        	        	    	
    	    }
    	});    		   	    	      		
    },        
    deleteClick: function(button,event,eOpts){
    	if(currentlySelected instanceof Array){
    		var deleteTargets = currentlySelected;    		
    		Ext.MessageBox.show({
    			title: 'Are you certain?', 
    			msg: 'You are about to delete the ['+deleteTargets.length+'] currently selected items. Are you certain you wish to continue?',
    			buttons: Ext.Msg.YESNO,
    			closable: false,
    			fn: function(btn){
    				if (btn == 'yes'){
    					var numSelected = deleteTargets.length;
    					for(i=0;i<numSelected;i++){
    						var item = deleteTargets[i];
    						var deleteUrl = "/file/"+item.get('id');         			    	
        					Ext.Ajax.request({
            	        	    method : "DELETE",
            	        	    url: deleteUrl,
            	        	    success : function(deletedItem){   
            	        	    	return function (){            	        	    		           	        	    			
        	        	    			var gridpanel = finderViewport.down('gridpanel');
        	        	    			gridpanel.getStore().remove(deletedItem);
        	        	    			gridpanel.getStore().load();
            	        	    	};            	        	    	
            	        	    }(item),
            	        	    failure: function(deletedItem){
            	        	    	return function(response){      
            	        	    		if(response.status == 403) {
            	        	    			Ext.MessageBox.alert('Forbidden', response.getResponseHeader("Reason-Phrase"));
            	        	    		}else if(response.status == 401) {
            	        	    			deletedItem.set('isRemovable',false);	 
                	        	    		Ext.MessageBox.alert('Unauthorized', "You are not allowed to delete "+deletedItem.get('name'));	
            	        	    		}else{
            	        	    			Ext.MessageBox.alert('Error', "Unable to delete "+deletedItem.get('name')+" at this time. Please try again later.");
            	        	    		}      	        	    		
            	        	    	};            	        	    	    	        	    	
            	        	    }(item)
            	        	});    						
    					}    					
    				}
    			}
        	});
    	}else{
    		Ext.MessageBox.show({
    			title: 'Are you certain?', 
    			msg: 'You are about to delete ['+currentlySelected.get('name')+']. Are you certain you wish to continue?',
    			buttons: Ext.Msg.YESNO,
    			closable: false,
    			fn: function(btn){
    				if (btn == 'yes'){
    					var deleteUrl = "/file/"+currentlySelected.get('id');
    					var deleteRootTarget = currentlySelected; 
    			    	if(deleteRootTarget.get('duid')){    			    		
    			    		if(deleteRootTarget.hasChildNodes()){ 								
    							Ext.MessageBox.alert({
        	    					title: 'WARNING', 
        	    					modal: true,
        	    					buttons: Ext.Msg.OKCANCEL,
        	    					msg: 'The folder you have selected for delete contains a subfolder. Click OK to delete this folder and all subfolders underneath it.',
    	    						closable: false,
        	    					fn: function(response){        	    						    	        	    						
        	    						if (response == 'ok'){        	    							
        	    							treeSelectionPopulatesGrid = false;
        	    							var deletingDialog = Ext.Msg.show({
    	    								    title: 'Deleting...',
    	    								    msg: 'Deleting all subfolders. Please wait.',
    	    								    width: 300,	                    	    								    
    	    								    wait: true,
    	    								    icon: Ext.window.MessageBox.INFO
    	    								});    
        	    							//Expand tree - workaround for bug in ExtJS
        	    							var allNodesLoadedFn = function() {
        	    								treeSelectionPopulatesGrid = true;
        	    								Ext.getCmp('foldertree').getSelectionModel().select(deleteRootTarget,false,false);
        	    								// Now delete nodes starting with the leaves
        	    								var deleteCalls = [];
        	    								recurse = function(targetNode){
            	    					    		for (var i = 0; i < targetNode.childNodes.length; i++) {
            	    					    			recurse(targetNode.childNodes[i]);    			    			
            	    					    		}  
            	    					    		deleteCalls.push(function(callback){
            	    					    			Ext.Ajax.request({
                	    					        	    method : "DELETE",
                	    					        	    url: "/folder/"+targetNode.get('duid'),
                	    					        	    success : function(){        	    	
                	    					        	    	targetNode.remove(false);
                	    					        	    	callback();
                	    					        	    },  
                	    					        	    failure: function(){
                	    					        	    	callback("Unable to delete ["+targetNode.get('name')+"]");
                	    					        	    }                	    					        	    
                	    					        	});            	    					    	        
            	    					    	    });            	    					    		
            	    					    	};    	
            	    					    	recurse(deleteRootTarget);
            	    					    	async.series(deleteCalls,function(error,results){
            	    					    		deletingDialog.close();            	    					    		
            	    					    		if(error){
            	    					    			Ext.MessageBox.alert('Failure', error);
            	    					    		}else{
            	    					    			Ext.MessageBox.alert('Success', 'Successfully deleted "'+deleteRootTarget.get('name')+'"');
            	    					    		}
            	    					    	});
            	    					    	if(deleteRootTarget.parentNode){
            	    					    		Ext.getCmp('foldertree').getSelectionModel().select(deleteRootTarget.parentNode,false,false);	
            	    					    	}else{
            	    					    		me.openFolder('~');
            	    					    	}            	    					    	
        	    						    },	                    	    						    
        	    						    taskFn = function(node, callback) {	                    	    						        
        	    						        node.expand(false, function(childNodes) {	                    	    						            	                    	    						            
        	    						            for(var i=0;i<childNodes.length;i++) {
        	    						                queue.push(childNodes[i]);
        	    						            }                
        	    						            callback();	                    	    						            	                    	    						            
        	    						        });
        	    						    },
        	    						    queue = async.queue(taskFn, 5);
        	    						    queue.drain = allNodesLoadedFn;
        	    						    queue.push(deleteRootTarget);	        	    						                	    							        	    							  	                    	    								                    	    	            				                    	    						
        	    						}
        	    					}
    							});    			    			
    			    			return;
    			    		}
    			    		deleteUrl = "/folder/"+deleteRootTarget.get('duid');
    			    	}
    					Ext.Ajax.request({
        	        	    method : "DELETE",
        	        	    url: deleteUrl,
        	        	    success : function(){
        	        	    	if(deleteRootTarget.get('duid')){        	        	    		        	        	    		
        	        	    		if(deleteRootTarget.parentNode){
	    					    		Ext.getCmp('foldertree').getSelectionModel().select(deleteRootTarget.parentNode,false,false);
	    					    		deleteRootTarget.remove(false);
	    					    	}else{
	    					    		me.openFolder('~');
	    					    	}        	        	    		
    	        	    		}else{            	        	    			
    	        	    			var gridpanel = finderViewport.down('gridpanel');
    	        	    			gridpanel.getStore().remove(deleteRootTarget);
    	        	    			gridpanel.getStore().load();    	        	    			
    	        	    		}
        	        	    },
        	        	    failure: function(response){
        	        	    	if(response.status == 403) {
        	        	    		Ext.MessageBox.alert('Forbidden', response.getResponseHeader("Reason-Phrase"));
    	        	    		}else if(response.status == 401) {
    	        	    			deleteRootTarget.set('isRemovable',false);	 
        	        	    		Ext.MessageBox.alert('Unauthorized', "You are not allowed to delete "+deleteRootTarget.get('name'));	
    	        	    		}else{
    	        	    			Ext.MessageBox.alert('Error', "Unable to delete "+deleteRootTarget.get('name')+" at this time. Please try again later.");
    	        	    		}    	        	    	
        	        	    }
        	        	});										
    				}
    			}
        	});    		
    	}    	    	    	    	
    },    
    expireClick: function(button,event,eOpts){
    	if(currentlySelected instanceof Array){
    		canExpire = true;
    		if(isNotDomainAdmin){
    			var numSelected = currentlySelected.length;
    			for(i=0;i<numSelected;i++){
    				if(currentlySelected[i].get('creator') != currentUsername){
    					canExpire = false;
    					break;
    				} 
    			}	
    		}
    		if(canExpire){
    			var linkPopup = Ext.create('FolderGridFinder.view.ui.ExpirationPopup', {
    	    		title: 'Assign expiration for the '+currentlySelected.length+' selected files.',
    	            renderTo: Ext.getBody()        
    	        });	    		    	  	            	       
        		linkPopup.show();	
    		}else{
    			Ext.MessageBox.alert('Access Denied', "You do not have permission to expire all of the selected files.");
    		}    		
    	}else{
    		var linkPopup = Ext.create('FolderGridFinder.view.ui.ExpirationPopup', {
	    		title: 'Assign expiration for ['+currentlySelected.get('name')+']',
	            renderTo: Ext.getBody()        
	        });	    		    	  	            	       
    		linkPopup.show();  		
    	}    	    	    	    	
    },        
    logClick: function(button,event,eOpts){    	
    	if(currentlySelected.get('isACLReadable')){
    		var logUrl = "/file/log/"+currentlySelected.get('id');
    		var logPopupClass = 'FolderGridFinder.view.ui.LogPopup';
	    	if(currentlySelected.get('duid')){
	    		logUrl = "/folder/log/"+currentlySelected.get('duid');
	    		logPopupClass = 'FolderGridFinder.view.ui.LogFolderPopup';
	    	}
        	Ext.Ajax.request({
        	    method : "GET",
        	    url: logUrl,
        	    success : function(response){        	    	
        	    	var logPopup = Ext.create(logPopupClass, {
        	    		title: 'Audit Log Listing for ['+currentlySelected.get('name')+']',
        	            renderTo: Ext.getBody()
        	        });
        	    	var logStore = Ext.data.StoreManager.lookup('logStore');
        	    	var jsonData = Ext.JSON.decode(response.responseText);        	    	
        	    	var rs = logStore.getProxy().getReader().read(jsonData);
        	    	logStore.add(rs.records);   
        	    	logPopup.show();        	    	
        	    },
        	    failure: function(response){        	    	
        	    	Ext.MessageBox.alert('Access Denied', "You are not allowed to read the audit log for "+currentlySelected.get('name'));        	    	
        	    }
        	});      		
    	}    	
    },    
    revisionsClick: function(button,event,eOpts){    	
		var revUrl = "/file/revisions/"+currentlySelected.get('id'); 	    	    		
    	Ext.Ajax.request({
    	    method : "GET",
    	    url: revUrl,
    	    success : function(response){        	    	
    	    	var revsPopup = Ext.create('FolderGridFinder.view.RevisionsPopup', {
    	    		title: 'Revisions Listing for ['+currentlySelected.get('name')+']',
    	            renderTo: Ext.getBody()
    	        });
    	    	var revStore = Ext.data.StoreManager.lookup('revStore');
    	    	var jsonData = Ext.JSON.decode(response.responseText);        	    	
    	    	var rs = revStore.getProxy().getReader().read(jsonData);
    	    	revStore.add(rs.records);        	    	        	    	        	            	        
    	    	revsPopup.show();        	    	
    	    },
    	    failure: function(response){        	    	
    	    	Ext.MessageBox.alert('Access Denied', "You are not allowed to read revisions for "+currentlySelected.get('name'));        	    	
    	    }
        	});      		    	
    },    
    manageUsersClick: function(button,event,eOpts){
    	var usersPopup = Ext.create('FolderGridFinder.view.ui.UsersPopup', {
    		title: 'Manage Domain Users',
            renderTo: Ext.getBody()
        });    	
    	usersPopup.show();
    },
    manageTrashClick: function(button,event,eOpts){
    	var binPopup = Ext.create('FolderGridFinder.view.ui.TrashBinPopup', {
    		title: 'TrashBin Folder Listing',
            renderTo: Ext.getBody()
        });	     
    	binPopup.show();
    },
    manageGroupsClick: function(button,event,eOpts){
    	var groupsPopup = Ext.create('FolderGridFinder.view.ui.GroupsPopup', {
    		title: 'Manage Domain Groups',
            renderTo: Ext.getBody()        
        });    	
    	groupsPopup.show();
    },    
    manageSettingsClick: function(button,event,eOpts){
    	var settingsPopup = Ext.create('FolderGridFinder.view.ui.SettingsPopup', {
    		title: 'Manage Domain Settings',
            renderTo: Ext.getBody()        
        });    	
    	settingsPopup.show();
    },    
    viewUploadReportClick: function(button,event,eOpts){    	
		var reportUrl = "/search/uploads";
		var reportPopupClass = 'FolderGridFinder.view.ui.UploadsPopup';	    	
    	Ext.Ajax.request({
    	    method : "GET",
    	    url: reportUrl,
    	    success : function(response){        	    	
    	    	var reportPopup = Ext.create(reportPopupClass, {
    	            renderTo: Ext.getBody()
    	        });
    	    	var reportStore = Ext.data.StoreManager.lookup('uploadsStore');
    	    	var jsonData = Ext.JSON.decode(response.responseText);        	    	
    	    	var rs = reportStore.getProxy().getReader().read(jsonData);
    	    	reportStore.add(rs.records);   
    	    	reportPopup.show();        	    	
    	    },
    	    failure: function(response){        	    	
    	    	Ext.MessageBox.alert('Access Denied', "You are not allowed to view that report");        	    	
    	    }
    	});      		    	
    },    
    showDetailsClick: function(button,event,eOpts){
    	if(currentlySelected.get('duid')){
    		var detailsPopup = Ext.create('FolderGridFinder.view.ui.FolderDetailsPopup', {
        		title: 'Folder Details',
                renderTo: Ext.getBody()        
            });	    		    	  	            	       
    		detailsPopup.show();
    	}else{
    		var detailsPopup = Ext.create('FolderGridFinder.view.ui.FileDetailsPopup', {
        		title: 'File Details',
                renderTo: Ext.getBody()        
            });	    		    	  	            	       
    		detailsPopup.show();    		
    	}		     		      		    	
    },
    linkClick: function(button,event,eOpts){
    	if(currentlySelected.get('duid')){
    		if(currentlySelected.get('isWritable') && currentlySelected.get('isACLWritable')){
        		var linkPopup = Ext.create('FolderGridFinder.view.ui.DropzonePopup', {
    	    		title: 'Create Public Dropzone for ['+currentlySelected.get('name')+']',
    	            renderTo: Ext.getBody()        
    	        });	    		    	  	            	       
        		linkPopup.show();     		      		
        	}
    	}else{
    		if(currentlySelected.get('isACLReadable') && currentlySelected.get('isACLWritable')){
        		var linkPopup = Ext.create('FolderGridFinder.view.ui.ShareLinkPopup', {
    	    		title: 'Create Public Link for ['+currentlySelected.get('name')+']',
    	            renderTo: Ext.getBody()        
    	        });	    		    	  	            	       
        		linkPopup.show();     		      		
        	}    		
    	}    	
    },
    permissionsClick: function(button,event,eOpts){
    	if(currentlySelected.get('isACLReadable')){
    		var aclUrl = "/file/acl/"+currentlySelected.get('id');    		
	    	if(currentlySelected.get('duid')){
	    		aclUrl = "/folder/acl/"+currentlySelected.get('duid');
	    	}    		
        	Ext.Ajax.request({
        	    method : "GET",
        	    url: aclUrl,
        	    success : function(response){        	    	
        	    	var permPopup = Ext.create('FolderGridFinder.view.PermissionsPopup', {
        	    		title: 'Access Control Listing for ['+currentlySelected.get('name')+']',
        	            renderTo: Ext.getBody()        
        	        });        	    	
        	    	var aclStore = Ext.data.StoreManager.lookup('aclStore');
        	    	var jsonData = Ext.JSON.decode(response.responseText);
        	    	currentlySelected.set('isACLWritable',jsonData.isACLWritable);        	    	
        	    	var rs = aclStore.getProxy().getReader().read(jsonData);
        	    	aclStore.add(rs.records);
        	    	
        	    	permPopup.editing.on('beforeedit',
    	            	function (editor, ctx) {
	        	    		if(ctx.field == 'grantee'){
	    	    				Ext.MessageBox.alert('Not Allowed', "You can't modify the grantee of an existing entry. " +
	    	    						"To remove this grantee from the ACL use the 'Remove Entry' button. Use the Add Entry button to assign permission to a different grantee.");
	    	    				return false;
	    	    			}
	        	    		if(ctx.record.get('grantee') == 'Administrators'){
	    	    				Ext.MessageBox.alert('Not Allowed', "You can't modify the permission for the Administrators group. " +
	    	    						"Domain Administrators maintain Full Control of all content.");
	    	    				return false;
	    	    			}
        	    			if(!currentlySelected.get('isACLWritable')){
        	    				Ext.MessageBox.alert('Access Denied', "You do not have permission to update this entry's permissions.");        	    				
        	    				return false;
        	    			}
        	    			return true;
    	    			}    	    		
        	    	);
        	    	permPopup.editing.on('edit', function (editor, event) {
		                if(!event.record.dirty) return;
		                permPopup.processPermissions(event.record,aclStore);
	    			});
        	    	permPopup.show();        	    	
        	    },
        	    failure: function(response){
        	    	currentlySelected.set('isACLReadable',false);        	    	
        	    	Ext.MessageBox.alert('Access Denied', "You are not allowed to read permissions for "+currentlySelected.get('name'));        	    	
        	    }
        	});      		
    	}
    },        
    newFolderClick: function(button,event,eOpts){
    	if(currentlySelected instanceof FolderGridFinder.model.Folder){
        	Ext.Msg.prompt('New Folder', 'Please enter a new folder name:', function(btn, text){
        	    if (btn == 'ok'){         	 		
        	 		var newDuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        	 		    var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
        	 		    return v.toString(16);
        	 		});        	 
        	 		var data = Ext.JSON.encode({"parentDuid":currentlySelected.get('duid'),"name":text,"duid":newDuid});
    	        	Ext.Ajax.request({
    	        	    method : "PUT",
    	        	    url: "/folder/"+ newDuid,
    	        	    jsonData : data,
    	        	    success : function(){
    	        	    	currentlySelected.expand();  
    	        	    	var newFolder = Ext.create('FolderGridFinder.model.Folder', {
    	        	    	    name : text,
    	        	    	    duid : newDuid,
    	        	    	    subfoldersInherit : currentlySelected.get("subfoldersInherit"),
    	        	    	    isWritable: true,
    	        	    	    isACLWritable: true,
    	        	    	    creator: currentUsername,
    	        	    	    updated : new Date(),
    	        	    	    parent : currentlySelected
    	        	    	});
    	        	    	currentlySelected.insertChild(0,newFolder);
    	        	    },
    	        	    failure: function(response){    	        	    	    	        	    	
    	        	    	Ext.MessageBox.alert('Access Denied', "You are not allowed to create a folder named ["+text+"] under "+currentlySelected.get('name'));    	        	    	
    	        	    }
    	        	});    		    	    	    	
        	    }
        	});      		
    	}    	
    }    
});
