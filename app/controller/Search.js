/**
 * Controller for search.
 */
Ext.define('FolderGridFinder.controller.Search', {
    extend: 'Ext.app.Controller',
    requires: ['FolderGridFinder.store.Search','FolderGridFinder.model.Folder'],
    views: [
        'ui.SearchContainer',            
        'ui.Dropdown'      
    ],
    refs: [
        {
            ref: 'field',
            selector: '#search-field'
        }
    ],

    // Current page in search results and nr of items on one page
    pageIndex: 0,
    pageSize: 10,
    id: 'search-controller',

    init: function() {
        this.control({
            '#search-dropdown': {
                changePage: function(dropdown, delta) {
                    // increment page number and update search results display
                    this.pageIndex += delta;
                    this.search(this.getField().getValue());
                    // footerClick doesn't fire in IE9,
                    // so keep the dropdown visible explicitly.
                    this.keepDropdown();
                },
                footerClick: function(dropdown) {
                    this.keepDropdown();
                }
            },
            '#search-field': {
                keyup: function(el, ev) {
                    var dropdown = this.getDropdown();

                    el.setHideTrigger(el.getValue().length === 0);

                    if (ev.keyCode === Ext.EventObject.ESC || !el.value) {
                        dropdown.hide();
                        el.setValue("");
                        return;
                    }
                    else {
                        dropdown.show();
                    }

                    var selModel = dropdown.getSelectionModel();
                    var record = selModel.getLastSelected();
                    var curIndex = dropdown.store.indexOf(record);
                    var lastIndex = dropdown.store.getCount() - 1;

                    if (ev.keyCode === Ext.EventObject.UP) {
                        if (curIndex === undefined) {
                            selModel.select(0);
                        } else {
                            selModel.select(curIndex === 0 ? lastIndex : (curIndex - 1));
                        }
                    }
                    else if (ev.keyCode === Ext.EventObject.DOWN) {
                        if (curIndex === undefined) {
                            selModel.select(0);
                        } else {
                            selModel.select(curIndex === lastIndex ? 0 : curIndex + 1);
                        }
                    }
                    else {
                        // A new search - reset paging back to first page
                        this.pageIndex = 0;
                        // Wait a bit before actually performing the search.
                        // When user is typing fast, the value of el.value
                        // might not right away be the final value.  For example
                        // user might type "tre" but we will get three keyup events
                        // where el.value === "t".
                        clearTimeout(this.searchTimeout);
                        this.searchTimeout = Ext.Function.defer(function() {
                        	if( Ext.String.trim(el.value).length >= 3) this.search(Ext.String.trim(el.value));
                        }, 200, this);
                    }
                },
                focus: function(el) {
                    if (el.value && this.getDropdown().store.getCount() > 0) {
                        this.getDropdown().show();
                    }
                },
                blur: function() {
                    // Don't hide the dropdown right away, otherwise
                    // we don't receive the itemclick event when focus
                    // was lost because we clicked on dropdown item.
                    // Not really a good solution, but I can't
                    // currently think of anything better.  Behaves
                    // badly when you make a long mouse press on
                    // dropdown item.
                    
                	//var dropdown = this.getDropdown();
                    //this.hideTimeout = Ext.Function.defer(dropdown.hide, 500, dropdown);
                }
            }
        });
    },

    getDropdown: function() {
        return this.dropdown || (this.dropdown = Ext.getCmp('search-dropdown'));
    },

    // Cancels hiding of dropdown
    keepDropdown: function() {
        clearTimeout(this.hideTimeout);
        this.getField().focus();
    },

    search: function(terms) {
    	var dropdown = this.getDropdown();    	
    	var me = this;
    	
    	Ext.Ajax.request({
    	    method : "GET",
    	    url: "/search/name/"+terms,
    	    success : function(response){    	    	
    	    	var results = Ext.JSON.decode(response.responseText);
    	        // Don't allow paging before first or after the last page.
    	        if (me.pageIndex < 0) {
    	            me.pageIndex = 0;
    	        }
    	        else if (me.pageIndex > Math.floor(results.hits.length / me.pageSize)) {
    	            me.pageIndex = Math.floor(results.hits.length / me.pageSize);
    	        }
    	        var start = me.pageIndex * me.pageSize;
    	        var end = start + me.pageSize;
    	        
    	        dropdown.setTotal(results.hits.length);
    	        dropdown.setStart(start);
    	        
    	        var slice = results.hits.slice(start, end);    	    	
    	    	
    	        dropdown.getStore().loadData(slice);
    	        // position dropdown below search box
    	        dropdown.alignTo('search-field', 'bl', [-12, -2]);
    	        // hide dropdown when nothing found
    	        if (results.hits.length === 0) {
    	            dropdown.hide();
    	        }
    	        else {
    	            // auto-select first result
    	            dropdown.getSelectionModel().select(0);
    	        }
    	    },
    	    failure: function(response){    	        	    	  	        	    	
    	    	Ext.MessageBox.alert('Forbidden', "You are not allowed to search at this time");    	        	    	
    	    }
    	});   
    	/* Good idea - but filterBy doesn't work with infinite scroll grids...
    	var keywords = terms.split(' ');
    	var panel = finderViewport.down('gridpanel');
    	panel.getStore().filterBy(function(record,id){
    		var name = record.get('name');
    		var hit = true;
    		for(var i=0;i<keywords.length;i++){
    			if(name.search(keywords[i]) < 0){    				
    				hit = false;
    			} 
    		}
    		return hit;
    	});
    	*/    	
    }
});
