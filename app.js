Ext.BLANK_IMAGE_URL = 'https://d1y4a35or0mr58.cloudfront.net/public/js/extjs422/resources/ext-theme-classic/images/tree/s.gif';
var splashscreen;
var fg_ratelimit = 170;
if (self !== top) {
    top.location.replace(self.location.href);
}
if(typeof String.prototype.trim !== 'function') {
  String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g, ''); 
  }
}
Ext.onReady(function() {
	Ext.Loader.setConfig({
        enabled: true
    });
	splashscreen = Ext.getBody().mask('Loading application', 'splashscreen');
	splashscreen.addCls('splashscreen');
	Ext.DomHelper.insertFirst(Ext.query('.x-mask-msg')[0], {
        cls: 'x-splash-icon'
    });		
	var queryString = document.location.href.split('?')[1];
    if(queryString){
    	var paramsObj = Ext.Object.fromQueryString(queryString);
    	if(paramsObj.openlocallyByDefault){
    		var expiration_date = new Date();
    		expiration_date.setFullYear(expiration_date.getFullYear() + 5);
    		Ext.util.Cookies.set("openlocallyByDefault",'true',expiration_date);	
    	}    	
    }

	Ext.application({
	    appFolder: deployedPath+'app',
	    name: 'FolderGridFinder',    
	    controllers: ['Controls','Search'],
	    stores: ['Search'],
	    launch: function() {
	    	var task = new Ext.util.DelayedTask(function() {
	            // Fade out the body mask
	            splashscreen.fadeOut({
	                duration: 1000,
	                remove:true
	            });
	            // Fade out the icon and message
	            splashscreen.next().fadeOut({
	                duration: 1000,
	                remove:true,
	                listeners: {
	                    afteranimate: function() {
	                        Ext.getBody().unmask();
	                    }
	                }
	            });
	        });
	        task.delay(500);
	        Ext.QuickTips.init();
	        var filePathMap = {}, refreshInterval;
	        var uploadDiv = $("#uploader");
	        uploadDiv.html("<p>Your browser doesn't have HTML5, Flash, Silverlight or HTML4 support.</p><p>Please use a modern graphical browser or upload through our API.</p>");
	        
	        var traverseFileTree = function (item, path) {
	        	var dirReader = null;
	        	path = path || '';
	        	if (item.isFile) {
	        		item.file(function(file) {
	        			// careful here, could be several files of the same name
	        			// we assume files will be in the same order here than in plupload
	        			if(filePathMap[file.name] === undefined) {
	        				filePathMap[file.name] = [];
	        			}
	        			filePathMap[file.name].push(path);
	        		});
	        	} else if (item.isDirectory) {
	        		dirReader = item.createReader();
	        		var readEntries = function() {
	        			dirReader.readEntries(function(entries) {		        			
	         		       if (entries.length) {	        		       
	         		    	  var n = 0;
		  	        		  for (n = 0; n < entries.length; n++) {
		  	        			traverseFileTree(entries[n], path + item.name + "/");
		  	        		  }
	        		          readEntries();
	        		       }
		        		});
	        	   };
         		   readEntries();  	        			        			
	        	}
	        	var uploader = uploadDiv.pluploadQueue();
    			if(uploader) {    				
    				uploader.trigger("QueueChanged", uploader);
    			}
	        };
	        document.getElementById('uploader').addEventListener('drop', function(e) {
	        	var items = e.dataTransfer.items, n, item;	        	
	        	if(items !== undefined){
		        	for(n = 0; n < items.length; n++) {
		        		item = items[n].webkitGetAsEntry();
		        		if(item) {
		        			traverseFileTree(item);
		        		}
		        	}		        	
	        	}else if (e.dataTransfer.files){
	        		items = e.dataTransfer.files;
	        		for(n = 0; n < items.length; n++) {
		        		var file = items[n];		        		
		        		if(!file.type && !file.path){		    				
		    				var reader = new FileReader();
		    				reader.onerror = function (e) {
		    					Ext.Msg.alert({
			                        msg: 'You have dropped a folder into the upload window but your browser does not support folder uploads.\nPlease use <a href="http://www.google.com/chrome/">Google\'s Chrome browser</a> to perform folder uploads.',
			                        title: 'Folder Upload Not Supported',
			                        closable: false,
			                        buttons: 1,
			                        draggable: false,
			                        fn : function() {
			                        	uploadPopup.close();
			                        }                      
			                    });
		    				};
		    				reader.readAsArrayBuffer(file);
		    			}
		        	}
	        	}
	        	
	        }, false);	        
	        uploadDiv.pluploadQueue({
		        // General settings
		        runtimes : "html5,silverlight,flash,html4",	        	
		        url : "/placeholder",
		        max_file_size : "200gb", 
		        // Specify what files to browse for
		        filters : [{title:"All files", extensions : "*"}],
		        // chunking is not currently supported chunk_size : "1mb",
		        multipart: false,
		        file_data_name: 'upload',
		        unique_names : false,
		        multiple_queues: true, //allow multiple successful operations
		        dragdrop: true,
		        rename: false, //introduces ESC bug in 1.x if true
		        resize : false,        
		        // Flash settings
		        container: 'uploader', //should match the div id used above - helps flash runtime in IE avoid buggy layout
		        drop_element: 'uploader', //name the drop element so we can bind to it for folder handling by Chrome
		        urlstream_upload: true, //Force UrlStream use in Flash to help address the occasional IO error. Error #2038
		        flash_swf_url : deployedPath+"Moxie.swf",
		        // Silverlight settings
		        silverlight_xap_url : deployedPath+"Moxie.xap",
		        // PreInit events, bound before any internal events
		        preinit : {
		            Init: function(up, info) {
		                if('html4' == up.runtime){
		                	up.setOption('multipart', true);
		                }
		            }
		        },
		        // Post init events, bound after the internal events
		        init : {
		            BeforeUpload: function(up, file) {
		            	var names = filePathMap[file.name];
		                if(names !== undefined) {
		                	var path = names.shift();
		                	if(path !== undefined) file.name = path + file.name;		    				
		    			}
		    			if(currentlySelected instanceof Array) {
		    			   var targetFolderDuid = currentlySelected[0].get('parentDuid');
		    			}else{
		    			   var targetFolderDuid = currentlySelected.get('duid') != null ? currentlySelected.get('duid') : currentlySelected.get('parentDuid');
		    			}		            	
				    	var thedata = '{"parentDuid": '+JSON.stringify(targetFolderDuid)+', "parts": "1", "name": '+JSON.stringify(file.name)+'}';
				        $.ajax({                 	
				            type: "POST",
				            url: "/file/provision?x-http-method-override=PUT",
				            contentType: "application/json",
				            data: thedata,
				            success: function(data,status,jqXHR){				       
				                up.setOption('url', jqXHR.getResponseHeader("location"));
				                //Not necessary : up.setOption('multipart_params', {'x-http-method-override': 'PUT'});
				                file.status = plupload.UPLOADING;
				                up.trigger("UploadFile", file);  
				                //Keep Portal session alive during long file uploads
				                refreshInterval = window.setInterval(function (a,b) {
				                	$.ajax({                 	
				    		            type: "GET",
				    		            url: "/folder/*",
				    		            contentType: "application/json",
				    		            data: '{"excludeFiles": true}'		            
				    		        });
				                },600000);
				            },
				            error: function(jqXHR, textStatus, errorThrown){
				                up.removeFile(file);                    
				                if(jqXHR.status == 401){
				                    Ext.Msg.alert({
				                        msg: 'You will now be redirected to login again.',
				                        title: 'Your session expired.',
				                        closable: false,
				                        buttons: 1,
				                        draggable: false,
				                        fn : function() {
				                            window.location = window.location; 
				                        }
				                    });                    
				                }else if(jqXHR.status == 403){
				                    msg = 'You do not have permission to add a new version of the file ['+file.name+'] to this folder.';
				                    if(jqXHR.getResponseHeader("Reason-Phrase")){
				                        msg = msg + " " + jqXHR.getResponseHeader("Reason-Phrase");
				                    }
				                    Ext.Msg.alert('Access Denied', msg);                                             
				                }else if(jqXHR.status == 500){
				                	Ext.Msg.alert('Redundant Storage Unavailable', 'Your redundant storage is temporarily unavailable. Please try again later or contact customer support for assistance.');
				                }		                				                
				                up.trigger("FileUploaded", file, jqXHR);
				            }
				        });
				        return false;
		            },		  		 
		            FileFiltered: function(up, file) {      
	                	var names = filePathMap[file.name];
		                if(names !== undefined) {
		                	var path = names.shift();
		                	if(path !== undefined) file.name = path + file.name;		    				
		    			}	                  
		                if('html4' == up.runtime && file.size >= 20971520){
		                	Ext.Msg.alert({
		                        msg: 'You are running an outdated browser limiting your maximum upload file size to 20 Megabytes. To upload '+file.name+' you will need to install <a href="get.adobe.com/flashplayer/">Adobe Flash</a>, <a href="www.microsoft.com/getsilverlight/">Microsoft Silverlight</a>, or <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">upgrade your browser to a modern version</a>.',
		                        title: 'Deprecated Browser Support',
		                        closable: false,
		                        buttons: 1,
		                        draggable: false,
		                        fn : function() {
		                        	up.removeFile(file);
		                        }                 
		                    });
		                }		                
		            },		  		  		  		            
		            UploadComplete: function(up, files) {
		                filePathMap = {};		                
		                Ext.ComponentManager.get('file-listing').getStore().load({addRecords: true});
		                if(refreshInterval !== undefined) clearInterval(refreshInterval);		                
		                if( !(currentlySelected instanceof Array) && currentlySelected.get('duid')){
		                	currentlySelected.set('lastRefreshed',1);		                	
		                	finderViewport.down('treepanel').getSelectionModel().deselectAll();
		                	var reselect = currentlySelected;
		                	currentlySelected = null;
    		    			finderViewport.down('treepanel').getSelectionModel().select(reselect);
		                }
		            }
		        }
		    });    	        	           	            
	        var rateLimitedRequest = _.rateLimit(Ext.Ajax.request, fg_ratelimit);
	        Ext.override(Ext.data.Connection, {
	            request : rateLimitedRequest	            
	        });	       	        
	    }
	});
	/* Extend the Underscore object with the following methods */
	// Origin: https://gist.github.com/mattheworiordan/1084831
	//
	// Dependencies
	// * underscore.js
	//
	_.rateLimit = function(func, rate, async) {
	  var queue = [];
	  var timeOutRef = false;
	  var currentlyEmptyingQueue = false;
	  
	  var emptyQueue = function() {
	    if (queue.length) {
	      currentlyEmptyingQueue = true;
	      _.delay(function() {
	        if (async) {
	          _.defer(function() { queue.shift().call(); });
	        } else {
	          queue.shift().call();
	        }	        
	        emptyQueue();
	      }, rate);
	    } else {
	      currentlyEmptyingQueue = false;
	    }
	  };
	  
	  return function() {		  
	    var args = _.map(arguments, function(e) { return e; }); // get arguments into an array
	    queue.push( _.bind.apply(this, [func, this].concat(args)) ); // call apply so that we can pass in arguments as parameters as opposed to an array
	    if (!currentlyEmptyingQueue) { emptyQueue(); }
	  };
	};	
});