A zero-footprint reference client for [FolderGrid's Secured Cloud File Servers](http://foldergrid.com/) written in ExtJS. Licensed under the GNU GPL license v3.

Features
========

* Zero-footprint (fully contained in the browser)
* Cross-platform drag & drop file upload support (HTML5, Flash, etc...)
* Backward-compatability (support browsers all the way back to IE6)
* Administrator actions (manage users and groups)
